<?php 
//SHOW IDS
function showIDs($cName) {
	// The args
	$args = array(
		'post_type'      => "product",
		'post_status' => array('publish', 'draft'),
		'posts_per_page' => -1,
		'product_cat' => $cName
	);
	// The Query
	$the_query = new WP_Query( $args );

	// The Loop
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$itemID = get_the_ID();
			
			switch($cName) {

				case "hot-spring":
				$metaKey = "syncIDHS";
				break;
				case "hot-spring-accessories":
				$metaKey = "syncIDHSA";
				break;
				case "hot-spring-hot-tub-innovation":
				$metaKey = "syncIDHSA";
				break;
				case "hot-spring-hot-tub-water-care":
				$metaKey = "syncIDHSA";
				break;
				case "hot-spring-covers":
				$metaKey = "syncIDHSA";
				break;
				case "hot-spring-lifters":
				$metaKey = "syncIDHSA";
				break;
				case "hot-spring-steps":
				$metaKey = "syncIDHSA";
				break;
				case "caldera-spas":
				$metaKey = "syncIDCS";
				break;
				case "caldera-spas-accessories":
				$metaKey = "syncIDCSA";
				break;
				case "caldera-spas-hot-tub-enjoyment":
				$metaKey = "syncIDCSA";
				break;
				case "caldera-spas-hot-tub-innovation":
				$metaKey = "syncIDCSA";
				break;
				case "caldera-spas-hot-tub-water-care":
				$metaKey = "syncIDCSA";
				break;
				case "caldera-spas-steps-covers-lifters":
					$metaKey = "syncIDCSA";
				break;
				case "other-caldera-spas-water-care":
				$metaKey = "syncIDCSAWC";
				break;
				case "monarch-water-care":
				$metaKey = "syncIDCSA";
				break;	
				case "caldera-spas-covers":
			    $metaKey = "syncIDCSA";
				break;
				case "caldera-spas-lifters":
				$metaKey = "syncIDCSA";
				break;
				case "caldera-spas-steps":
				$metaKey = "syncIDCSA";
				break;
				case "freeflow-spas":
				$metaKey = "syncIDFRS";
				break;
				case "freeflow-spas-accessories":
				$metaKey = "syncIDFRSA";
				break;
				case "fantasy-spas":
				$metaKey = "syncIDFAS";
				break;
				case "fantasy-spas-accessories":
				$metaKey = "syncIDFASA";
				break;
				case "endless-pools-fitness-systems":
				$metaKey = "syncIDEPFS";
				break;
				case "ep-accessories":
				$metaKey = "syncIDEPFSA";
				break;
			
			}

			$syncID = get_post_meta($itemID, $metaKey, true);
			echo get_the_title() . " - " . $itemID. " | SyncID: ". $syncID;
			echo "<hr><br>";

		}

	} else {
		// no posts found
	}
	/* Restore original Post Data */
	wp_reset_postdata();
}

//SYNC IDS
function syncProductsIDs($cName) {

	// The args
	$args = array(
		'post_type'      => "product",
		'post_status' => array('publish', 'draft'),
		'posts_per_page' => -1,
		'product_cat' => $cName
	);
	// The Query
	$the_query = new WP_Query( $args );
    $syncIDDone = false;
	// The Loop
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$itemID = get_the_ID();
			$itemName = get_the_title();
			//match ID with SyncID and ADD the META KEY 

			//Check WHAT IS THE Category Name
			if($cName == "hot-spring") {
				$metaKey = "syncIDHS";
				$syncIDs = ["1001549","1001521","1001522","1001520","1001519","1001547","1001518","1001517","1001516","1001515","1015791","1015794","1015820", "1018465", "1018462", "1018459", "1020536", "1020520", "1020518", "1020515", "1020514", "1020512", "1020510", "1020508", "1020470", "1020461"];
				$syncNames = ["Gleam","Pulse","Flair","Glow","Bolt","Tempo","Rhythm","Relay","SX","TX","Prism","Flash","Beam", "Triumph", "Sovereign", "Jetsetter", "Prodigy", "Jetsetter LX", "Vanguard", "Aria",  "Envoy", "Grandee", "Stride"];
				
				$hs = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					
					
					if($itemName == $syncName) {
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $hs, true);
					
						// echo $itemName . " Added SyncKey!<br>";
                        // echo $itemID . " - " . $hs ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$hs ++;
				}
			}

		
			if($cName == "hot-spring-accessories") {
				$metaKey = "syncIDHSA";
				$syncNames = [
					'Hot Spring® Connextion™ Remote Monitoring System',
					'Cool Zone™ Hot Tub Cooling System',
					'Hot Spring® 32 Everwood Spa Steps',
					'Hot Spring® 32 Polymer Spa Steps',
					'Hot Spring® Replacement Covers',
					'Hot Spring® Lift N Glide® Cover Lifter',
					'Hot Spring® UpRite® Cover Lifter',
					'Hot Spring® CoverCradle® Cover Lifter',
					'Hot Spring® CoverCradle® II Cover Lifter',
					'Hot Spring® Sound System with Bluetooth® Wireless Technology',
					'Hot Spring® Spas 22” HD Wireless Monitor',
					];
				$hsA = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);
					
					if($itemName == $syncName) {
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $hsA, true);
					
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $hsA ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$hsA ++;
				}
			}

			if($cName == "other-hot-spring-spas-water-care") {
				$metaKey = "syncIDHSWC";
				$syncNames = [
					"Vanishing Act™ Calcium Remover",
					"Hot Spring® Replacement Filters",
					"Hot Spring® Clean Screen® Pre-Filter",
					];
				$hsWC = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);

					if($itemName == $syncName) {
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $hsA, true);
					
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $hsWC ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$hsWC ++;
				}
			}

			

			if($cName == "caldera-spas") {
				$metaKey = "syncIDCS";
				$syncIDs = ["1000582","1000581","1000570","1000580","1000579","1000578","1000577","1000576","1000575","1000574","1000573","1000628","1000571", "1018440", "1018444", "1021781", "1021792"];
				$syncNames = ["Cantabria", "Geneva", "Niagara", "Tahitian", "Makena", "Salina", "Martinique", "Kauai", "Palatino", "Marino", "Vanto", "Tarino", "Aventine", "Florence", "Provence", "Seychelles", "Reunion"];
				$cs = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					
					if($itemName == $syncName) {
	
						delete_post_meta($itemID, $metaKey);

						add_post_meta($itemID, $metaKey, $cs, true);

						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $cs."<br><hr><br>";
                        $syncIDDone = true;
					}
					$cs ++;
				}

			}


			if($cName == "caldera-spas-accessories") {

				$metaKey = "syncIDCSA";
				$syncNames = [
					"Caldera® Spas Retail-Ready Filters",
					"Caldera® Spas 22” HD Wireless Monitor",
					"Caldera® Spas 32″ Polymer Hot Tub Steps",
					"Caldera® Spas Connextion™ Remote Monitoring System",
					"Caldera® Paradise™ Series Speakers",
					"Caldera® Utopia® Series Speakers",
					"Caldera® Spas Replacement Cover",
					"Caldera® Spas In Home Wireless",
					"Caldera® Spas Vacanza Speaker",
					"Caldera® Spas CoolZone™",
					"Caldera® Spas ProLift® Hot Tub Cover Lifter",
					"Caldera® Spas ProLift® IV Hot Tub Cover Lifter",
					"Caldera® Spas ProLift® II Hot Tub Cover Lifter",
					"Caldera® Spas ProLift® III Hot Tub Cover Lifter",
					"Caldera® Spas Sound System with Bluetooth® Wireless Technology",
					"Caldera® Spas EcoTech® Steps",
					"Caldera® Spas Retail-Ready Filters",
					"Caldera® Avante® Spa Steps",
					];
				$csA = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);
					
					if($itemName == $syncName) {
	
						delete_post_meta($itemID, $metaKey);

						add_post_meta($itemID, $metaKey, $csA, true);

						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $csA ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$csA ++;
				}

			}


			

			if($cName == "other-caldera-spas-water-care") {
					//Add new image to hot tub water care
			$waterCare = term_exists( 'caldera-spas-hot-tub-water-care', 'product_cat' );
 
			$waterCareImg = addImg('https://s3.amazonaws.com/wp-agilitysquared/myproductdata/wp-content/uploads/2019/03/mpd-freshwater.jpg');
		
			update_woocommerce_term_meta( $waterCare['term_id'], 'thumbnail_id', absint( $waterCareImg ) );	
	 
			
				$metaKey = "syncIDCSAWC";
				$syncNames = [
					"Caldera® Spas Retail-Ready Filters",
					"Vanishing Act™ Calcium Remover",
					"Clean Screen™ Pre-filter",
					"MPS Test Strips",
					"Monarch Silver Cartridge",
					"Ozone MPS solution",
					];
				$cswA = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);
					
					if($itemName == $syncName) {
	
						delete_post_meta($itemID, $metaKey);

						add_post_meta($itemID, $metaKey, $cswA, true);

						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $cswA ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$cswA ++;
				}

			}

			

			if($cName == "freeflow-spas") {
				$metaKey = "syncIDFRS";
				$syncIDs = ["1587","1602","1611","1613","1614","1615","1616","1020969","1020964","1020966"];
			    $frs = 1;
				foreach ($syncIDs as $syncID) {
					if($itemID == $syncID){
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $frs, true);
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $frs."<br><hr><br>";
                        $syncIDDone = true;
					}
					$frs ++;
				} 

			}	


			if($cName == "freeflow-spas-accessories") {
				$metaKey = "syncIDFRSA";
				$syncNames = [
				"Freeflow Spas® Replacement Filters",
				"Freeflow Spas® Straight Steps",
				"Freeflow Spas® Square Cover Lifter",
				"Freeflow Spas® Round/Triangle Cover Lifter",
				"Freeflow Spas® Ozone Kit",
				"Freeflow Spas® Curved Step",
				"Freeflow Spas® The Cool Step™"];
				$frsA = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);

					if($itemName == $syncName) {
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $frsA, true);
					
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $frsA ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$frsA ++;
				}
			}


			if($cName == "fantasy-spas") {
				$metaKey = "syncIDFAS";
				$syncIDs = ["1619","1621","1622","1623","2938","1021001"];
			    $fas = 1;
				foreach ($syncIDs as $syncID) {
					if($itemID == $syncID){
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $fas, true);
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $fas."<br><hr><br>";
                        $syncIDDone = true;
					}
					$fas ++;
				} 

			}

			if($cName == "fantasy-spas-accessories") {
				$metaKey = "syncIDFASA";
				$syncNames = [
				"Fantasy Spas® Replacement Filters",
				"Fantasy Spas® Spa Straight Steps",
				"Fantasy Spas® Square Cover Lifter",
				"Fantasy Spas® Ozone Kit",
				"Fantasy Spas® The Cool Step™",
				"Fantasy Spas® Curved Step",
				"Fantasy Spas® Round/Triangle Cover Lifter"];
				$fasA = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);

					if($itemName == $syncName) {
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $fasA, true);
					
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $fasA ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$fasA ++;
				}
			}



			if($cName == "endless-pools-fitness-systems") {
				$metaKey = "syncIDEPFS";
				$syncNames = [
				"E500 Endless Pools® Fitness Systems",
				"E550 Endless Pools® Fitness Systems",
				"E700 Endless Pools® Fitness Systems",
				"E2000 Endless Pools® Fitness Systems",
				"X200 SwimCross™ Exercise Systems",
				"X500 SwimCross™ Exercise Systems",
				"X2000 SwimCross™ Exercise Systems",
				"RecSport® R200 Recreation Systems",
				"RecSport® R500 Recreation Systems"];
				$epfs = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);

					if($itemName == $syncName) {
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $epfs, true);
					
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $epfs ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$epfs ++;
				}
			}

	
			
			if($cName == "ep-accessories") {
				$metaKey = "syncIDEPFSA";
				$syncNames = [
				"Watkins CoverCradle®",
				"CoolZone™ Cooling System",
				"Endless Pools Fit@Home™ Wifi and Mobile App",
				"Underwater Treadmill",
				"Pace Displays",
				"Wifi and Mobile App",
				"Vacuseal™ Lifting System",
				"Watkins Cover & Lifter",
				"Bluetooth®-Enabled Sound System",
				"Swim Tether",
				"Row Bar Kit with Resistance Gear",
				"Underwater Mirror",
				"Aqua Bike",
				"Easy Slider Cover Mounts",
				"Signature Steps"];
				$epfsA = 1;	
				
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);
	
					if($itemName == $syncName) {
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $epfsA, true);
					
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $epfsA ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$epfsA ++;
				}
			}
			
			

		}

    } 

    if (   $syncIDDone == true) {
    echo ' 
<div class="alert alert-success" role="alert">
Sync IDs are Done!
</div>
';
} else {
    echo ' 
    <div class="alert alert-danger" role="alert">
    Something when wrong! for '.$cName .'
    </div>
    ';
}
	/* Restore original Post Data */
	wp_reset_postdata();
}

// function syncPagesIDs($cName) {

// 	//Get Category Name and go over the page names

// 	if($cName == "hot-spring") {
// 		$pageNames = ['',''];
// 	}
// 	foreach ($pageNames as $pageName) {
// 	// The args
// 	$args = array(
// 		'post_type'      => "page",
// 		'post_status' => array('publish', 'draft'),
// 		'posts_per_page' => -1,
// 		'pagename' => $pageName
// 	);
// 	// The Query
// 	$the_query = new WP_Query( $args );
//     $syncIDDone = false;
// 	// The Loop
// 	if ( $the_query->have_posts() ) {
// 		while ( $the_query->have_posts() ) {
// 			$the_query->the_post();

// 		}
// 	}
// }
// 	/* Restore original Post Data */
// 	wp_reset_postdata();
// }

?>