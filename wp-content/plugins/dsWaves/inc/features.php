<?php 

function syncFeats($cName) {
	$type = "features";
	$host = "http://staging.myproductdata.com/wp-json/wp/v2/";
	$ItemIDS = [];
	$didItWork = false;
	$sync = "manual";

	//check that category and get the ids
	switch ($cName) {
		case 'hot-spring':
			$itemIDS = ["3372", "3234", "3222", "3221", "3220","3219","3218","3215","3214","3194","3188","3186","3080","3078","993","992","991","983","982","1003152","1006968"];
        break;
        case 'caldera-spas':
            $itemIDS = ["1680","1685","1687","1689","1721","1723","1726","1728","1747","1789","2803","2950","2973","2976","1002789","1002790","1002791","1021805","1021804","1021803","1021779","1021778","1021776"];
        break;
        case 'fantasy-spas':
            $itemIDS = ["3392","3389","3386"];
        break;
        case 'freeflow-spas':
            $itemIDS = ["1626","1625","1600"];
		break;
		case 'endless-pools-fitness-systems':
			$itemIDS = ["1023409", "1023410", "1023414", "1023417", "1023419"];
		break;
	}

	//loop over the ids and update each one
	foreach($itemIDS as $itemID) {
		
		$response = wp_remote_get($host.$type.'/'.$itemID);
		if( is_wp_error( $response ) ) {
			echo $response->get_error_message();
				echo "<br>";
				echo "try again please!";
				die;
		}
		$post = json_decode( wp_remote_retrieve_body( $response ) );





		$postTitle = html_entity_decode($post->title->rendered);
		if($post->content->rendered) {
			$content = $post->content->rendered;	
		} else {
			$content = " ";
		}
	
		$excerpt = " ";
		$pageSlug = $post->slug;
		$pageID = $post->id;
		$type = html_entity_decode($type);
		$author = "0";
		  $postObject = get_page_by_slug_feat($pageSlug);
		  $date = new DateTime();
		  $date->add(DateInterval::createFromDateString('yesterday'));
		  $contentModified = $date->format('Y-m-d') . "\n";
		  if ($postObject) {
				 //echo "Updated: " . $postTitle . "<br>";
				 $pageID = $postObject->ID;

	  } else {
					  //create Feature 	
					  if($postTitle) {
					  $pageID = createWPPost($postTitle,$author,$content,$excerpt,$type);
				 // echo "Created: " . $postTitle . "<br>";
					  }
				  
		  }


	// 	  if($sync == "auto") {
	// 	//check if it really needs an update 
	// 	$contentModifiedSync = $post->modified;
	// 	$contentModifiedSync = explode('T', $contentModifiedSync);
	// 	$contentModifiedSync = $contentModifiedSync[0];
	//  }  
	//  if($sync == "manual") {

	//  }

		//   echo $contentModified . " - ";
		//   echo $contentModifiedSync . "<br>";
		
	
			$didItWork = true;	
			// echo "Updated: " . $postTitle . "<br>";
	 	
		  //update acfs
		  if($post->acf) {

			  $acfs = object_2_array($post->acf);
			  foreach ($acfs as $acfName => $acfValue) {
				  if($acfName == "feature-single-image") {
					  if(!empty($acfValue)) {
						  $imgID = addImg($acfValue);
						  $acfValue = $imgID;
					  }
				  }
				  if($acfName == "image-slider") {
					$images = object_2_array($acfValue);
					$imgIDs = [];
					foreach ($images as $image) {
						$imgID = addImg($image['url']);
						array_push($imgIDs, $imgID);
					}
					$acfValue = $imgIDs;
				}
					// echo $acfName . "<br>";
				  update_field($acfName, $acfValue, $pageID);
				}
				
		  }

	

	}

	if($didItWork) {
			echo ' 
	<div class="alert alert-success" role="alert">
			Step Features: '.$cName.' has been done!
	</div>';
	} else {
		echo ' 
		<div class="alert alert-warning" role="alert">
					'.$cName.' Features did not need an update maybe a problem here.
		</div>';			
	}

}
?>