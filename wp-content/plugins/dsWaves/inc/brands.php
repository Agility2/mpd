<?php 

//Sync Brands
function syncBrands($cName) {

	if($cName == 'finnleo-saunas') {
				 //? Delete All the products from Finnleo and the Accessories
				 $products = wc_get_products(array(
					'limit' => -1,
					'category' => array('finnleo'),
				 ));
				 $products2 = wc_get_products(array(
					'limit' => -1,
					'category' => array('finnleo-saunas'),
				 ));
				 $products3 = wc_get_products(array(
					'limit' => -1,
					'category' => array('finnleo-accessories'),
				 ));
				
				 foreach($products as $product) {
					wp_delete_post($product->get_ID(), true);
				 }
				 foreach($products2 as $product2) {
					wp_delete_post($product2->get_ID(), true);
				 }
				 foreach($products3 as $product2) {
					wp_delete_post($product3->get_ID(), true);
				 }

				//? find product categories and delete them to remake the whole structure again.
				$categories = array(
					'classic-sauna-controls',
					'deluxe-sauna-controls',
					'hallmark-series',
					'heaters-finnleo-saunas',
					'blackline-with-biowater-technique',
					'classic-heaters',
					'tonttu-series-heaters',
					'wood-burning-sauna-heaters',
					'heaters-controls',
					'indoor-sauna-rooms',
					'custom-cut-saunas',
					'designer-series', 
					'sisu-hallmark-series',
					'infra-saunas',
					'is-series',
					'infrared-saunas',
					'b-series',
					'custom-cut-far-infrared-saunas',
					'g-series-far-infrared-saunas',
					'infrasauna',
					's-series',
					'outdoor-sauna-rooms',
					'outdoor-saunas',
					'traditional-saunas',
					'with-steam',
					'without-steam',
					'classic-sauna-accessories',
					'light-sound',
					'other-sauna-accessories',
					'rento-buckets',
					'rento-ladles',
					'rento-linens',
					'sauna-doors'	
				);


				$localCat = term_exists('finnleo', 'product_cat');
				wp_delete_term($localCat['term_id'], 'product_cat');

				$localCat = term_exists('finnleo-saunas', 'product_cat');
				wp_delete_term($localCat['term_id'], 'product_cat');

				$localCat = term_exists('finnleo-accessories', 'product_cat');
						wp_delete_term($localCat['term_id'], 'product_cat');
					
			
					//create top category
					$saunaCat = term_exists('saunas', 'product_cat');
		
						if($saunaCat != 0 && $saunaCat != NULL) {
							$parentID = $saunaCat['term_id'];
						} else {
							$parentID = ' ';
						}
						wp_insert_term('finnleo', 'product_cat', array (
							'description' => 'Finnleo Saunas',
							'slug' => 'finnleo',
							'parent' => $parentID
						) );

					//create accessories for finnleo
						$saunaAccCat = term_exists('sauna-accessories', 'product_cat');
						if($saunaAccCat != 0 && $saunaAccCat != NULL) {
							$parentID = $saunaAccCat['term_id'];
						} else {
							$parentID = ' ';
						} 
						wp_insert_term('finnleo-accessories', 'product_cat', array (
 													 'description' => 'Finnleo Saunas Accessories',
													 'slug' => 'finnleo-accessories',
													 'parent' => $parentID
						 ) );
						
						

				foreach($categories as $category) {
						$localCat = term_exists($category, 'product_cat');
						wp_delete_term($localCat['term_id'], 'product_cat');
					
					$parent = term_exists('finnleo', 'product_cat');
					$parentID = $parent['term_id']; 

					if($category == 'blackline-with-biowater-technique' || 
						 $category == 'classic-heaters' || 
						 $category == 'tonttu-series-heaters' ||
						 $category == 'woodburning-sauna-heaters') {
							
							$parent = term_exists('heaters-finnleo-saunas', 'product_cat');
							$parentID = $parent['term_id']; 
					}

					if($category == 'custom-cut-saunas' || 
					$category == 'designer-saunas' || 
					$category == 'sisu-hallmark-series') {
					 $parent = term_exists('indoor-sauna-rooms', 'product_cat');
					 $parentID = $parent['term_id']; 
					
					}
		
					if($category == 'is-series') {
					 $parent = term_exists('infra-saunas', 'product_cat');
					 $parentID = $parent['term_id']; 
					}

					if($category == 'b-series' || 
					$category == 'custom-cut-far-infrared-saunas' || 
					$category == 'g-series-far-infrared-saunas' ||
					$category == 'infrasauna' ||
					$category == 's-series') {
					 $parent = term_exists('infrared-saunas', 'product_cat');
					 $parentID = $parent['term_id']; 
					}

					if($category == 'classic-sauna-accessories' || 
					$category == 'light-sound' || 
					$category == 'other-saunas-accessories' ||
					$category == 'rento-buckets' ||
					$category == 'rento-ladles' ||
					$category == 'rento-linens' ||
					$category == 'sauna-doors') {
					 $parent = term_exists('finnleo-accessories', 'product_cat');
					 $parentID = $parent['term_id']; 
					}
	
					wp_insert_term($category, 'product_cat', array (
					 'slug' => $category,
					 'parent' => $parentID
					) );

				}
	
	}

echo '<div class="alert alert-success" role="alert">
Step SyncBrands: Finnleo has been done!
</div>';
 }

 ?>