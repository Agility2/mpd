<?php 
//Load WordPress Stuff
global $wpdb;
//FIX template for new WooCommerce
add_filter( 'template_include', 'product_custom_template', 11 );
//GET OBJECT AND TURN IT TO ARRAY TO BE READY TO POPULATE
function object_2_array($result) {
	$result2 = (array) $result;
	$json = json_encode($result2);
	$result = json_decode($json, true);
	    return $result;
}

function fixArray($catNames) {
		if (strpos($catNames, ',') !== false) {
			$rawCNames = explode(",", $catNames);
			foreach($rawCNames as $rawCName) {
					$realCatNames[] = $rawCName;
			}
		} else {
				$realCatNames[] = $catNames;
		}
	
	return $realCatNames;
}

function getTaxonomy($cName) {

	switch ($cName) {
		case "finnleo":
			$cName = array('saunas', $cName );
		break;
		case "finnleo-accessories": 
			$cName = array('sauna-accessories', $cName );
		break;
		case "highlife-nxt":
			$cName = array('hot-spring', $cName );
		break;
		case "highlife":
			$cName = array('hot-spring', $cName );
		break;
		case "limelight":
			$cName = array('hot-spring', $cName );
		break;
		case "hotspot":
			$cName = array('hot-spring', $cName );
		break;	

		case "utopia":
			$cName = array('caldera-spas', $cName );
		break;
		case "paradise":
			$cName = array('caldera-spas', $cName );
		break;
		case "vacanza":
			$cName = array('caldera-spas', $cName );
		break;

		case "caldera-spas-accessories":
		$cName = array( 'spa-accessories', $cName );
		break;

		case "hot-spring-accessories":
		$cName = array( 'spa-accessories', $cName );
		break;

		case "fantasy-spas-accessories":
		$cName = array( 'spa-accessories', $cName );
		break;
		
		case "freeflow-spas-accessories":
		$cName = array( 'spa-accessories', $cName );
		break;
		
		case "caldera-spas-hot-tub-enjoyment":
		$cName = array( 'spa-accessories', 'caldera-spas-accessories', $cName );
		break;
		
		case "caldera-spas-hot-tub-innovation":
		$cName = array( 'spa-accessories', 'caldera-spas-accessories', $cName );
		break;
		
		case "caldera-spas-hot-tub-water-care":
		$cName = array( 'spa-accessories', 'caldera-spas-accessories', $cName );
		break;
		
		case "caldera-spas-steps-cover-lifters":
		$cName = array( 'spa-accessories', 'caldera-spas-accessories', $cName );
		break;
		
		case "caldera-spas-covers":
		$cName = array( 'spa-accessories', 'caldera-spas-accessories', 'caldera-spas-steps-cover-lifters', $cName );
		break;

		case "caldera-spas-lifters":
		$cName = array( 'spa-accessories', 'caldera-spas-accessories', 'caldera-spas-steps-cover-lifters', $cName );
		break;

		case "caldera-spas-steps":
		$cName = array( 'spa-accessories', 'caldera-spas-accessories', 'caldera-spas-steps-cover-lifters', $cName );
		break;
		
		//HOT SPRING ACCESSORIES
		case "hot-spring-hot-tub-enjoyment":
		$cName = array( 'spa-accessories', 'hot-spring-accessories', $cName );
		break;
		
		case "hot-spring-hot-tub-innovation":
		$cName = array( 'spa-accessories', 'hot-spring-accessories', $cName );
		break;
		
		case "hot-spring-hot-tub-water-care":
		$cName = array( 'spa-accessories', 'hot-spring-accessories', $cName );
		break;
		
		case "steps-cover-lifters":
		$cName = array( 'spa-accessories', 'hot-spring-accessories', $cName );
		break;
		
		case "hot-spring-covers":
		$cName = array( 'spa-accessories', 'hot-spring-accessories', '-steps-cover-lifters', $cName );
		break;

		case "hot-spring-lifters":
		$cName = array( 'spa-accessories', 'hot-spring-accessories', '-steps-cover-lifters', $cName );
		break;

		case "hot-spring-steps":
		$cName = array( 'spa-accessories', 'hot-spring-accessories', '-steps-cover-lifters', $cName );
		break;

		case "ep-swimcross-exercise-systems":
		$cName = array( 'endless-pools-fitness-systems', $cName );
		break;

		case "ep-fitness-systems":
		$cName = array( 'endless-pools-fitness-systems', $cName );
		break;

		case "ep-recsport-recreation-systems":
		$cName = array( 'endless-pools-fitness-systems', $cName );
		break;

		case "ep-recsport-recreation-systems":
		$cName = array( 'endless-pools-fitness-systems', $cName );
		break;

		case "ep-accessories":
		$cName = array( 'endless-pools-fitness-systems', $cName );
		break;
	}
	return $cName;
}
function get_page_by_slug_promo($page_slug, $output = OBJECT, $post_type = 'promotion' ) { 
	global $wpdb; 
	 $page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s AND post_status = 'publish'", $page_slug, $post_type ) ); 
	   if ( $page ) 
		  return get_post($page, $output); 
	  return null; 
	}
function get_page_by_slug_feat($page_slug, $output = OBJECT, $post_type = 'features' ) { 
	global $wpdb; 
	 $page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s AND post_status = 'publish'", $page_slug, $post_type ) ); 
	   if ( $page ) 
		  return get_post($page, $output); 
	  return null; 
	}
function get_page_by_slug($page_slug, $output = OBJECT, $post_type = 'general-block' ) { 
	global $wpdb; 
	 $page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s AND post_status = 'publish'", $page_slug, $post_type ) ); 
	   if ( $page ) 
		  return get_post($page, $output); 
	  return null; 
	}

    function wp9838c_timeout_extend( $time )
    {
        // Default timeout is 5
        return 50;
    } add_filter( 'http_request_timeout', 'wp9838c_timeout_extend' );
    
//CREATE A WORDPRESS POST-
function createWPPost($title,$author,$content,$excerpt,$type) {
	global $wpdb;
    $dsPost = array(
			  'filter' => true,
        'post_title'    => $title,
        'post_status'   => 'publish',
        'post_author'   => $author,
        'post_content'  => $content,
		'post_excerpt'  => $excerpt,
        'post_type'     => $type,
    );

	$dsPostID = wp_insert_post( $dsPost, $wp_error );
	if(!is_wp_error($dsPostID)){
		return $dsPostID;
	  }else{
		//there was an error in the post insertion, 
		return $dsPostID->get_error_message();
	  }    
}


  
//CREATE A WORDPRESS POST with Custom Slug-
function createWPPostWithSlug($title,$slug,$author,$content,$excerpt,$type) {
	global $wpdb;
    $dsPost = array(
			  'filter' => true,
        'post_title'    => $title,
        'post_status'   => 'publish',
				'post_author'   => $author,
				'post_name'     => $slug,
        'post_content'  => $content,
		    'post_excerpt'  => $excerpt,
        'post_type'     => $type,
    );

	$dsPostID = wp_insert_post( $dsPost, $wp_error );
	if(!is_wp_error($dsPostID)){
		return $dsPostID;
	  }else{
		//there was an error in the post insertion, 
		return $dsPostID->get_error_message();
	  }    
}

//Delete Items for testing purposes
function deleteItems($postType,$cName) {
	global $wpdb;
	// The args
	$args = array(
		'post_type'      => $postType,
		'post_status' => array('publish', 'draft'),
		'posts_per_page' => -1,
		'product_cat' => $cName
	);
	// The Query
	$the_query = new WP_Query( $args );

	// The Loop
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$itemID = get_the_ID();
			//delete post
			wp_delete_post($itemID);
		}

	} else {
		// no posts found
	}
	/* Restore original Post Data */
	wp_reset_postdata();

}

function fixName($itemName) {
	$itemName = str_replace("®", "", $itemName);
	$itemName = str_replace("&", "", $itemName);
	$itemName = str_replace("™", "", $itemName);
	$itemName = str_replace("#038;", "", $itemName);
	$itemName = str_replace(" ", "", $itemName);
	$itemName = str_replace("‘", "", $itemName);
	$itemName = str_replace('"', '', $itemName);
	$itemName = str_replace("#8243;", "", $itemName);
	$itemName = str_replace("#8216;", "", $itemName);

	

	return $itemName;
}

function getContent($dsType,$dsFilter,$cName) {
    
        $host = "http://staging.myproductdata.com/wp-json/wp/v2/";

        //example https://myproductdata.com/wp-json/wp/v2/product?filter[posts_per_page]=100&filter[product_cat]=fantasy-spas
    $response = wp_remote_get($host.$dsType.'?filter[posts_per_page]=100&filter['.$dsFilter.']=' . $cName);
    
    
        if( is_wp_error( $response ) ) {
            echo $response->get_error_message();
                echo "<br>";
                echo "try again please!";
                die;
        }
    
        $posts = json_decode( wp_remote_retrieve_body( $response ) );
    
        return $posts;
	}

	function getContentEP($dsType,$dsFilter,$cName) {
    
		$host = "http://staging.myproductdata.com/wp-json/wp/v2/";

		//example https://myproductdata.com/wp-json/wp/v2/product?filter[posts_per_page]=100&filter[product_cat]=fantasy-spas
$response = wp_remote_get($host.$dsType.'?filter[posts_per_page]=100&filter['.$dsFilter.']=' . $cName);


		if( is_wp_error( $response ) ) {
				echo $response->get_error_message();
						echo "<br>";
						echo "try again please!";
						die;
		}

		$posts = json_decode( wp_remote_retrieve_body( $response ) );

		return $posts;
}
function get_page_by_slug_page($page_slug, $output = OBJECT, $post_type = 'page' ) { 
  global $wpdb; 
   $page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s AND post_status = 'publish'", $page_slug, $post_type ) ); 
     if ( $page ) 
        return get_post($page, $output); 
    return null; 
	}
	

function acfLinkChangeData($acfValue) {
		$data = array_walk_recursive($acfValue, 'strReplaceCallback');
		return $data;
}

function strReplaceCallback( &$value, $key ) {
	$homeURL = home_url();
	$value = str_replace( 'https://staging.myproductdata.com', $homeURL, $value );
}



function get_page_by_post_name($post_name, $output = OBJECT, $post_type = 'post' ){
    global $wpdb;
    $page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type= %s", $post_name, $post_type ) );

    if ( $page ) return get_post( $page, $output );

    return null;
}

function get_post_id_by_slug( $slug, $post_type = "post" ) {
    $query = new WP_Query(
        array(
            'name'   => $slug,
            'post_type'   => $post_type,
            'numberposts' => 1,
            'fields'      => 'ids',
        ) );
    $posts = $query->get_posts();
    return array_shift( $posts );
}

	function getPages($pageSlug) {

		 $host = "http://staging.myproductdata.com/wp-json/wp/v2/";
   	 $response = wp_remote_get($host.'pages?slug='.$pageSlug);	
	   
      if( is_wp_error( $response ) ) {
            echo $response->get_error_message();
                echo "<br>";
                echo "try again please!";
                die;
       }
				
			  $page = json_decode( wp_remote_retrieve_body( $response ) );
    
        return $page;
	}



	function acf_get_field_key( $field_name, $post_id ) {
		global $wpdb;
		$acf_fields = $wpdb->get_results( $wpdb->prepare( "SELECT ID,post_parent,post_name FROM $wpdb->posts WHERE post_excerpt=%s AND post_type=%s" , $field_name , 'acf-field' ) );
		// get all fields with that name.
		switch ( count( $acf_fields ) ) {
			case 0: // no such field
				return false;
			case 1: // just one result. 
				return $acf_fields[0]->post_name;
		}
		// result is ambiguous
		// get IDs of all field groups for this post
		$field_groups_ids = array();
		$field_groups = acf_get_field_groups( array(
			'post_id' => $post_id,
		) );
		foreach ( $field_groups as $field_group )
			$field_groups_ids[] = $field_group['ID'];
		
		// Check if field is part of one of the field groups
		// Return the first one.
		foreach ( $acf_fields as $acf_field ) {
			if ( in_array($acf_field->post_parent,$field_groups_ids) )
				return $acf_field->post_name;
		}
		return false;
	}


	function deleteContent($cName) {
		
	$cat = get_term_by("slug", $cName, "product_cat");

	$catArgs = array(
    'post_type'             => 'product',
    'post_status'           =>  array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
    'posts_per_page'        => -1,
    'tax_query'             => array(
        array(
            'taxonomy'      => 'product_cat',
            'field'         => 'term_id',
            'terms'         => $cat->term_id,
            'operator'      => 'IN'
				),
		 )
		);
		$catQuery = new WP_Query( $catArgs );
		if( $catQuery->have_posts() ) {
			while( $catQuery->have_posts() ) {
				$catQuery->the_post();
				wp_delete_post( get_the_ID() );
			};
		};


	}

	function checkIfExists($productName) {
		$product = get_page_by_title( $productName, OBJECT, "product" );
		if($product->ID) {
			return true;
		} else {
			return false;
		}
	}

	function deleteProductsBySlug($slugs) {
		foreach($slugs as $slug) {
			$product = get_product_by_slug($slug);
			wp_delete_post($product->ID, true);
		}
	}

?>