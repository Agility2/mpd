<?php 
//Sync Pages
function syncPages($catName) {
	$didItWork = false;

	if($catName == 'hot-spring') {
		
		$pages = array (
			'highlife',
			'limelight',
			'hot-spot',
			'hot-spring-spas-water-care',
			'hot-spring-spas-freshwater-salt-system',
			'thank-requesting-hot-spring-brochure',
		);
		
	updatePages($pages);
	$didItWork = true;

	}

// CALDERA SPAS
if($catName == 'caldera-spas') {	
	
	$pages = array (
			'caldera-spas-freshwater-salt-system',
			// "caldera-spas',
			'utopia-series',
			'vacanza-series',
			'paradise-series',
			// "caldera-spas-owners-manuals",
			// "caldera-spas-accessories",
			// "caldera-spas-hot-tub-innovation",
			// "caldera-spas-steps-covers-lifters",
			// "caldera-spas-hot-tub-enjoyment",
			'caldera-spas-water-care',
			// "caldera-spas-owners-manuals",
			// "frog-water-care-system",
			// "other-caldera-spas-water-care",
			 'thank-requesting-caldera-spas-brochure',
			//  "caldera-spas-owners-manuals",
			// "caldera-spas-pre-delivery-instructions"
		);
		
		updatePages($pages);		  
		$didItWork = true;
}




// Freeflow SPAS
if($catName == 'freeflow-spas') {	

	$pages = array (
		'freeflow-spas',
		'about-freeflow-spas'
	);

	updatePages($pages);
	$didItWork = true;

}

// Freeflow SPAS
if($catName == 'fantasy-spas') {	
	$pages = array (
		'fantasy-spas',
	);	
	updatePages($pages); 
	$didItWork = true;
}

// Endless Pools
if($catName == 'endless-pools-fitness-systems') {	

	$pages = array (
		'endless-pools',
		'endless-pools-fitness-systems',
		'swimcross-exercise-systems',
		'recsport-recreation-systems',
	);
	
	updatePages($pages);
	$didItWork = true;
}

// Finnleo Saunas Pages
if($catName == "finnleo-saunas") {
	$pages = array(
		'finnleo-saunas',
		'finnleo-traditional-saunas',
		'indoor-sauna-rooms',
		'outdoor-sauna-rooms',
		'custom-cut-saunas',
		'designer-series',
		'sisu-hallmark-series',
		's-series',
		'infrasauna',
		'infrared-saunas',
		'custom-infrared-sauna-rooms',
		'sauna-accessories',
		'saunas-101',
		'health-benefits-of-sauna',
		'sauna-construction',
		'how-to-maintain-your-sauna',
		'finnleo-faq',
		'request-finnleo-sauna-pricing',
		'download-finnleo-sauna-brochure',
		'classic-sauna-accessories',
		'light-sound',
		'other-accessories',
		'classic-saunas-accessories',
		'classic-accessories',
		'other-sauna-accessories',
		'sauna-controls',
		'sauna-heaters',
		'sauna-doors',
		'rento-accessories',
		'rento-buckets',
		'rento-ladles',
		'rento-linens',
		'classic-sauna-controls',
		'deluxe-sauna-controls',
		'classic-heaters',
		'tonttu-series-heaters',
		'wood-burning-sauna-heaters',
		'sisu-saunas',
		'finnleo-documents'

	);
	deletePages($pages);
	updatePages($pages);
 
	$didItWork = true;
	
}

switch($catName) {
	case 'hot-spring':
	$catName = "Hot Spring Spas";
	break;
	case 'caldera-spas':
	$catName = "Caldera Spas";
	break;
	case 'freeflow-spas':
	$catName = "Freeflow Spas";
	break;
	case 'fantasy-spas':
	$catName = "Fantasy Spas";
	break;
	case 'endless-pools-fitness-systems':
	$catName = "Endless Pools";
	break;
	case 'finnleo-saunas':
		$catName = "Finnleo Saunas";
	break;
}

if($didItWork) {
	echo '<div class="alert alert-success" role="alert"> Step Pages: '.$catName.' pages have been updated!</div>';
} else {
	echo '<div class="alert alert-warning" role="alert">Step Pages: '.$catName.' pages did not need an update </div>';			
}

}

function deletePages($pageSlugs) {
	if(is_array($pageSlugs)) {
		foreach($pageSlugs as $pageSlug) {
			deletePage($pageSlug);
		}
	}
}

function deletePage($pageSlug) {
	//lets get the page from the system
	$page = get_page_by_slug_page($pageSlug, OBJECT, 'page');
	if($page) { // if it exists lets continue 
		$pageDeleted = wp_delete_post($page->ID); // pwn3d
		//echo "Page Deleted " . $page->post_title . " <br/>";
	}
}

function updatePage($pageSlug) {
			//echo $pageSlug . "<br>";
			//Lets get the page from MPD and update the page on this site. 
			$masterPage = getPages($pageSlug);
			//var_dump($masterPage);
			// render page 
			$masterPage = object_2_array($masterPage);

			$postTitle = $masterPage[0]['title']['rendered'];
			$content = $masterPage[0]['content']['rendered'];
			$excerpt = $masterPage[0]['excerpt']['rendered'];
			$pageTemplate = $masterPage[0]['template'];
			$acfs = $masterPage[0]['acf'];
			$parent = $masterPage[0]['parent'];
			$author = $masterPage[0]['author'];
		
	//var_dump($masterPage);
	//Lets check if there is a page in the system with the same slug
	$page = get_page_by_slug_page($pageSlug, OBJECT, 'page');

	if($page) {
	
	$pageID = $page->ID;

	$updatePage = array(
		'ID'            => $pageID,
		'post_title'    => $postTitle,
		'post_content'  => $content,
		'post_excerpt'  => $excerpt,
		'post_name'     => $pageSlug,
		'page_template' => $pageTemplate,
	);
	 
	// update the page
	 wp_update_post( $updatePage );
	 $syncPageID = $pageID;

	 } else {


		if($postTitle) {

		//No Page was found in the client website so we must create a new page
			if($content == "") {
				$content = " ";
			}
			if($excerpt == "") {
				$excerpt = " ";
			}


		//? We will need to change the parent id to the actual id from dealer site instead of mpd site.
		//? find what is the title of the page by looking it up on mpd.
		$response = wp_remote_get('http://staging.myproductdata.com/wp-json/wp/v2/pages/'.$parent);
		if( is_wp_error( $response ) ) {
				echo $response->get_error_message();
						echo "<br>";
						echo "try again please!";
						die;
		}
		$mpdPages = json_decode( wp_remote_retrieve_body( $response ) );
		$mpdPages = object_2_array($mpdPages);
		if(isset($mpdPages['slug'])) {
			$mpdPageSlug = $mpdPages['slug'];
			$mpdPageObj = get_page_by_slug_page($mpdPageSlug, OBJECT, 'page');
		}
		
		if($mpdPageObj) {
			$localParent = $mpdPageObj->ID;
		} else {
			$localParent = ' ';
		}
		
		$newPage = array(
			'post_title'    => $postTitle,
			'post_content'  => $content,
			'post_excerpt'  => $excerpt,
			'post_status'   => 'publish',
			'post_name'     => $pageSlug,
			'post_author'   => 1,
			'post_type'    => 'page',
			'post_parent' => $localParent,
			'page_template' => $pageTemplate 
		  );
		 
		// Insert the post into the database
		$syncPageID = wp_insert_post( $newPage );
		//echo "Page Created:" . $postTitle . " <br>";

		}


	 //update acfs
	 if($acfs) { 
	  foreach ($acfs as $acfName => $acfValue) {
	 	 if($acfName == "aspot_mobile_img") {
	 	  if(!empty($acfValue)) {
	 	 	 $imgID = addImg($acfValue);
	 	 	  $acfValue = $imgID;
	 	 	}
			}

			
			if($acfName == "aspot-image") {
				if(!empty($acfValue)) {
					$imgID = addImg($acfValue);
					 $acfValue = $imgID;
				 }
			 }

			if($acfName == "aspot_video_mobile_image") {
				if(!empty($acfValue)) {
					$imgID = addImg($acfValue);
					 $acfValue = $imgID;
				 }
			 }
			
			if($acfName == "aspot_video_mp4_link") {
				update_field($acfName, $acfValue, $pageID);
			}

	 	 if($acfName == "col_aspot_image") {
	 	  if(!empty($acfValue)) {
	 	 	 $imgID = addImg($acfValue);
	 	 	 $acfValue = $imgID;
	 	 	}
	 	}
			 
		 	//Special Finnleo Links 
		 	if($acfName == "links_pages") {
					 $acfValue = acfLinkChangeData($acfValue);
			 }
		 		 	//Special Finnleo Links 
						if($acfName == "links") {
							$acfValue = acfLinkChangeData($acfValue);
					}
			if($acfName == "all_features") {
				$features = object_2_array($acfValue);
				$featuresArray = array();
				foreach ($features as $feature) {
					$featTitle = $feature['post_title'];
					//get post id 
					$featureObj = get_page_by_title( $featTitle, OBJECT, "features" );
					array_push($featuresArray, $featureObj->ID);
				}
				$acfValue = $featuresArray;
			} 
			

			// I need to figure out a way to add General Content Blocks here that are new! WTF
			if($acfName == "general_content_block_selector") {
				$contentBlocks = object_2_array($acfValue);

				$contentBlocksArray = array();

				foreach($contentBlocks as $contentBlock) {
					if($contentBlock) {
						$contentBlockTitle = $contentBlock['block']['post_title'];
						$contentBlockObj = get_page_by_title($contentBlockTitle, OBJECT, "general-block");
						if($contentBlockObj) {
							array_push($contentBlocksArray, $contentBlockObj->ID);
						}
					}
				}

					$acfValue = $contentBlocksArray;
			}




			if($acfName == "select_reviews" OR $acfName == "select_videos" OR $acfName == "product-category") {
	 	 } else {
	 	  update_field($acfName, $acfValue, $syncPageID);
	 	 }
		 }
		 
	

	 }
					
		}
				  return true;
}

function updatePages($pages) {

	foreach ($pages as $page) {
		//LOOK ON CORE.PHP FOR updatePage Function
	 $pageUpdate = updatePage($page);

 }
}

function updateSlug($title, $slug, $postParent) {
	$page = get_page_by_title($title, OBJECT, 'page');
	$updatePage = wp_update_post(
		array (
			'ID'        => $page->ID,
			'post_name' => $slug,
			'post_parent' => $postParent
		)
	);
		return $updatePost;
}

 ?>