<?php 
//Sync Spas
function syncAcce($catNames) {
	ignore_user_abort(true);
  set_time_limit(0);
	$type = "product";
	$dsFilter = "product_cat";
	$catNames = fixArray($catNames);
  foreach ($catNames as $cName) {

	 $mainProducts = getContent($type,$dsFilter,$cName);
	 if ($cName == 'caldera-spas-accessories') {

		//DELETE 
		$slugs = array( 'monarch-phalkalinity-1-5-lb',
										'monarch-phalkalinity-1-25-lb',
										'monarch-cover-shield',
										'monarch-filter-cleaner-granules-2-lb',
										'monarch-chlorinating-granules',
										'monarch-instant-filter-cleaner-spray-16-oz',
										'monarch-calcium-hardness-increaser-32-oz',
										'monarch-defoamer-16-oz',
										'monarch-ph-alkalinity-down-1-5-lb',
										'monarch-ph-alkalinity-up-1-25-lb',
										'monarch-stain-scale-defense-16-oz',
										'monarch-cd-ozone-water-care-system',
										'everfresh-water-care-system',
	 						    );			
		deleteProductsBySlug($slugs);
		
	}
	if($cName == 'hot-spring-steps') {

		//DELETE 
		$slugs = array (
			'freshwater-mpscents-island-escape',
			'freshwater-mpscents-spring-shower',
			'freshwater-balanced-shock-4-in-1-blend',
			'freshwater-ph-balance-plus',
			'freshwater-sodium-bromide-solution',
			'freshwater-spa-shine',
			'freshwater-enzyme-clarifier',
			'freshwater-mps-tabs',
			'freshwater-mpscents-zen-garden',
			'freshwater-mpscents-citrus-blossom',
			'hot-spring-everwood-spa-steps'
		);
		deleteProductsBySlug($slugs);
		

	}

	if($cName == 'fantasy-spas-accessories') {
		
	}
	
	if($cName == 'freeflow-spas-accessories') {

	}

	 foreach( $mainProducts as $mainProduct ) {
		
		$mainProductID = $mainProduct->id;
		$mainTitle = $mainProduct->title->rendered;
		$mainSlug = $mainProduct->slug;
		$mainContent = $mainProduct->content->rendered;
		$mainExcerpt = $mainProduct->excerpt->rendered;
		$mainCategories = $mainProduct->categories; 
		
		//Lets find out if this product is in this website
		$siteProduct = get_product_by_slug($mainSlug);
		//If there is an ID which Means there is a product in this site with the same slug
		$productID = $siteProduct->ID;


		//echo $mainTitle . " - " . $productID . "<br>";
		
		if($productID) {
			//update this product with new stuff
			$productPost = array(
				'ID'           => $productID,
				'post_title'   => $mainTitle,
				'post_content' => $mainContent,
				'post_excerpt' => $mainExcerpt,
				);
				// Update the post into the database
				wp_update_post($productPost);
		} else {
			//create a product with the new stuff

			$author = "0";
			$term = $dsFilter;
			$taxonomy = array();
	 
			foreach ($mainCategories as $category) {
				$slug = $category->slug;
				$taxonomy[] = $slug;
			}

			$productID = createWPPostWithSlug($mainTitle,$mainSlug,$author,$mainContent,$mainExcerpt,$type);
			wp_set_object_terms($productID, $taxonomy, $term);
			// echo "created " . $mainTitle . "<br>";
		}

	//update ACFS
	if($productID != "") {

			//add featured image 
			$listID = "";
			//Get media to download from server
			$featuredMedia = $mainProduct->featured_media;
			$APIMediaURL = "http://staging.myproductdata.com/wp-json/wp/v2/media/";
			$featuredMediaURLAPI = $APIMediaURL . $featuredMedia;
			$response = wp_remote_get($featuredMediaURLAPI);
				if( is_wp_error( $response ) ) {
						echo $response;
						echo "<br>";
						echo "try again please!";
						die;
				} else {
			$media = json_decode( wp_remote_retrieve_body( $response ) );
			$mainImage = $media->source_url;
			}
			//Lets check if we need to add images to the gallery,
			 	 
			$getGalleryURLS = "http://staging.myproductdata.com/wp-json/product_gallery/v1/product/";
			$galleryAPI = $getGalleryURLS .$mainProductID;
			$response = wp_remote_get($galleryAPI);
			 if( is_wp_error( $response ) ) {
					 echo $response;
					 echo "<br>";
					 echo "try again please!";
					 die;
			 } else {
	 			$galleryMedia = json_decode( wp_remote_retrieve_body( $response ) );
	  		 if($galleryMedia) {
			    foreach ($galleryMedia as $galleryImg) {
					//$galleryImg image url from api upload it to media gallery and get id
					$imgID = addImg($galleryImg);
	 				$listID .= $imgID . ",";
			    }
				$listID = substr($listID, 0, -1);
				//var_dump($listID);
				update_post_meta($productID, '_product_image_gallery', $listID);
				 
				//check if there is a post meta already if not make one
				$productGallery = get_post_meta($productID, '_product_image_gallery', true );
				if($productGallery) {
			 	}	else {
			 		add_post_meta($productID,'_product_image_gallery',$listID);
				} 
		 	}
	 }
	  if($mainImage){
		 addFeaturedImg($mainImage,$productID);
	  }

		//new way to update acfs 
		//get acfs from restapi 
		$acfs = object_2_array($mainProduct->acf);
				if($acfs) {
				foreach ($acfs as $acfName => $acfValue) {
					if($acfName == "jet-selection") {
						$acfValue = object_2_array($acfValue);
					}
					if($acfName == "reviews_code_embed") {
						$acfValue = str_replace("<p>", "", $acfValue);
						$acfValue = str_replace("</p>", "", $acfValue);
					}
					if($acfName == "features") {
						$features = object_2_array($acfValue);
						$featuresArray = [];
						foreach ($features as $feature) {
							if(is_array($feature)) {
							 $featTitle = $feature['post_title'];
							//get post id 
							$featureObj = get_page_by_title( $featTitle, OBJECT, "features" );
							array_push($featuresArray, $featureObj->ID);
							}
						}
						$acfValue = $featuresArray;
					} 

					//special EF ACFS 

					if($acfName == "add_images") {
						$images = object_2_array($acfValue);
						$imgIDs = [];
						foreach ($images as $image) {
							$imgID = addImg($image['url']);
							array_push($imgIDs, $imgID);
						}
						$acfValue = $imgIDs;
					}
					if($acfName == "visual_list") {
						$acfContents = object_2_array($acfValue);
						$acfArray = [];
						foreach($acfContents as $acfContent) {
							$acfCs = object_2_array($acfContent);
							$acfCArray = [];
							foreach($acfCs as $acfN => $acfV) {
								if($acfN == "vl_image") {
									$imgID = addImg($acfV);
									$acfV = $imgID;
								}
								$acfCArray[$acfN] = $acfV;
							}
							array_push($acfArray, $acfCArray);
						}
						$acfValue = $acfArray;
					}
					if($acfName == "col_aspot_image") {
						$imgID = addImg($acfValue);
						$acfValue = $imgID; 
					}

					update_field($acfName, $acfValue, $productID);
				}
			}


	  }

	 }

}
echo ' 
<div class="alert alert-success" role="alert">
Step Accessories: '.$cName.' has been done!
</div>
';
}

 function get_product_by_slug($product_slug, $output = OBJECT, $post_type = 'product' ) { 
  global $wpdb; 
   $page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s AND post_status = 'publish'", $product_slug, $post_type ) ); 
     if ( $page ) 
        return get_post($page, $output); 
    return null; 
	}
?>