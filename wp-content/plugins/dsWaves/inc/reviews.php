<?php 
//SYNC REVIEWS
function syncReviews() {
	//Lets just worry about getting the latest reviews which are from HOT SPRING
	$type = "reviews";
	$host = "http://staging.myproductdata.com/wp-json/wp/v2/";
	$didItWork = false;
	
	//add or update reviews_cat
$wpdocs_cat = array('taxonomy' => 'reviews_cat', 'cat_name' => 'Pace', 'category_description' => 'Pace', 'category_nicename' => 'pace', 'category_parent' => '17');
$paceID = wp_insert_category($wpdocs_cat);
$wpdocs_cat2 = array('taxonomy' => 'reviews_cat', 'cat_name' => 'Stride', 'category_description' => 'Stride', 'category_nicename' => 'stride', 'category_parent' => '17');
$strideID = wp_insert_category($wpdocs_cat2);
$wpdocs_cat3 = array('cat_ID' => 37, 'taxonomy' => 'reviews_cat', 'cat_name' => 'Jetsetter LX', 'category_description' => 'Jetsetter LX', 'category_nicename' => 'jetsetter-lx', 'category_parent' => '14');
$wpdocs_cat_id3 = wp_insert_category($wpdocs_cat3);


	$reviewsIDs = ['1024692',
	'1024688',
	'1024550',
	'1024549',
	'1024548',
	'1024547',
	'1024546',
	'1024545',
	'1024544',
	'1024543',
	'1024542',
	'1024541',
	'1024540',
	'1024539',
	'1024538',
	'1024537',
	'1024535',
	'1024532',
	'1024533',
	'1024531',
	'1024529',
	'1024468',];

	
	//loop over the ids and update each one
	foreach($reviewsIDs as $reviewID) {

		$response = wp_remote_get($host.$type.'/'.$reviewID);
		if( is_wp_error( $response ) ) {
			echo $response->get_error_message();
				echo "<br>";
				echo "try again please!";
				die;
		}
		$post = json_decode( wp_remote_retrieve_body( $response ) );
		$postTitle = html_entity_decode($post->title->rendered);
		if($post->content->rendered) {
			$content = $post->content->rendered;	
		} else {
			$content = " ";
		}
		$excerpt = " ";
		$pageSlug = $post->slug;
		$pageID = $post->id;
		$type = html_entity_decode($type);
		$author = "0";
		  $postObject = get_page_by_slug_feat($pageSlug);
		  $date = new DateTime();
		  $date->add(DateInterval::createFromDateString('yesterday'));
		  $contentModified = $date->format('Y-m-d') . "\n";
		  if ($postObject) {

		  } else {
			 //create review 	
			 if($postTitle) {
						$pageID = createWPPost($postTitle,$author,$content,$excerpt,$type);
						$reviewCats = array();
						foreach($post->reviews_cat as $reviewCat) {
							if($reviewCat == 2176) {
								$reviewCat = $paceID;
							}
							if($reviewCat == 2053) {
								$reviewCat = $strideID;
							}

							$reviewCats[] = $reviewCat;

							// echo $reviewCat;
						}

					//  print_r($post->reviews_cat);

						wp_set_object_terms($pageID, $reviewCats, 'reviews_cat');
						wp_set_post_tags($pageID, $post->tags);

					}	  
		  }
		  //update acfs
		  if($post->acf) {
			  $acfs = object_2_array($post->acf);
			  foreach ($acfs as $acfName => $acfValue) {
				  update_field($acfName, $acfValue, $pageID);
			  }
			}
			$didItWork = true;	
	}

	echo ' 
	<div class="alert alert-success" role="alert">
	 Reviews have been updated!
	</div>
	';
}


?>