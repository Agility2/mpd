<?php 
//SYNC JETS
function syncJets($cName) {

    $author = "1";
	$content = "Jets";
	$except = "Jets";
	$type = "jets";
	$term = "jets_cat";
	$dsType = "jets-api";
	$sync = "manual";

		$posts = getContent($dsType,$term,$cName);
			

		//Lets delete some jets
		$jet1 = get_page_by_title('Sole Soothers® Jets', OBJECT, 'jets');
		$jet2 = get_page_by_title('UltraMassage®', OBJECT, 'jets');
		$jet3 = get_page_by_title('Euro Jets', OBJECT, 'jets');
		$jet4 = get_page_by_title('AdaptaFlo® Jet', OBJECT, 'jets');
		$jet5 = get_page_by_title('AdaptaSsage® Jet', OBJECT, 'jets');
		$jet6 = get_page_by_title('VersaSsage® Jet', OBJECT, 'jets');
		$jet7 = get_page_by_title('Directional Precision Jets', OBJECT, 'jets');

		wp_delete_post($jet1->ID, true);
		wp_delete_post($jet2->ID, true);
		wp_delete_post($jet3->ID, true);
		wp_delete_post($jet4->ID, true);
		wp_delete_post($jet5->ID, true);
		wp_delete_post($jet6->ID, true);
		wp_delete_post($jet7->ID, true);
		


            foreach( $posts as $post ) {
                $postTitle = $post->title->rendered;
				$postID = $post->id;
				$content = $post->content->rendered;
				$excerpt = $post->excerpt->rendered;
                //Check if post exists
                if (post_exists($postTitle) ) {
					// echo "Updated: " . $postTitle . "<br>";
				} else {
					 //if its new create it!
			     	 $postID = createWPPost($postTitle,$author,$content,$except,$type);
					 wp_set_object_terms($postID, $cName, $term);
					//  echo "Created: " . $postTitle . "<br>";
				}
				if($post->acf) {
					$acfs = object_2_array($post->acf);
					if($acfs) {
					  foreach ($acfs as $acfName => $acfValue) { 
						  	update_field($acfName, $acfValue, $postID);
					  } 
					}
            	} 
		}
		echo ' 
		<div class="alert alert-success" role="alert">
		 Step 2: Jets have been updated!
		</div>
		';
}
?>