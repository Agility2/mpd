<?php 
//SYNC Promotions
function syncPromos($cName) {


    $type = "promotion";
    $author = "0";
		$dsFilter = "tag";

				  $posts = getContent($type,$dsFilter,$cName);

            foreach( $posts as $post ) {

				$postTitle = $post->title->rendered;
				$postContent = $post->content->rendered;
				$postID = $post->id;
				$postSlug = $post->slug;
								$tags = $post->tags;
								$excerpt = "";

                //Check if post exists
                if ( get_page_by_slug_promo($postSlug) ) {
					
									wp_set_post_terms($postID, $tags, 'post_tag', false);
									
							$promoPost = array(
								'ID'           => $postID,
								'post_title'   => $postTitle,
								'post_content' => $postContent,
								'post_excerpt' => $excerpt,
							);
			
							// Update the post into the database
							wp_update_post( $promoPost );


                } else {

                    //if its new create it!
					$excerpt = "";
					$pageID = createWPPost($postTitle,$author,$postContent,$excerpt,$type);

            		wp_set_post_terms($pageID, $tags, 'post_tag', false);

				}
				
						 //Get media to download from server
						 $featuredMedia = $post->featured_media;
						 $APIMediaURL = "https://myproductdata.com/wp-json/wp/v2/media/";
						 $featuredMediaURLAPI = $APIMediaURL . $featuredMedia;
						 $response = wp_remote_get($featuredMediaURLAPI);
						 				 
							 if( is_wp_error( $response ) ) {
									 echo $response;
									 echo "<br>";
									 echo "try again please!";
									 die;
							 }
					 
						$media = json_decode( wp_remote_retrieve_body( $response ) );
						$mainImage = $media->guid->rendered;
						 addFeaturedImg($mainImage,$pageID);

								//UPDATE ACFs
								if($post->acf) {
								//field #s
								$promotionImgPositionField = 'field_56c6244c97b38'; //Select Field
								$promotionStartDateField = 'field_56c7aa5fdc54a'; //Date picker field
								$promotionEndDateField = 'field_56c7aa83dc54b'; //Date picker field
								$promotionHideField = 'field_5730d405a0cec'; //true/false field

								//ACFS VALUES FROM MASTER
								$promotionImgPositionValue = $post->acf->{'promo_image_position'};
								$promotionStartDateValue = $post->acf->{'promo_start_date'};
								$promotionEndDateValue = $post->acf->{'promo_end_date'};
								$promotionHideValue = $post->acf->{'hide_promo_dates'};


								update_field($promotionImgPositionField, $promotionImgPositionValue, $pageID);
								update_field($promotionStartDateField, $promotionStartDateValue, $pageID);
								update_field($promotionEndDateField, $promotionEndDateValue, $pageID);
								update_field($promotionHideField, $promotionHideValue, $pageID);

								}

      }

	  echo ' 
	  <div class="alert alert-success" role="alert">
		  Promotions have been updated!
	  </div>
	  ';
		}
?>