<?php 

//Sync Spas
function syncProds($catNames) {
	//lets delete one product Tempo
	// wp_delete_post(1001547, true);

	ignore_user_abort(true);
 set_time_limit(0);

	$type = "product";
	$dsFilter = "product_cat";
	
	$catNames = fixArray($catNames);



foreach ($catNames as $cName) {
$didItWork = false;

switch ($cName) {
	case 'ep-fitness-systems':
	$posts = getContent($type,$dsFilter,$cName);
	//  deleteContent($cName);
	break;
	case 'ep-recsport-recreation-systems':
	$posts = getContent($type,$dsFilter,$cName);
	//  deleteContent($cName);
	break;
	case 'ep-swimcross-exercise-systems':
	$posts = getContent($type,$dsFilter,$cName);
	//  deleteContent($cName);
	//delete x2000 
		$slugs = array('x200-swimcross-exercise-systems');
		deleteProductsBySlug($slugs);
	

	break;
	default:
	$posts = getContent($type,$dsFilter,$cName);
}


	foreach( $posts as $post ) {

			$postTitle = $post->title->rendered;
			$postSlug = $post->slug;
		  	$content = $post->content->rendered;
			$excerpt = $post->excerpt->rendered;
			$mainCategories = $post->categories; 
			$syncIDLabel = "NO";
			$xID = "NO";

 if ($cName == 'utopia') {
	  $syncIDLabel = "syncIDCS";
	  $xID = $post->syncIDCS;
 } 
 if ($cName == 'paradise') {
	$syncIDLabel = "syncIDCS";
	$xID = $post->syncIDCS;
} 
if ($cName == 'vacanza') {
	$syncIDLabel = "syncIDCS";
	$xID = $post->syncIDCS;
} 

//  if ($cName ==  'highlife-nxt' ) {
// 	//  $syncIDLabel = "syncIDHS";
// 	//  $xID = $post->syncIDHS;
// } 
 if ($cName ==  'highlife' ) {
	$syncIDLabel = "syncIDHS";
	$xID = $post->syncIDHS;
} 
if ($cName ==  'limelight' ) {
	$syncIDLabel = "syncIDHS";
	$xID = $post->syncIDHS;
} 
if ($cName ==  'hotspot' ) {
	$syncIDLabel = "syncIDHS";
	$xID = $post->syncIDHS;
	
	
} 

 if ($cName == 'fantasy-spas' ) {
	 $syncIDLabel = "syncIDFAS";
	 $xID = $post->syncIDFAS;
 } 

 if ($cName == 'freeflow-spas') {
	 $syncIDLabel = "syncIDFRS";
	 $xID = $post->syncIDFRS;
 }

 if ($cName == 'ep-swimcross-exercise-systems') {
	 $syncIDLabel = "syncIDEPFS";
	 $xID = $post->syncIDEPFS;
 }   
 if ($cName == 'ep-fitness-systems') {
	$syncIDLabel = "syncIDEPFS";
	$xID = $post->syncIDEPFS;
} 
if ($cName == 'ep-recsport-recreation-systems') {
	$syncIDLabel = "syncIDEPFS";
	$xID = $post->syncIDEPFS;
}     

if ($cName == 'finnleo') {
	$syncIDLabel = "syncIDFinnleo";
	$xID = '';
}

 //CHECK IF THE ITEMS ARE SUPPOSE TO BE SYNCED
if ($syncIDLabel == "NO") { 
	echo "Something is wrong... Couldn't find syncable products.";
	echo " cname: ".$cName." xID: ".$xID;
	echo "<br>";
	} else {

 $syncQueryArgs = array(
    'posts_per_page'   => -1,
    'post_type'        => 'product',
	'name' => $postSlug,
	'numberposts' => 1
);
$syncQuery = new WP_Query( $syncQueryArgs );




if( $syncQuery->have_posts() ) {
	while( $syncQuery->have_posts() ) {
	  $syncQuery->the_post();
		//Get Product ID
		$localPostTitle = get_the_title(); 
		$pageID = get_the_ID();

	
							$productPost = array(
							'ID'           => $pageID,
							'post_title'   => $postTitle,
							'post_content' => $content,
							'post_excerpt' => $excerpt,
						);
		
						
						// Update the post into the database
						wp_update_post( $productPost );
					//	echo "Update Product: " . $postTitle . " <br> ";
					$term = $dsFilter;
					$taxonomy = array();
	 
					foreach ($mainCategories as $category) {
						$slug = $category->slug;
						$taxonomy[] = $slug;
					}
					wp_set_object_terms($pageID, $taxonomy, $term);
	} // end while

} else {
	$postTitle = $post->title->rendered;
	$author = "0";
	$term = $dsFilter;
	$taxonomy = array();
	 
	foreach ($mainCategories as $category) {
		$slug = $category->slug;
		$taxonomy[] = $slug;
	}
	 //if its new create it!
	 $pageID = createWPPost($postTitle,$author,$content,$excerpt,$type);
	 wp_set_object_terms($pageID, $taxonomy, $term);
	 add_post_meta($pageID, $syncIDLabel, $xID, true);
	// echo "Created Product: " . $postTitle . " <br> ";
	//add featured image 
					
}
				
					//new way to update acfs 
						 //get acfs from restapi 
						 $acfs = object_2_array($post->acf);
						  if($acfs) {

							$fakeData = array(
								'name' => ' ',
								'value' => ' ',
						  );

						  add_row('field_5bc67764bf574', $fakeData, $pageID);
						   add_row('field_5ba131857a263', $fakeData, $pageID);
						   add_row('field_5ba13670ec410', $fakeData, $pageID);
						

							foreach ($acfs as $acfName => $acfValue) {
								if($acfName == "primary_specs") {
									$fieldKey1 = acf_get_field_key($acfName, $pageID);	
									add_row($fieldKey1, $fakeData, $pageID);
								}
								if($acfName == "additional_specs") {
									$fieldKey2 = acf_get_field_key($acfName, $pageID);	
									add_row($fieldKey1, $fakeData, $pageID);
								}
								if($acfName == "acc_products") {
									$accIds = array();
									$acfName = "field_55f1d17a56c8b";

										//lets check if this is freeflow 
										if($cName == "freeflow-spas") {

											// $accIds = [];
	
											// // Freeflow Spas® The Cool Step™
											// $product1 = get_page_by_title('Freeflow Spas® The Cool Step™', OBJECT, 'product');
											// // Freeflow Spas® Replacement Filters
											// $product2 = get_page_by_title('Freeflow Spas® Replacement Filters', OBJECT, 'product');
											// // Freeflow Spas® Ozone Kit
											// $product3 = get_page_by_title('Freeflow Spas® Ozone Kit', OBJECT, 'product');
											// // Freeflow Spas® Square Cover Lifter
											// $product4 = get_page_by_title('Freeflow Spas® Square Cover Lifter', OBJECT, 'product');

											// if( ($postTitle == "Tristar ™") || ($postTitle == "Aptos ®"))  {
											// 	$product4 = get_page_by_title('Freeflow Spas® Round/Triangle Cover Lifter', OBJECT, 'product');
											// }

											// $accIds[] .= $product1->ID;
											// $accIds[] .= $product2->ID;
											// $accIds[] .= $product3->ID;
											// $accIds[] .= $product4->ID;
											
	
										} //lets check if this is fantasy
										elseif ($cName == "fantasy-spas") {
	
											// $accIds = [];
											
											// // Fantasy Spas® The Cool Step™
											// $product1 = get_page_by_title('Fantasy Spas® The Cool Step™', OBJECT, 'product');
											// // Fantasy Spas® Replacement Filters
											// $product2 = get_page_by_title('Fantasy Spas® Replacement Filters', OBJECT, 'product');
											// // Fantasy Spas® Ozone Kit
											// $product3 = get_page_by_title('Fantasy Spas® Ozone Kit', OBJECT, 'product');
											// // Fantasy Spas® Square Cover Lifter
											// $product4 = get_page_by_title('Fantasy Spas® Square Cover Lifter', OBJECT, 'product');

									
											// if( ($postTitle == "Splendor ®") || ($postTitle == "Embrace ®"))  {
											// 	$product4 = get_page_by_title('Fantasy Spas® Round/Triangle Cover Lifter', OBJECT, 'product');
											// }

										
											// $accIds[] .= $product1->ID;
											// $accIds[] .= $product2->ID;
											// $accIds[] .= $product3->ID;
											// $accIds[] .= $product4->ID;
	
	
	
										} else {
											foreach ($acfValue as $accValue) {
												$accIds[] .= $accValue['ID'];
											}	
										}
	
										$acfValue = $accIds;

								}
								if($acfName == "jet-selection") {
									$acfValue = object_2_array($acfValue);
								}
								if($acfName == "reviews_code_embed") {
									$acfValue = str_replace("<p>", "", $acfValue);
									$acfValue = str_replace("</p>", "", $acfValue);
								}
								if($acfName == "features") {
									$features = object_2_array($acfValue);
									$featuresArray = [];
									foreach ($features as $feature) {
										$featTitle = $feature['post_title'];
										//get post id 
										$featureObj = get_page_by_title( $featTitle, OBJECT, "features" );
										// var_dump($featureObj);
										array_push($featuresArray, $featureObj->ID);
									}

									
									$acfValue = $featuresArray;
									//var_dump($acfValue);

								} 

								//special EF ACFS 

								if($acfName == "add_images") {
									$images = object_2_array($acfValue);
									$imgIDs = [];
									foreach ($images as $image) {
										$imgID = addImg($image['url']);
										array_push($imgIDs, $imgID);
									}
									$acfValue = $imgIDs;
								}
								if($acfName == "visual_list") {
									$acfContents = object_2_array($acfValue);
									$acfArray = [];
									foreach($acfContents as $acfContent) {
										$acfCs = object_2_array($acfContent);
										$acfCArray = [];
										foreach($acfCs as $acfN => $acfV) {
											if($acfN == "vl_image") {
												$imgID = addImg($acfV);
												$acfV = $imgID;
											}
											$acfCArray[$acfN] = $acfV;
										}
										array_push($acfArray, $acfCArray);
									}
									$acfValue = $acfArray;
								}
								if($acfName == "col_aspot_image") {
									$imgID = addImg($acfValue);
									$acfValue = $imgID; 
								}
			
								update_field($acfName, $acfValue, $pageID);
			
							}
						}

					
				}
     	}

		}
		if($cName == 'hotspot') {
			$cName = 'Hot Spring Spas';
		}
		if($cName == 'vacanza') {
			$cName = 'Caldera Spas';
		}

		echo ' 
<div class="alert alert-success" role="alert">
Step Products: '.$cName.' has been done!
</div>
';
 }

 ?>