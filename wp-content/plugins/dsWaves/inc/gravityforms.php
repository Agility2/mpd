<?php 

function syncGravityForms($cNames) {

 foreach ($cNames as $cName) {

  $forms = GFAPI::get_forms();

  foreach($forms as $form) {
    //Get Gravity Forms and edit the form depending on the CName which is the brand
    if($cName == "hot-spring-spas") {
      $products = array (
          array(
            'text' => 'Hot Spring - Jetsetter® - 3 Person',
            'value' => 'Hot Spring - Jetsetter® - 3 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Jetsetter® LX - 3 Person',
            'value' => 'Hot Spring - Jetsetter® LX - 3 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Triumph™ - 4 Person',
            'value' => 'Hot Spring - Triumph™ - 4 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Prodigy® - 5 Person',
            'value' => 'Hot Spring - Prodigy® - 5 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Sovereign® - 6 Person',
            'value' => 'Hot Spring - Sovereign® - 6 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Vanguard® - 6 Person',
            'value' => 'Hot Spring - Vanguard® - 6 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Aria® - 5 Person',
            'value' => 'Hot Spring - Aria® - 5 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Envoy® - 5 Person',
            'value' => 'Hot Spring - Envoy® - 5 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Grandee® - 7 Person',
            'value' => 'Hot Spring - Grandee® - 7 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Prism™ - 7 Person',
            'value' => 'Hot Spring - Prism™ - 7 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Pulse - 7 Person',
            'value' => 'Hot Spring - Pulse - 7 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Flash™ - 7 Person',
            'value' => 'Hot Spring - Flash™ - 7 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Flair - 6 Person',
            'value' => 'Hot Spring - Flair - 6 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Beam™ - 4 Person',
            'value' => 'Hot Spring - Beam™ - 4 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - TX - 2 Person',
            'value' => 'Hot Spring - TX - 2 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - SX - 3 Person',
            'value' => 'Hot Spring - SX - 3 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Stride™ - 3 Person',
            'value' => 'Hot Spring - Stride™ - 3 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Pace™ - 5 Person',
            'value' => 'Hot Spring - Pace™ - 5 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Relay - 6 Person',
            'value' => 'Hot Spring - Relay - 6 Person',
            'isSelected' => false,
            'price' => ''
          ),
          array(
            'text' => 'Hot Spring - Rhythm - 7 Person',
            'value' => 'Hot Spring - Rhythm - 7 Person',
            'isSelected' => false,
            'price' => ''
          )
        );
        //download a brochure furshure dude
        if($form['title'] == "Download Brochure - Hot Spring Spas") {
            $choices = GFAPI::get_field( $form, 59);
            $choices->choices = $products;
        }
        //get pricing form
        if($form['title'] == "Get Pricing - Hot Spring Spas") {
            $choices = GFAPI::get_field( $form, 63);
            $choices->choices = $products;
        }
    }
    if($cName == "caldera-spas") {
        $products = array(
            array(
                'text' => 'Caldera Spas - Cantabria - 8 Person',
                'value' => 'Caldera Spas - Cantabria - 8 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Geneva - 6 Person',
                'value' => 'Caldera Spas - Geneva - 6 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Niagara - 7 Person',
                'value' => 'Caldera Spas - Niagara - 7 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Tahitian - 6 Person',
                'value' => 'Caldera Spas - Tahitian - 6 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Florence™ - 6 Person',
                'value' => 'Caldera Spas - Florence™ - 6 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Provence™ - 4 Person',
                'value' => 'Caldera Spas - Provence™ - 4 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Makena - 6 Person',
                'value' => 'Caldera Spas - Makena - 6 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Salina - 7 Person',
                'value' => 'Caldera Spas - Salina - 7 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Reunion™ - 7 Person',
                'value' => 'Caldera Spas - Reunion™ - 7 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Seychelles™ - 6 Person',
                'value' => 'Caldera Spas - Seychelles™ - 6 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Martinique - 5 Person',
                'value' => 'Caldera Spas - Martinique - 5 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Kauai - 3 Person',
                'value' => 'Caldera Spas - Kauai - 3 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Palatino - 6 Person',
                'value' => 'Caldera Spas - Palatino - 6 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Marino - 6 Person',
                'value' => 'Caldera Spas - Marino - 6 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Vanto - 7 Person',
                'value' => 'Caldera Spas - Vanto - 7 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Tarino - 5 Person',
                'value' => 'Caldera Spas - Tarino - 5 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Celio™ - 3 Person',
                'value' => 'Caldera Spas - Celio™ - 3 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Caldera Spas - Aventine - 2 Person',
                'value' => 'Caldera Spas - Aventine - 2 Person',
                'isSelected' => false,
                'price' => ''
            )
        );
       
     //download a brochure furshure dude
     if($form['title'] == "Download Brochure - Caldera Spas") {
        $choices = GFAPI::get_field( $form, 20);
         $choices->choices = $products;
    }
    //get pricing form
    if($form['title'] == "Get Pricing - Caldera Spas") {
        $choices = GFAPI::get_field( $form, 21);
        $choices->choices = $products;
    }

    }
    if($cName == "freeflow-spas") {
         //the choices is an array ex below
        
        $newProducts = array( 
            array(
                'text' => 'Freeflow Spas - Mini - 2 Person',
                'value' => 'Freeflow Spas - Mini - 2 Person',
                'isSelected' => false,
                'price' => ''
            ), 
            array(
                'text' => 'Freeflow Spas - Tristar - 3 Person',
                'value' => 'Freeflow Spas - Tristar - 3 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Freeflow Spas - Cascina - 4 Person',
                'value' => 'Freeflow Spas - Cascina - 4 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Freeflow Spas - Azure - 4 Person',
                'value' => 'Freeflow Spas - Azure - 4 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Freeflow Spas - Azure Premier - 4 Person',
                'value' => 'Freeflow Spas - Azure Premier - 4 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Freeflow Spas - Aptos - 5 Person',
                'value' => 'Freeflow Spas - Aptos - 5 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Freeflow Spas - Excursion - 5 Person',
                'value' => 'Freeflow Spas - Excursion - 5 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Freeflow Spas - Excursion Premier - 5 Person',
                'value' => 'Freeflow Spas - Excursion Premier - 5 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Freeflow Spas - Monterey - 7 Person',
                'value' => 'Freeflow Spas - Monterey - 7 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Freeflow Spas - Monterey Premier - 7 Person',
                'value' => 'Freeflow Spas - Monterey Premier - 7 Person',
                'isSelected' => false,
                'price' => ''
            )
            
         );
        
        //download a brochure furshurdude
        if($form['title'] == "Download Brochure - Freeflow Spas") {
            $choices = $form['fields'][5]->choices;
             $form['fields'][5]->choices = $newProducts;
        }
        //get pricing form
        if($form['title'] == "Get Pricing - Freeflow Spas") {
            $choices = $form['fields'][5]->choices;
            $form['fields'][5]->choices = $newProducts;
        }
    }
    if($cName == "fantasy-spas") {

        $newProducts = array( 
            array(
                'text' => 'Fantasy Spas - Aspire - 2 Person',
                'value' => 'Fantasy Spas - Aspire - 2 Person',
                'isSelected' => false,
                'price' => ''
            ), 
            array(
                'text' => 'Fantasy Spas - Embrace - 3 Person',
                'value' => 'Fantasy Spas - Embrace - 3 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Fantasy Spas - Drift - 4 Person',
                'value' => 'Fantasy Spas - Drift - 4 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Fantasy Spas - Enamor - 4 Person',
                'value' => 'Fantasy Spas - Enamor - 4 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Fantasy Spas - Splendor - 5 Person',
                'value' => 'Fantasy Spas - Splendor - 5 Person',
                'isSelected' => false,
                'price' => ''
            ),
            array(
                'text' => 'Fantasy Spas - Entice - 5 Person',
                'value' => 'Fantasy Spas - Entice - 5 Person',
                'isSelected' => false,
                'price' => ''
            )
         );

             //download a brochure furshurdude
        if($form['title'] == "Download Brochure - Fantasy Spas") {
            $choices = $form['fields'][5]->choices;
             $form['fields'][5]->choices = $newProducts;
        }
        //get pricing form
        if($form['title'] == "Get Pricing - Fantasy Spas") {
            $choices = $form['fields'][5]->choices;
            $form['fields'][5]->choices = $newProducts;
        }


    }
    if($cName == "endless-pools") {
         $newProducts = array( 
            array(
                'text' => 'R200',
                'value' => 'R200',
                'isSelected' => false,
                'price' => ''
            ), 
            array(
                'text' => 'R500',
                'value' => 'R500',
                'isSelected' => false,
                'price' => ''
            ),
            
         );


       //download a brochure furshurdude
        if($form['title'] == "Download Brochure - Endless Pools") {
            $choices = $form['fields'][8]->choices;
             $form['fields'][8]->choices = $newProducts;
        }
        //get pricing form
        if($form['title'] == "Get Pricing - Endless Pools") {
            $choices = $form['fields'][8]->choices;
            $form['fields'][8]->choices = $newProducts;
        }

    }
    
    $result = GFAPI::update_form($form);
  }

 }


 echo ' 
 <div class="alert alert-success" role="alert">
             Step 7: Gravity Forms have been updated!
 </div>';

}