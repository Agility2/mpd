<?php 

function syncGCBS($cName) {
	$sync = "manual";
	$type = "general-block";
	$host = "http://staging.myproductdata.com/wp-json/wp/v2/";
	$ItemIDS = [];
	$didItWork = false;

	//check that category and get the ids
	switch ($cName) {
		case 'endless-pools-fitness-systems':
			$itemIDS = ["1006277", "1006312", "1006322", "1006337", "1012360", "1023222"];
		break;
		case 'hot-spring':
			// $itemIDS = ["1000770", "1000786", "1020395","1006977","1006587", "1006584", "1006583", "1006582", "1006581", "1000913", "1000912", "1000901", "1000896", "1000889", "1000770", "1000786", "1000900", "1000905", "1000908"];
			$itemIDS = ["1020395"];
		break;
		case 'caldera-spas':
		$itemIDS = ["1021011", "1000770", "1000786"];
		break;
		case 'finnleo-saunas': 
			$itemIDS = [
				"1024425",
				"1024423",
				"1024422",
				"1024421",
				"1024420",
				"1024419",
				"1024418",
				"1024417",
				"1024416",
				"1024415",
				"1024414",
				"1024413",
				"1024412",
				"1024411",
				"1024410",
				"1024409",
				"1024408",
				"1024407",
				"1024406",
				"1024405",
				"1024404",
				"1024403",
				"1024402",
				"1024401",
				"1024400",
				"1024399",
				"1024398",
				"1024397",
				"1024396",
				"1024395",
				"1024394",
				"1024392"
			];
			break;
	}
	//loop over the ids and update each one
	foreach($itemIDS as $itemID) {
		$response = wp_remote_get($host.$type.'/'.$itemID);
		if( is_wp_error( $response ) ) {
			echo $response->get_error_message();
				echo "<br>";
				echo "try again please!";
				die;
		}
		$post = json_decode( wp_remote_retrieve_body( $response ) );

		$postTitle = html_entity_decode($post->title->rendered);
		$content = $post->content->rendered;
		$excerpt = $post->excerpt->rendered;
		$pageSlug = $post->slug;
		$pageID = $post->id;
		$type = html_entity_decode($type);
		$author = "0";
		$date = new DateTime();
		$date->add(DateInterval::createFromDateString('yesterday'));
		$contentModified = $date->format('Y-m-d') . "\n";

		  $postObject = get_page_by_slug($pageSlug);
		  //var_dump($postObject);
		  if ($postObject) {
			  $pageID = $postObject->ID;
			
	  } else {
					  //create GCB 	
					  $content = " ";
					  $excerpt = " ";
					  $pageID = createWPPost($postTitle,$author,$content,$excerpt,$type);
					//echo "Created: " . $postTitle . " - ".$pageID."<br>";
					  
		  }
		 

			$didItWork = true;
			//echo "Updated: " . $postTitle . " " . $pageID . "<br>";	
		  //update acfs
		  if($post->acf) {
			  $acfs = object_2_array($post->acf);
			  foreach ($acfs as $acfName => $acfValue) {
				  if($acfName == "gb_header_background_image") {
					  if(!empty($acfValue)) {
						  $imgID = addImg($acfValue);
						  $acfValue = $imgID;
					  }
				  }
				  if($acfName == "block_1_background_image") {
					if(!empty($acfValue)) {
						$imgID = addImg($acfValue);
						$acfValue = $imgID;
					}
				}
				if($acfName == "block_2_background_image") {
					if(!empty($acfValue)) {
						$imgID = addImg($acfValue);
						$acfValue = $imgID;
					}
				}
				if($acfName == "block_1_image") {
					if(!empty($acfValue)) {
						$imgID = addImg($acfValue);
						$acfValue = $imgID;
					}
				}
				if($acfName == "block_2_image") {
					if(!empty($acfValue)) {
						$imgID = addImg($acfValue);
						$acfValue = $imgID;
					}
				}
				  update_field($acfName, $acfValue, $pageID);
			  }
		  }
	

	}


	if($didItWork) {
		echo ' 
<div class="alert alert-success" role="alert">
			Step General Content Blocks: '.$cName.' have been updated!
</div>';
} else {
	echo ' 
	<div class="alert alert-warning" role="alert">
				Step General Content Blocks: '.$cName.' did not need an update
	</div>';			
}

}
?>