<?php
//Get Core Functions 
include_once "inc/core.php";
//Get Images Fuctions
include_once "inc/img.php";
//Get Sync Ids
include_once "inc/syncids.php";
//Get Features Functions
include_once "inc/features.php";
//Get GCBS Functions
include_once "inc/gcbs.php";
//Get Jets Fuctions
include_once "inc/jets.php";
//Get Accessories Functions
include_once "inc/accessories.php";
//Get Products Functions
include_once "inc/products.php";
//Get Brands Functions
include_once "inc/brands.php";
//Get Promotions Functions
include_once "inc/promotions.php";
//Get Pages Functions
include_once "inc/pages.php";
//Get Reviews Functions
include_once "inc/reviews.php";
//Get Gravity Forms Functions
include_once "inc/gravityforms.php";