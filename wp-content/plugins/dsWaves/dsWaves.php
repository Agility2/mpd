<?php
/**
* Plugin Name:DS Waves NA
* Plugin URI: http://DesignStudio.com/
* Description: Syndication Platform for WordPress.
* Version: 1.9.8 (Finnleo)
* Author: DesignStudio
* Author URI: http://DesignStudio.com/
* License: GPL12
*/

require_once 'functions.php';

function dsWaves() {
  add_menu_page('DS Waves Menu', 'DS Waves', 'manage_options', 'ds-waves-page', 'dsWavesPage', plugin_dir_url( __FILE__ ) . 'assets/img/icons/dsWaves.png');
} add_action('admin_menu', 'dsWaves');

function dsWavesPage() { ?>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/css/style.css" rel="stylesheet">
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/js/jquery.min.js"></script>

    <div id="waves" class="container">
    <div class="wavesCard">
    <div class="contain">
    <h2 style="text-align:center; padding-top:15px; margin-bottom:25px;"><strong>DesignStudio </strong><span>Waves 1.9.8 (Finnleo)</span></h2>

  <div class="row row-6">
    <div class="dot dot-5"></div>
    <div class="dot dot-4"></div>
    <div class="dot dot-3"></div>
    <div class="dot dot-2"></div>
    <div class="dot dot-1"></div>
    <div class="dot dot-0"></div>
  </div>
  <div class="row row-5">
    <div class="dot dot-4"></div>
    <div class="dot dot-3"></div>
    <div class="dot dot-2"></div>
    <div class="dot dot-1"></div>
    <div class="dot dot-0"></div>
  </div>
  <div class="row row-4">
    <div class="dot dot-3"></div>
    <div class="dot dot-2"></div>
    <div class="dot dot-1"></div>
    <div class="dot dot-0"></div>
  </div>
  <div class="row row-3">
    <div class="dot dot-2"></div>
    <div class="dot dot-1"></div>
    <div class="dot dot-0"></div>
  </div>
  <div class="row row-2">
    <div class="dot dot-1"></div>
    <div class="dot dot-0"></div>
  </div>
  <div class="row row-1">
    <div class="dot dot-0"></div>
  </div>
</div>
 <?php
        $sync = "manual";
      //Sync ACFS
      if(isset($_POST['acfSubmit'])) { syncAllACF(); }
      //Sync Features
      if(isset($_POST['syncFeatsSubmit'])) {$cName = $_POST['featCat']; syncFeats($cName); }
      //Sync GCBS
      if(isset($_POST['syncGCBSSubmit'])) {$cName = $_POST['brand']; syncGCBS($cName); }
      //Sync Jets
      if(isset($_POST['syncJetsSubmit'])) { $cName = $_POST['jetCat']; syncJets($cName); }
      //Sync Accessories
      if(isset($_POST['syncAcceSubmit'])) { $catNames = $_POST['brand']; syncAcce($catNames); }
      //Sync Products
      if(isset($_POST['syncProdsSubmit'])) { $catNames = $_POST['brand']; syncProds($catNames); }
      //Sync Products IDs
      if(isset($_POST['syncProductsIDs'])) { $cName = $_POST['brand']; syncProductsIDs($cName); }
      //Sync Pages
      if(isset($_POST['syncPagesSubmit'])) { $cName = $_POST['brand']; syncPages($cName); }
      //SyncPromos
      if(isset($_POST['syncPromoSubmit'])) { $cName = $_POST['brand']; syncPromos($cName); }
      //SyncBrand
      if(isset($_POST['syncBrandsSubmit'])) { $cName = $_POST['brand']; syncBrands($cName); }
      //Update Reviews
      if(isset($_POST['UDReviewsSubmit'])) { syncReviews(); }
      //update Gravity Forms
      if(isset($_POST['updateGFSubmit'])) { $cNames = $_POST['updateGF']; syncGravityForms($cNames); }

    ?>

<style>
  .autoSyncMore {
    display:none;
  }
</style>
</div>
<br />

<div class="wavesCard">
<h4><strong>Step 1:</strong> Sync Brand</h4>
<p> This will delete old brand categories and update categories or create new ones</p>

<form method="post">
<select class="mdb-select colorful-select dropdown-dark brandSelect" name="brand">
    <option value="" disabled selected>Sync Products</option>
    <option value="finnleo-saunas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/finnleo-saunas.png" >Finnleo Saunas</option>
    </select>
      <button id="buttonBrand" type="submit" name="syncBrandsSubmit"></button>
    </form>
</div>
<br />


<!-- <div class="wavesCard">
  
    <h4><strong>Step 2: </strong>General Content Blocks </h4>
    <p>Update other blocks about products</p>

  <form method="post">
        <select class="mdb-select colorful-select dropdown-dark gcbSelect" name="brand">
        <option value="" disabled selected>Sync GCBS</option>
      <option value="finnleo-saunas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/finnleo-saunas.png" >Finnleo Saunas</option>
       </select>
        <button id="button3" type="submit" name="syncGCBSSubmit"></button>
    </form>
</div>
    <br> -->
    
    <div class="wavesCard">
    <h4><strong>Step 2: </strong>Pages </h4>
  <p>Update pages that are tied into a specific brand or create new ones</p>

      <form method="post">
<select class="mdb-select colorful-select dropdown-dark pagesSelect" name="brand">
    <option value="" disabled selected>Sync Pages</option>
      <option value="finnleo-saunas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/finnleo-saunas.png" >Finnleo Saunas</option>
     </select>
      <button id="buttonSyncPages" type="submit" name="syncPagesSubmit"></button>
    </form>
</div>
    <br>


    <div class="wavesCard">
    <h4><strong>Step 3: </strong>Accessories </h4>
    <p>Update accessories products or create new ones</p>

    <form method="post">
          <select class="mdb-select colorful-select dropdown-dark accSelect" name="brand">
          <option value="" disabled selected>Sync Accessories</option>
      <option value="finnleo-accessories" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/finnleo-saunas.png" >Finnleo Saunas</option>
      </select>
      <button id="button4" type="submit" name="syncAcceSubmit"></button>
    </form>
</div>
    <br>

    <div class="wavesCard">
  <h4><strong>Step 4: </strong>Products </h4>
  <p>Update products or create new ones</p>

      <form method="post">
<select class="mdb-select colorful-select dropdown-dark prodSelect" name="brand">
    <option value="" disabled selected>Sync Products</option>
      <option value="finnleo" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/finnleo-saunas.png" >Finnleo Saunas</option>
      </select>
      <button id="button" type="submit" name="syncProdsSubmit"></button>
    </form>
</div>
    <br>
    
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/js/mdb.min.js"></script>
    <script type="text/javascript" src="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/js/custom.js"></script>


<?php 
} ?>