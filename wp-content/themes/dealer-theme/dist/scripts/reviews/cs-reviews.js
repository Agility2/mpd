
  function BVHandleSummaryResults(jsonData) { 
   var ugcSummary = document.getElementById('UgcSummary_' + jsonData.subjectID); 
   if(ugcSummary) { 
    if(jsonData.totalReviews > 0 && jsonData.reviewsUrl.length > 0) {
     var readReviewStr = "Read <span itemprop='reviewCount'>" + jsonData.totalReviews + "</span> reviews"; 
     var averageRating = jsonData.averageRating.toFixed(2);
     var averageRatingSplit = averageRating.split('.');
     var averageRatingUrl = averageRatingSplit[0] + '_' + averageRatingSplit[1];
     var starRating = 'https://calderaspas.ugc.bazaarvoice.com/0501-en_us/'+ averageRatingUrl +'/5/rating.gif';
     ugcSummary.innerHTML += "<a class=\"fancybox\" href=" + jsonData.reviewsUrl + " title='Read Caldera Spas Reviews' target='_blank' style='color: #4E7682; text-decoration: none;'><span itemprop='aggregateRating' itemscope itemprop='http://schema.org/AggregateRating'>" +
       "<span itemprop='ratingValue'><img src="+ starRating +" alt="+ jsonData.averageRating.toFixed(2) +"  style='padding: 5px 0;border:none;' /></span>" +
       "<br>" +
       "Read <span itemprop='reviewCount'>" + jsonData.totalReviews + "</span> reviews</span></a>";   
    } 
    /*if(jsonData.totalQuestions > 0 && jsonData.questionsUrl.length > 0) { 
     var readQAStr = "Read " + jsonData.totalQuestions + " questions and " + jsonData.totalAnswers + " answers"; ugcSummary.innerHTML += "<br>" + readQAStr.link(jsonData.questionsUrl); 
    } */
   } 
  } 