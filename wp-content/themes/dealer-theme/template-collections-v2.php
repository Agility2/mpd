<?php
/**
 * Template Name: Collections v2
 */

$this_id        = get_the_ID();
$all_categories = []; // used to show only items from categories that exist in filter
$termID = get_field('main_category');
?>

<?php $hero_back = get_field( 'product_hero_image' ); ?>
<div class="archive-product-hero" style="background-image: url('<?php echo $hero_back; ?>')"></div>

<?php $links = get_field( 'links' ); ?>
<div class="archive-product-links">
	<div class="row d-row">
		<?php $links = array_chunk( $links, ceil( count( $links ) / 2 ) ); ?>
		<div class="columns large-4 small-12 small-order-2">
			<?php if ( ! empty( $links[0] ) ) : ?>
				<div class="archive-product-links-wrapper">
					<?php foreach ( $links[0] as $link ) : ?>
						<?php
						$link_alone      = $link["link"];
						if ( $link_alone ):
							$link_url = $link_alone['url'];
							$link_title  = $link_alone['title'];
							$link_target = $link_alone['target'] ? $link['target'] : '_self';
							?>
							<a href="<?php echo esc_url( $link_url ); ?>"
							   target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="columns large-4 small-12 small-order-1 text-center archive-product-links-image">
			<?php if ( get_field( 'center_image' ) ): ?>
				<img src="<?php the_field( 'center_image' ); ?>"/>
			<?php endif; ?>
		</div>
		<div class="columns large-4 small-12 small-order-3">
			<?php if ( ! empty( $links[1] ) ) : ?>
				<div class="archive-product-links-wrapper">
					<?php foreach ( $links[1] as $link ) : ?>
						<?php
						$link_alone      = $link["link"];
						if ( $link_alone ):
							$link_url = $link_alone['url'];
							$link_title  = $link_alone['title'];
							$link_target = $link_alone['target'] ? $link['target'] : '_self';
							?>
							<a href="<?php echo esc_url( $link_url ); ?>"
							   target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<div class="row">
	<div class="ds-filters-over"></div>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="columns small-12 large-3">
			<?php if ( have_rows( 'product_filter' ) ): ?>
				<div id="ds-filters" class="ds-filters">
					<div class="ds-filters__top show-for-medium-down">
						<h4><?php _e( 'Filter', 'dealer-theme' ); ?></h4>
						<span id="js-toggle-filters"><i class="fa fa-times" aria-hidden="true"></i></span>
					</div>
					<form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="ds-filter">
						<div id="accordion" class="accordion-container">
							<?php while ( have_rows( 'product_filter' ) ) : the_row();

								switch ( get_row_layout() ) {
									case "category":
										$categories = get_sub_field( 'categories' ); ?>
										<?php if ( $title = get_sub_field( 'title' ) ) : ?>
										<h4 class="article-title open"><?php echo $title; ?></h4>
									<?php endif; ?>
										<div class="accordion-content content-categories">
											<ul>
												<?php foreach ( $categories as $category ): ?>
													<?php
													if ( ! in_array( $category->term_id, $all_categories ) ):
														$all_categories[] = $category->term_id;
													endif;
													?>
													<li class="">
														<input name="<?php echo $category['category']->slug; ?>"
														       type="checkbox"
														       value="<?php echo $category['category']->term_id; ?>"
														       id="<?php echo $category['category']->slug; ?>">
														<label
																for="<?php echo $category['category']->slug; ?>"><?php echo $category['category']->name; ?></label>
														<?php

														$cat_args = array(
															'parent'     => $category['category']->term_id,
															'hide_empty' => false,
														);

														$terms_ch = get_terms( 'product_cat', $cat_args );
														if ( $category['show_child_categories'] && $terms_ch ) : ?>


															<ul class="filter-submenu">
																<?php foreach ( $terms_ch as $term_obj ):
																	$term_id = $term_obj->term_id;
																	$all_categories[] = $term_id;
																	if ( ! in_array( $term_id, $all_categories ) ):
																		$all_categories[] = $term_id;
																	endif;
																	?>
																	<li>
																		<input name="<?php echo $term_obj->slug; ?>"
																		       type="checkbox"
																		       value="<?php echo $term_id; ?>"
																		       id="<?php echo $term_obj->slug; ?>">
																		<label
																				for="<?php echo $term_obj->slug; ?>"><?php echo $term_obj->name; ?></label>
																	</li>
																<?php endforeach; ?>

															</ul>
														<?php endif; ?>
													</li>
												<?php endforeach; ?>
											</ul>
										</div>
									<?php
								}
							endwhile; ?>
						</div><!--/#accordion-->
						<div class="ds-filters__bottom show-for-medium-down">
							<input id="ds-filters-reset" type='reset' value='Clear' name='reset'>
							<button id="ds-filters-apply" class="apply-button"><?php _e( 'Apply', 'dealer-theme' ); ?></button>
						</div>
					</form>
				</div>
			<?php endif; ?>
		</div>
		<div class="columns small-12 large-9">
			<?php
			$arg = array(
				'post_type'      => 'product',
				'order'          => 'DESC',
				'orderby'        => 'menu_order',
				'posts_per_page' => 12,
				'tax_query'      => array( array(
					'taxonomy' => 'product_cat', // The taxonomy name
					'field'    => 'term_id', // Type of field ('term_id', 'slug', 'name' or 'term_taxonomy_id')
					'terms' => $termID
					) ),
			);

			$the_query = new WP_Query( $arg );
			if ( $the_query->have_posts() ) : ?>
				<div class="row flex-row" id="response" data-counter="<?php echo $the_query->found_posts; ?>"
				     data-categories="<?php echo json_encode( $all_categories ); ?>">
					<div class="columns small-12 ds-filters-nav">
						<div class="ds-filters-nav-right">
							<div class="ds-filters-name hide-for-medium-down">
								<span class="ds-filters-name__value"><?php _e( 'Finnleo Saunas', 'dealer-theme' ); ?></span>
							</div>
							<form id="ds-filters-search-wrap" class="" action="<?php echo esc_url( home_url( '/' ) ); ?>">
								<input type="search" name="ds-search" id="ds-filters-search" class="search__input"
								       placeholder="<?php _e( 'Search', 'dealer-theme' ); ?>"
								       value="<?php echo get_search_query(); ?>"/>
							</form>

							<button id="js-toggle-filters" class="ds-filters-toggle show-for-medium-down">
								<i class="fa fa-filter" aria-hidden="true"></i>
							</button>

							<!--							<select name="sort_by" id="ds-sort_by">-->
							<!--								<option value="" disabled selected>Sort by Price</option>-->
							<!--								<option value="price-asc" --><?php //echo $_POST['sort_by'] == 'price-asc' ? 'selected' : ''; ?><!-- >Low to High</option>-->
							<!--								<option value="price-desc" --><?php //echo $_POST['sort_by'] == 'price-desc' ? 'selected' : ''; ?><!-- >High to Low</option>-->
							<!--							</select>-->
						</div>

					</div>
					<?php while ( $the_query->have_posts() ) :
						$the_query->the_post(); ?>
						<?php $product = wc_get_product( get_the_ID() ); ?>
						<div class="columns small-12 medium-6 large-4">
							<div class="ds-product">
								<a href="<?php echo get_permalink() ?>">
									<?php if ( $product->is_on_sale() ): ?>
										<span class="ds-product__sale">Sale</span>
									<?php endif; ?>
									<span class="ds-product__image"
									      style="background-image: url(<?php echo get_field('single_product_image'); ?>)">
                                        </span>
									<span class="ds-product__title"><?php the_title(); ?></span>
								</a>
							</div>
						</div>
					<?php endwhile; ?>
					<div class="columns small-12 ds-filters-footer-nav">
						<div class="hide-for-medium-down">
							<?php $_POST['posts_per_page'] = '12'; ?>
							<ul id="ds-posts_per_page">
								<li class="<?php echo $_POST['posts_per_page'] == '12' ? 'selected' : ''; ?>">12</li>
								<li class="<?php echo $_POST['posts_per_page'] == '55' ? 'selected' : ''; ?>">55</li>
							</ul>
						</div>
						<div class="js-pagination">
							<?php foundation_pagination( $the_query ); ?>

						</div>
					</div>
				</div>
			<?php endif;
			wp_reset_query(); ?>
		</div>
	<?php endwhile; ?>

	<script>
			;
			(function ($) {
				var $body = $('body');

				$body.on('submit', '#ds-filter, #ds-filters-search-wrap', function () {
					dsFilter();
				});

				$body.on('change', '#ds-filter :checkbox, #ds-filter :radio, #ds-sort_by', function () {
					dsFilter();
				});

				$body.on('click', '#ds-posts_per_page li', function () {
					$('#ds-posts_per_page li').removeClass('selected');
					$(this).addClass('selected');
					dsFilter();
				});

				$body.on('click', '#ds-filters-reset, #ds-filters-apply', function () {
					setTimeout(function () {
						dsFilter();
						$('#ds-filters').fadeOut();
					}, 100);
				});

				$body.on('change', '#ds-filters-paged', function () {
					dsFilter($(this).val());
				});

				$body.on('click', '#js-toggle-filters', function () {
					$('#ds-filters').fadeToggle();
				});

				$body.on('click', '#toTop', function () {
					$("html, body").animate({scrollTop: 0}, 1000);
				});

				$('.article-title').on('click', function () {

					$(this).next().slideToggle(200);

					$(this).toggleClass('open');
				});

				$body.on('click', '.js-pagination a', function () {
					event.preventDefault();
					var urlThis = $(this).attr('href'),
						paged = '';
					if (urlThis.includes('admin-ajax.php')) {
						paged = urlThis.split('?paged=')[1];
					} else {
						var pagedStr = urlThis.split('/page/')[1];
						paged = pagedStr.split('/')[0];
					}
					dsFilter(paged, '');
				});

				var dsFilter = function (paged) {
					if (event) {
						event.preventDefault();
					}

					var filter = $('#ds-filter');

					// gather categories
					var categoriesCheckboxValue = "";
					$(".content-categories :checkbox").each(function () {
						var ischecked = $(this).is(":checked");
						if (ischecked) {
							categoriesCheckboxValue += $(this).val() + ",";
							console.log(categoriesCheckboxValue += $(this).val());
						}
					});

					if (categoriesCheckboxValue === '') {
						categoriesCheckboxValue = $('#response').data('categories');
						categoriesCheckboxValue = categoriesCheckboxValue + '';
					}

					// gather brands
					var brandsCheckboxValue = "";
					$(".content-brands :checkbox").each(function () {
						var ischecked = $(this).is(":checked");
						if (ischecked) {
							brandsCheckboxValue += $(this).val() + ",";
						}
					});

					// gather product use
					var prUseCheckboxValue = "";
					$(".content-product-use :checkbox").each(function () {
						var ischecked = $(this).is(":checked");
						if (ischecked) {
							prUseCheckboxValue += $(this).val() + ",";
						}
					});

					var sale = $('#sale_sale').is(':checked'),
						orderItem = $('#ds-sort_by'),
						orderBy = 'date',
						order = 'desc';

					if (orderItem.length > 0 && orderItem.val() !== null) {
						var orderStr = orderItem.val();
						orderBy = orderStr.split('-')[0];
						order = orderStr.split('-')[1];
					}

					var data = {
						action: 'ds_filter',
						posts_per_page: $('#ds-posts_per_page li.selected').text(),
						orderby: orderBy,
						order: order,
						categories: categoriesCheckboxValue,
						brands: brandsCheckboxValue,
						product_use: prUseCheckboxValue,
						price_min: $('#price_min').val(),
						price_max: $('#price_max').val(),
						sale: sale,
						this_id: <?php echo $this_id ?>
					}

					if (paged) {
						data.paged = paged;
					}

					var dsFiltersSearch = $('#ds-filters-search');

					if (dsFiltersSearch.val() !== '') {
						data.search = dsFiltersSearch.val();
					}

					$.ajax({
						url: filter.attr('action'),
						data: data,
						type: filter.attr('method'), // POST
						beforeSend: function (xhr) {
							$('.ds-filters-over').addClass('show');
						},
						success: function (data) {
							$('.ds-filters-over').removeClass('show');
							$('#response').html(data); // insert data

							// update value for product counter in filter section
							if ($('.ds-filters-counter.hide-for-medium-down').length) {
								$('.ds-filters-counter.show-for-medium-down').html($('.ds-filters-counter.hide-for-medium-down').html());
							} else {
								$('.ds-filters-counter.show-for-medium-down').html('No products found')
							}
						}
					});
					return false;
				}

				$(document).on('click', '.single_add_to_cart_button', function (e) {
					e.preventDefault();

					var $thisbutton = $(this),
						id = $thisbutton.val(),
						variation_id = $thisbutton.data('variation_id') || 0;

					var data = {
						action: 'woocommerce_ajax_add_to_cart',
						product_id: id,
						product_sku: '',
						quantity: 1,
						variation_id: variation_id,
					};

					$(document.body).trigger('adding_to_cart', [$thisbutton, data]);
					console.log(data);
					$.ajax({
						type: 'post',
						url: wc_add_to_cart_params.ajax_url,
						data: data,
						beforeSend: function (response) {
							$thisbutton.removeClass('added').addClass('loading');
						},
						complete: function (response) {
							$thisbutton.addClass('added').removeClass('loading');
						},
						success: function (response) {

							if (response.error & response.product_url) {
								window.location = response.product_url;
								return;
							} else {
								$(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);
							}
						},
					});

					return false;
				});

				$(document).on('ready', function () {
					$('.ds-filters .ds-filters-counter__value').html($('#response').data('counter'));
				});

			}(jQuery));
	</script>
</div>
