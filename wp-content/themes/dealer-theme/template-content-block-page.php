<?php
/**
 * Template Name: Content Blocks Page
 */
?>

<?php while (have_posts()) : the_post(); ?>
 
  <?php get_template_part('templates/content', 'general-block-page'); ?>
<?php endwhile; ?>
