<style>
    .header,
    .category-contact,
    .footer{ 
        display:none; 
    }
    .the360container{
        width: 100%;
        height: auto;
    }
    img.the360size{
        max-width: 785px;
    }
    #spin360imgs{
        margin: 0 auto;
        z-index: 100;
    }
</style>

<?php 
//get the images
$getTheImages = get_field('image_url_list');
$theImages = preg_replace('#\s+#',',',trim($getTheImages));
$theImages = explode(',' , $theImages);
?>
<h2 style="text-align:center; margin-top:1em;"><?php echo the_title(); ?> 360 View</h2>

<div class="the360container" style="position:relative">

    <div id='spin360imgs'></div>

    <img src="https://watkinsdealer.s3.amazonaws.com/Hot%20Tub%20Images/Caldera%20Spas/360%20Images/2016-AVENTINE/Vacanza-Aventine-360_0002_Layer%2070.jpg" alt="Sizer For Javascript" class="the360size" style="width:100%; height:auto; position:absolute; opacity:0; top:0;">

</div>

<!--
<div class="instructions">
    <p>Take a better look at by stopping the hottub at an agle by dragging left or right.</p>
</div>
-->

<script type="text/javascript">
        
jQuery(document).ready(function($){
    
    var the360Height = $('.the360size').height();
    var the360Width = $('.the360size').width();
    $('#spin360imgs').css({height:the360Height,width:the360Width});

    $("#spin360imgs").spritespin({
        // path to the source images.
        source: [
            <?php
                foreach($theImages as $image){
            ?>
                '<?php echo $image; ?>', 
            <?php
                }
            ?> 
        ],
        width: 800,
        height: 500,
    });
    
    $(window).resize(function(){
        var the360Height = $('.the360size').height();
        var the360Width = $('.the360size').width();
        $('#spin360imgs').css({height:the360Height,width:the360Width});
    });

});

</script>