<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Config;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }
  if ( isset( $post ) ) {
    $classes[] = $post->post_type . '-' . $post->post_name;
  }
  if ( !is_front_page() || !is_single() || !is_archive() || !is_home() ) {
    $classes[] = 'inside-page';
  }

  // Add class if sidebar is active
  if (Config\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a class="read-more" href="' . get_permalink() . '">' . __('Read More', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

//function favicon_ds(){
//  $html = '<link rel="shortcut icon" href="' . get_stylesheet_directory_uri() . '/dist/img/favicon.ico">';
//  echo $html;
//}
//add_action( 'wp_head', __NAMESPACE__ . '\\favicon_ds', 1 );

