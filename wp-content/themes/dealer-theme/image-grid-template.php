<?php
/**
 * Template Name: Image Grid Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <style>
.entry-title {
    display:none;
}
.page-template-template-elementor .container{
    width:100% !important;
    max-width:100% !important;
    padding:0 !important;
    margin:0 !important;
}
</style>
  <?php get_template_part('templates/content', 'image-grid'); ?>
<?php endwhile; ?>
