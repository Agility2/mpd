<?php
/**
 * Template Name: Hot Tub Dimensions
 */

 $show_title_bar = get_field('show_title_bar') ?>
<?php
          echo mainAspot();
?>



<div class="container">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

       <?php if(!empty($show_title_bar)){?>
        <div class="title-bar" style="max-width:960px; margin:0 auto;">
            <h2 class="title page-title" style="!important; float:none; text-align:center;margin-bottom: 0;"><?php echo the_title(); ?></h2>

                          <div class="breadcrumbs">
                            <div class="row" style="text-align:center;">
                              <?php //BREADCRUMBS
                              if ( function_exists('yoast_breadcrumb') ) {
                                yoast_breadcrumb('','');
                              }
                              ?>
                            </div>
                          </div>

            <div class="clearfix"></div>
        </div>
        <?php } ?>
        
        
<div class="content-container" style="max-width:<?php the_field('container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('container_padding_top'); ?>px; padding-bottom:<? the_field('container_padding_bottom'); ?>px;">
      <div class="row">
         <div style="max-width: 800px;
margin: 1em auto;">
          <?php the_content(); ?>
          </div>
      </div>
</div>


				<?php 

				// check for rows (parent repeater)
				if( have_rows('dimensions_visual_list') ): ?>
					<div id="dimensions_visual_list">
					<?php 

					// loop through rows (parent repeater)
					while( have_rows('dimensions_visual_list') ): the_row(); ?>
						<table style="margin: 0 auto;">
						<td style="min-width: 175px;text-align: center;">
							<h3><?php the_sub_field('dimensions_year'); ?></h3>
				        </td>
				        <td style="min-width: 150px;">
							<h3><?php the_sub_field('dimensions_series_name'); ?></h3>
							<?php 

							// check for rows (sub repeater)
							if( have_rows('dimensions_model') ): ?>
								<ul style="list-style:none;">
								<?php 

								// loop through rows (sub repeater)
								while( have_rows('dimensions_model') ): the_row();


									?>
									<li <?php if( get_sub_field('dimensions_model_name') ){ echo 'class="dimensions_model_name"'; } ?>><?php the_sub_field('dimensions_model_name'); ?></li>
                                
                                
                                        <a href="<?php the_sub_field('model_pdf_link'); echo 'class="model_pdf_link"';  ?>">PDF</a> |


                                        <a href="<?php the_sub_field('model_cad_link'); echo 'class="model_cad_link"'; ?>">CAD</a>

								    
								    
								<?php endwhile; ?>
								</ul>
							<?php endif; //if( get_sub_field('items') ): ?>
							</td>
						</table>	

					<?php endwhile; // while( has_sub_field('dimensions_visual_list') ): ?>
					</div>
				<?php endif; // if( get_field('dimensions_visual_list') ): ?>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
<script type="text/javascript">
$(document).foundation('tab', 'reflow');
</script>