<?php
$canonical_link2 = get_field('canonical_link2');
$custom_embed_code = get_field('custom_embed_code', 'options');

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-Frame-Options" content="sameorigin">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="canonical" href="<?php echo $canonical_link2 ?>" />

    <!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
    <link rel="apple-touch-icon" type="image/png" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" type="image/png" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" type="image/png" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" type="image/png" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" type="image/png" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" type="image/png" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" type="image/png" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-config" content="/browserconfig.xml">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <?php wp_head(); ?>

    <?php echo $custom_embed_code; ?>

</head>
