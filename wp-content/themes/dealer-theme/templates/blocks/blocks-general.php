<?php
$row_num       = get_query_var( 'row_num' );
$posts         = get_field( 'general_content_block_selector' );
$current_row   = $posts[ $row_num ];
$current_block = $current_row['block'];
$post          = $current_block;
setup_postdata( $post );

$postID = $post->ID;

//Header Settings
$gb_header_img      = get_field( 'gb_header_background_image' );
$gb_header_bg_color = get_field( 'gb_header_background_color' );
$gb_parallax        = get_field( 'section_image_parallax' );

// Quiz
$show_quiz = get_field( 'show_quiz' );
$choose_quiz = get_field('choose_perfect');


$gb_header_overlay_color          = get_field( 'gb_header_overlay_color' );
$gb_header_overlay_opacity        = get_field( 'gb_header_overlay_opacity' );
$gb_header_title                  = get_field( 'gb_header_title_text' );
$gb_header_title_url              = get_field( 'gb_header_title_text_url' );
$gb_header_subtext                = get_field( 'gb_header_subtext' );
$gb_header_title_color            = get_field( 'gb_header_title_text_color' );
$gb_header_spacing                = get_field( 'gb_header_spacing' );
$gb_header_container              = get_field( 'gb_header_container' );
$gb_header_add_custom_html_toggle = get_field( 'add_custom_html_to_header' );
$gb_header_custom                 = get_field( 'gb_header_custom' );
$gb_header_cta                    = get_field( 'gb_header_cta' );
$gb_header_cta_text               = get_field( 'gb_header_cta_text' );
$gb_header_cta_url                = get_field( 'gb_header_cta_url' );
$gb_header_text_align             = get_field( 'gb_header_text_align' );

//Block Settings

$section_layout  = get_field( 'section_layout_general' );
$gbs_bg_color    = get_field( 'section_background_color_general' );
$gbs_container   = get_field( 'general_block_has_container' );
$gbs_padding     = get_field( 'block_spacing_inner' );
$gb_custom_class = get_field( 'gb_custom_class' );
$gb_custom_id    = get_field( 'gb_custom_id' );
$gb_text_align   = get_field( 'general_blocks_text_align' );

//Tabbed Settings
$section_type = get_field( 'section_type' );


if ( $section_type == "tabbed" ) {
    $tabbed = 'tabbed-section';
} else {
    $tabbed = '';
}


//Block Settings
$numBlocks      = 0;
$block_classes  = array();
$block1_bg_type = get_field( 'block_1_background_type' );
$block2_bg_type = get_field( 'block_2_background_type' );

$block1_content_type = get_field( 'block_1_content_type' );
$block2_content_type = get_field( 'block_2_content_type' );

$block1_title = get_field( 'block_1_title' );
$block2_title = get_field( 'block_2_title' );

$block1_title_position = get_field( 'block_1_title_position' );
$block2_title_position = get_field( 'block_2_title_position' );

$block1_content = get_field( 'block_1_content' );
$block2_content = get_field( 'block_2_content' );

$block1_html = get_field( 'block_1_custom_html' );
$block2_html = get_field( 'block_2_custom_html' );

$block_wrap_class = 'case' . $section_layout;

switch ( $section_layout ) {
    case 0:
        $numBlocks = 0;
        break;
    case 5050:
        $numBlocks        = 2;
        $block_classes[0] = 'large-6 medium-6 xsmall-12';
        $block_classes[1] = 'large-6 medium-6 xsmall-12';
        break;
    case 3070:
        $numBlocks        = 2;
        $block_classes[0] = 'large-4 medium-4 xsmall-12';
        $block_classes[1] = 'large-8 medium-8 xsmall-12';
        break;
    case 7030:
        $numBlocks        = 2;
        $block_classes[0] = 'large-8 medium-8 xsmall-12';
        $block_classes[1] = 'large-4 medium-4 xsmall-12';
        break;
    case 333333:
        $numBlocks        = 3;
        $block_classes[0] = 'large-4 medium-4 xsmall-12';
        $block_classes[1] = 'large-4 medium-4 xsmall-12';
        $block_classes[2] = 'large-4 medium-4 xsmall-12';
        break;
    case 252550:
        $numBlocks        = 3;
        $block_classes[0] = 'large-3 medium-6 xsmall-12';
        $block_classes[1] = 'large-3 medium-6 xsmall-12';
        $block_classes[2] = 'large-6 medium-12 xsmall-12 ';
        break;
    case 502525:
        $numBlocks        = 3;
        $block_classes[2] = 'large-3 medium-6 xsmall-12';
        $block_classes[1] = 'large-3 medium-6 xsmall-12';
        $block_classes[0] = 'large-6 medium-12 xsmall-12';
        break;
    case 25:
        $numBlocks        = 4;
        $block_classes[0] = 'large-3 medium-6 xsmall-12';
        $block_classes[1] = 'large-3 medium-6 xsmall-12';
        $block_classes[2] = 'large-3 medium-6 xsmall-12';
        $block_classes[3] = 'large-3 medium-6 xsmall-12';
        break;
    default:
        $block_classes[0] = 'large-6 medium-6 xsmall-12';
        $block_classes[1] = 'large-6 medium-6 xsmall-12';
}

if ( $numBlocks > 2 ) {
    $block3_bg_type = get_field( 'block_3_background_type' );
    $block4_bg_type = get_field( 'block_4_background_type' );
    $block_bg_types = array( $block1_bg_type, $block2_bg_type, $block3_bg_type, $block4_bg_type );

    $block3_content_type = get_field( 'block_3_content_type' );
    $block4_content_type = get_field( 'block_4_content_type' );

    $block_content_types = array(
        $block1_content_type,
        $block2_content_type,
        $block3_content_type,
        $block4_content_type
    );

    $block3_title = get_field( 'block_3_title' );
    $block4_title = get_field( 'block_4_title' );

    $block_titles = array( $block1_title, $block2_title, $block3_title, $block4_title );

    $block3_title_position = get_field( 'block_3_title_position' );
    $block4_title_position = get_field( 'block_4_title_position' );

    $block_title_positions = array(
        $block1_title_position,
        $block2_title_position,
        $block3_title_position,
        $block4_title_position
    );

    $block3_html = get_field( 'block_3_custom_html' );
    $block4_html = get_field( 'block_4_custom_html' );

    $blocks_custom_html = array( $block1_html, $block2_html, $block3_html, $block4_html );

    $block3_content = get_field( 'block_3_content' );
    $block4_content = get_field( 'block_4_content' );
    $block_contents = array( $block1_content, $block2_content, $block3_content, $block4_content );
} else {
    $block_bg_types = array( $block1_bg_type, $block2_bg_type );

    $block_content_types = array( $block1_content_type, $block2_content_type );

    $block_titles = array( $block1_title, $block2_title );

    $block_title_positions = array( $block1_title_position, $block2_title_position );

    $block_contents = array( $block1_content, $block2_content );

    $blocks_custom_html = array( $block1_html, $block2_html );
}

$image_backgrounds = array();
$color_backgrounds = array();
$block_overlays    = array();
$block_links       = array();

?>
<a id="<?php echo $gb_custom_id; ?>"></a>
<section class="general block section-block <?php echo $gb_custom_class;
echo $tabbed; ?>">

    <?php
    if ( get_field( 'section_type' ) == "reviews" ) {
        get_template_part( 'templates/blocks/blocks', 'reviews' );
    }

    if ( get_field( 'section_type' ) == "custom" ) {
        get_template_part( 'templates/blocks/blocks', 'custom' );
    }

    
    if ( get_field( 'section_type' ) == "wysiwyg" ) {
        get_template_part( 'templates/blocks/blocks', 'wysiwyg' );
    }

    ?>

    <?php
    if ( get_field( 'section_type' ) == "tabbed" || get_field( 'section_type' ) == "general" ) {
        //if( !empty($gb_header_custom) || !empty($gb_header_title) ){

        ?>

        <?php


        //if($gb_header_custom){
        if ( ! empty( $gb_header_add_custom_html_toggle ) ) {

            ?>
            <div class="gb-header" style="padding:<?php echo $gb_header_spacing ?>; ">
                <?php if ( $gb_header_container ){ ?>
                <div class="container">
                    <?php }; ?>
                    <div class="custom">
                        <?php echo $gb_header_custom; ?>
                    </div>
                    <?php if ( $gb_header_container ){ ?>
                </div>
            <?php } ?>
            </div>

        <?php } else { ?>

            <div class="gb-header"
                 style="<?php if ( $gb_parallax ) { ?>background-attachment:fixed;<?php } ?>; background-image:url('<?php echo $gb_header_img ?>'); background-color:<?php echo $gb_header_bg_color ?>; padding:<?php echo $gb_header_spacing ?>; ">

                <?php if ( $gb_header_overlay_color ) { ?>
                    <div class="overlay"
                         style="background-color:<?php echo $gb_header_overlay_color ?>; opacity:.<?php echo $gb_header_overlay_opacity ?>;"></div>
                <?php } ?>

                <?php if ( $gb_header_container ){ ?>
                <div class="container">
                    <?php }; ?>

                    <div class="content" style="text-align:<?php echo $gb_header_text_align ?>;">
                        <?php if ( $gb_header_title ){ ?>
                    <?php if ( $gb_header_title_url ) { ?>
                        <a href="<?php echo $gb_header_title_url ?>">
                            <? } ?>
                            <h2 style="color:<?php echo $gb_header_title_color ?>;"><?php echo strip_tags( $gb_header_title ) ?></h2>
                            <?php } ?>
                            <?php if ( $gb_header_title_url ) { ?>
                        </a>
                    <? } ?>
                        <?php if ( $gb_header_subtext ) { ?>
                            <p style="color:<?php echo $gb_header_title_color ?>;"><?php echo strip_tags( $gb_header_subtext ) ?></p>
                        <?php } ?>


                        <?php if ( $gb_header_cta_text ) { ?>
                            <a href="<? echo $gb_header_cta_url ?>" style="color:<?php echo $gb_header_title_color ?>;">
                                <button class="btn btn-primary"
                                        type="button"><?php echo strip_tags( $gb_header_cta_text ) ?></button>
                            </a>
                        <? } ?>

                    </div>

                    <?php if ( $gb_header_container ){ ?>
                </div>
            <?php } ?>

                <!-- Quiz -->
                <?php if ( $show_quiz ) { ?>

                    <section class="hero perfect_quiz">

                        <div class="inner">

                            <div class="copy">
                            
                            <?php switch ($choose_quiz) {
                                case 'calderaquiz':
                                    echo do_shortcode('[gravityform id="68" title="true" description="true" ajax="true"]');
                                    break;

                                case 'hotspringquiz':
                                    echo do_shortcode('[gravityform id="68" title="true" description="true" ajax="true"]
');
                                    break;
                                
                                default:
                                    # code...
                                    break;
                            } ?>

                            </div>

                        </div>

                    </section>

                <?php } ?>

            </div>
        <?php } ?>

        <?php //}
    }
    ?>


    <div class="gblocks wrapper cf" style="background-color:<?php echo $gbs_bg_color ?>">

        <?php if ( $gbs_container ){ ?>
        <div class="container cf">
            <?php } ?>


            <?php for ( $i = 0; $i < $numBlocks; $i ++ ) {
                $x = $i + 1;
                
                if($overlay_field) {
                    $block_overlays[ $i ] = get_field( $overlay_field );
                }
                $link_field        = 'block_' . $x . '_link';
                $block_links[ $i ] = get_field( $link_field );

                if ( $block_bg_types[ $i ] == 'Image' ) {

                    $image_field             = 'block_' . $x . '_background_image';
                    $image_backgrounds[ $i ] = get_field( $image_field );

                } else {

                    $color_field             = 'block_' . $x . '_background_color';
                    $color_backgrounds[ $i ] = get_field( $color_field );

                }

                if ( $block_content_types[ $i ] == 'Image' ) {

                    $image_content_field        = 'block_' . $x . '_image';
                    $image_content_fields[ $i ] = get_field( $image_content_field );

                } else {

                    $video_content_field        = 'block_' . $x . '_video';
                    $video_content_fields[ $i ] = get_field( $video_content_field );

                }


                ?>

                <article class="<?= $block_classes[ $i ]; ?> general-block general-block-<?= $i; ?>"
                         style="text-align:<?php echo $gb_text_align ?>;">

                    <?php if ( empty( $blocks_custom_html[ $i ] ) ) { ?>

                        <?php if ( ! empty( $block_links[ $i ] ) ) { ?>

                            <div class="general-block-outer"
                                 style="background:url('<?= $image_backgrounds[ $i ]; ?>'); background-color:<?= $color_backgrounds[ $i ]; ?>; padding:<?php echo $gbs_padding ?>;">


                                <div class="general-block-inner">

                                    <a href="<?= $block_links[ $i ]; ?>">

                                        <?php if ( $block_title_positions[ $i ] == 'above' ) { ?>

                                            <?php if ( ! empty( $block_titles[ $i ] ) ) { ?>
                                                <h3><?php
                                                    $convert_title = str_replace( "<r></r>", "® ", $block_titles[ $i ] );
                                                    $clean_title   = strip_tags( $convert_title );
                                                    echo $clean_title;
                                                    ?></h3>
                                            <?php } ?>

                                            <?php if ( ! empty( $image_content_fields[ $i ] ) ) { ?>
                                                <img src="<?= $image_content_fields[ $i ]; ?>" alt="General Image">
                                            <?php } ?>

                                            <?php if ( ! empty( $video_content_fields[ $i ] ) ) { ?>
                                                <div class="video-wrapper"><?= $video_content_fields[ $i ]; ?></div>
                                            <?php } ?>


                                        <?php } else if ( $block_title_positions[ $i ] == 'below' ) { ?>

                                            <?php if ( ! empty( $image_content_fields[ $i ] ) ) { ?>
                                                <img src="<?= $image_content_fields[ $i ]; ?>" alt="General Image">
                                            <?php } ?>

                                            <?php ?>

                                            <?php if ( ! empty( $video_content_fields[ $i ] ) ) { ?>
                                                <div class="video-wrapper"><?= $video_content_fields[ $i ]; ?></div>
                                            <?php } ?>

                                            <?php if ( ! empty( $block_titles[ $i ] ) ) { ?>
                                                <h3><?php
                                                    $convert_title = str_replace( "<r></r>", "® ", $block_titles[ $i ] );
                                                    $clean_title   = strip_tags( $convert_title );
                                                    echo $clean_title;
                                                    ?></h3>
                                            <?php } ?>

                                        <?php } else { ?>
                                            <?php if ( ! empty( $video_content_fields[ $i ] ) ) { ?>

                                                <?php if ( ! empty( $block_titles[ $i ] ) ) { ?>
                                                    <h3><?php
                                                        $convert_title = str_replace( "<r></r>", "® ", $block_titles[ $i ] );
                                                        $clean_title   = strip_tags( $convert_title );
                                                        echo $clean_title;
                                                        ?></h3>
                                                <?php } ?>

                                                <?= $video_content_fields[ $i ]; ?>
                                            <?php } else { ?>

                                                <div class="imgwithtitle">

                                                    <?php if ( ! empty( $block_titles[ $i ] ) ) { ?>
                                                        <h3><?php
                                                            $convert_title = str_replace( "<r></r>", "® ", $block_titles[ $i ] );
                                                            $clean_title   = strip_tags( $convert_title );
                                                            echo $clean_title;
                                                            ?></h3>
                                                    <?php } ?>

                                                    <?php if ( ! empty( $image_content_fields[ $i ] ) ) { ?>
                                                        <img src="<?= $image_content_fields[ $i ]; ?>"
                                                             alt="General Image">
                                                    <?php } ?>

                                                </div>

                                            <?php } ?>
                                        <?php } ?>

                                    </a>
                                    <?php if ( ! empty( $block_contents[ $i ] ) ) { ?>
                                        <p><?= $block_contents[ $i ]; ?></p>
                                    <?php } ?>
                                </div>


                            </div>

                        <?php } else { ?>

                            <div class="general-block-outer"
                                 style="background:url('<?= $image_backgrounds[ $i ]; ?>'); background-color:<?= $color_backgrounds[ $i ]; ?>; padding:<?php echo $gbs_padding ?>;">

                                <div class="general-block-inner">

                                    <?php if ( $block_title_positions[ $i ] == 'above' ) { ?>

                                        <?php if ( ! empty( $block_titles[ $i ] ) ) { ?>
                                            <h3><?php
                                                $convert_title = str_replace( "<r></r>", "® ", $block_titles[ $i ] );
                                                $clean_title   = strip_tags( $convert_title );
                                                echo $clean_title;
                                                ?></h3>
                                        <?php } ?>

                                        <?php if ( ! empty( $image_content_fields[ $i ] ) ) { ?>
                                            <img src="<?= $image_content_fields[ $i ]; ?>" alt="General Image">
                                        <?php } ?>

                                        <?php if ( ! empty( $video_content_fields[ $i ] ) ) { ?>
                                            <div class="video-wrapper"><?= $video_content_fields[ $i ]; ?></div>
                                        <?php } ?>

                                    <?php } else if ( $block_title_positions[ $i ] == 'below' ) { ?>

                                        <?php if ( ! empty( $image_content_fields[ $i ] ) ) { ?>
                                            <img src="<?= $image_content_fields[ $i ]; ?>" alt="General Image">
                                        <?php } ?>

                                        <?php if ( ! empty( $video_content_fields[ $i ] ) ) { ?>
                                            <div class="video-wrapper"><?= $video_content_fields[ $i ]; ?></div>
                                        <?php } ?>

                                        <?php if ( ! empty( $block_titles[ $i ] ) ) { ?>

                                            <h3><?php
                                                $convert_title = str_replace( "<r></r>", "® ", $block_titles[ $i ] );
                                                $clean_title   = strip_tags( $convert_title );
                                                echo $clean_title;
                                                ?></h3>

                                        <?php } ?>

                                    <?php } else if ( $block_title_positions[ $i ] == 'middletop' ) { ?>
                                        <?php if ( ! empty( $video_content_fields[ $i ] ) ) { ?>
                                            <?php if ( ! empty( $block_titles[ $i ] ) ) { ?>

                                                <h3><?php
                                                    $convert_title = str_replace( "<r></r>", "® ", $block_titles[ $i ] );
                                                    $clean_title   = strip_tags( $convert_title );
                                                    echo $clean_title;
                                                    ?></h3>

                                            <?php } ?>
                                            <?= $video_content_fields[ $i ]; ?>
                                        <?php } else { ?>

                                            <div class="imgwithtitletop">

                                                <?php if ( ! empty( $block_titles[ $i ] ) ) { ?>
                                                    <h3><?php
                                                        $convert_title = str_replace( "<r></r>", "® ", $block_titles[ $i ] );
                                                        $clean_title   = strip_tags( $convert_title );
                                                        echo $clean_title;
                                                        ?></h3>
                                                <?php } ?>

                                                <?php if ( ! empty( $image_content_fields[ $i ] ) ) { ?>
                                                    <img src="<?= $image_content_fields[ $i ]; ?>" alt="General Image">
                                                <?php } ?>

                                            </div>

                                        <?php } ?>
                                    <?php } else { ?>

                                        <?php if ( ! empty( $video_content_fields[ $i ] ) ) { ?>

                                            <?php if ( ! empty( $block_titles[ $i ] ) ) { ?>
                                                <h3><?php
                                                    $convert_title = str_replace( "<r></r>", "® ", $block_titles[ $i ] );
                                                    $clean_title   = strip_tags( $convert_title );
                                                    echo $clean_title;
                                                    ?></h3>
                                            <?php } ?>

                                            <?= $video_content_fields[ $i ]; ?>

                                        <?php } else { ?>

                                            <div class="imgwithtitle">
                                                <?php if ( ! empty( $block_titles[ $i ] ) ) { ?>
                                                    <h3><?php
                                                        $convert_title = str_replace( "<r></r>", "® ", $block_titles[ $i ] );
                                                        $clean_title   = strip_tags( $convert_title );
                                                        echo $clean_title;
                                                        ?></h3>
                                                <?php } ?>

                                                <?php if ( ! empty( $image_content_fields[ $i ] ) ) { ?>
                                                    <img src="<?= $image_content_fields[ $i ]; ?>" alt="General Image">
                                                <?php } ?>
                                            </div>

                                        <?php } ?>
                                    <?php } ?>
                                    <?php if ( ! empty( $block_contents[ $i ] ) ) { ?>
                                        <p><?= $block_contents[ $i ]; ?></p>
                                    <?php } ?>
                                </div>


                            </div>

                        <?php } ?>

                    <?php } else { ?>

                        <div class="general-block-outer" style="padding:<?php echo $gbs_padding ?>;">
                            <div class="general-block-inner">
                                <div class="custom">
                                    <?php echo $blocks_custom_html[ $i ] ?>
                                </div>
                            </div>
                        </div>

                    <?php } ?>


                </article>

            <?php } ?>
            <?php if ( $gbs_container ){ ?>
        </div>
    <?php } ?>
        <?php
        if ( get_field( 'section_type' ) == "tabbed" ) {
            get_template_part( 'templates/blocks/blocks', 'tabbed' );
        }
        ?>


    </div>


</section>


<?php wp_reset_postdata(); ?>
