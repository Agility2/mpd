<?php
    $reviewsType = get_field('gcb_reviews_type');
    if($reviewsType == 'simple'){
?>
    <?php
    $selected_reviews = get_field('select_reviews', false, false);
?>
       <div class="collection-reviews">
        <div class="row">
             <h2 class="title"><?php the_field('gcb_reviews_header'); ?></h2>
             <p class="description" style="margin:0;"><?php the_field('gcb_reviews_sub_header'); ?></p>
              <div class="quote-section">
                <div class="row">
                    <?php
                    //get image variables
                    $left_quote = '//watkinsdealer.s3.amazonaws.com/images/sitewide/left-quote.png';
                    $right_quote = '//watkinsdealer.s3.amazonaws.com/images/sitewide/right-quote.png';
                    $right_quote_white = '//watkinsdealer.s3.amazonaws.com/images/sitewide/right-quote-white1.png';
                    $left_quote_white = '//watkinsdealer.s3.amazonaws.com/images/sitewide/left-quote-white1.png';
                    ?>
                    <div class="large-12 xsmall-12 columns mobile-quotes"><img src="/wp-content/uploads/2015/05/commas-174x54.png" alt="Comas Icon" /></div>
                    <div class="large-2 medium-2 small-1 xsmall-1 columns"><img src="<?php echo $left_quote; ?>" class="right" style="margin-top: 130px;" alt="Left Quote Icon" /></div>
                    <div class="large-8 medium-8 small-12 xsmall-12 columns">
                        <div class="slider">

                        <?php


                            //$current_category = get_field('product-category');
                            $args = array(
                                'post_type'         => 'reviews',
                                //'reviews_cat'     => $current_category,
                                /*'orderby'           => 'rand',*/
                                'post__in'          => $selected_reviews,
                                'posts_per_page'    => 5
                                    );
                                $query = new WP_Query( $args );
                                // The Loop
                                if ( $query->have_posts() ) {
                                    while ( $query->have_posts() ) {
                                        $query->the_post();
                                        $review_title = get_the_title();
                                        $quote = custom_field_excerpt();
                                        $owner = get_field('review-owner');
                                        $location = get_field('review-location');
                                        $raw_score = get_field('review-score');
                                        $score = round($raw_score);
                                        $score_img = '//watkinsdealer.s3.amazonaws.com/images/sitewide/'.$score.'_star_rating.png';

                                        echo '<div>';
                                        echo '';
                                        if (!empty($raw_score)){
                                        echo '<img src="'.$score_img.'" alt="'.$raw_score.' Review Score Icon" class="review-score-img" />';
                                        }
                                        echo '<blockquote>'.$review_title.'</blockquote>';
                                        echo '<blockquote>'. $quote .'</blockquote>';
                                        if( (!empty($owner)) && (!empty($location)) ){
                                        echo '<p class="quote-author">'. $owner .' - '.$location.'</p>';
                                        }
                                        echo '</div>';
                                    }
                                } else {
                                    echo " ";
                                }
                            wp_reset_postdata();
                        ?>
                        </div>
                  </div>
                  <div class="large-2 medium-2 small-1 xsmall-1 columns"><img src="<?php echo $right_quote; ?>" alt="Right Quote Icon" class="left" style="margin-top: 130px;" /></div>
               </div>
            </div>
        </div>
    </div>
<?
  }

  if($reviewsType == 'withVid'){ ?>


      <?php
      $selected_reviews = get_field('gcb_video_select_reviews', false, false);
      ?>
      <a id="videoreview"></a>
      <div class="collection-video-reviews">
          <div class="row">
              <!-- SECTION TITLES -->

              <div class="large-6 medium-12 xsmall-12 columns">
                  <h2 class="title"><?php the_field('gcb_videos_header'); ?></h2>
                  <div class="video-section">
                      <div class="slider">
                          <?php
                          if( have_rows('gcb_select_videos') ):

                          while ( have_rows('gcb_select_videos') ) : the_row();
                          $posts = get_sub_field('gcb_pick_video');
                          foreach($posts as $post){
                              setup_postdata($post);
                              $video_title = get_field('gcb_video_header');
                              $embed_code = get_field('youtube_embed_code');
                              if(!empty($embed_code)) {
                                  echo '<div>';

                                  echo '<div>'.$embed_code.'</div>';
                                  echo '</div>';
                              }
                          }
                          wp_reset_postdata();
                          endwhile;

                          else :

                          // no rows found

                          endif;
                          ?>
                      </div>
                  </div>
              </div>
              <div class="product-reviews large-6 medium-12 xsmall-12 columns">
                  <h2 class="title"><?php the_field("col_reviews_header"); ?></h2>
                  <div class="quote-section">
                      <!--<div class="row">-->
                      <?php
      //get image variables
    $left_quote = '//watkinsdealer.s3.amazonaws.com/images/sitewide/left-quote.png';
    $right_quote = '//watkinsdealer.s3.amazonaws.com/images/sitewide/right-quote.png';
    $right_quote_white = '//watkinsdealer.s3.amazonaws.com/images/sitewide/right-quote-white1.png';
    $left_quote_white = '//watkinsdealer.s3.amazonaws.com/images/sitewide/left-quote-white1.png';
                      ?>
                      <?php
      $current_category = get_field('gcb_reviews_category');
      $cat_term = get_term($current_category, 'reviews_cat');

                      ?>
                      <div class="large-12 mobile-quotes"><img src="//watkinsdealer.s3.amazonaws.com/images/sitewide/commas-174x54.png" alt="Commas Icon" /></div>
                      <div class="left-quote large-2 medium-2 small-1 xsmall-1 columns"><img src="<?php echo $left_quote_white;?>" alt="Left Quote Icon" style="float:right;"/></div>
                      <div class="large-8 medium-8 small-12 xsmall-12  columns" style="padding-left: 0; padding-right:0;">
                          <div class="slider">

                              <?php



      $args = array(
          'post_type'     => 'reviews',
          //'reviews_cat'   => $cat_term->slug,
          'tax_query'     => array(
              array(
                  'taxonomy'  =>  'reviews_cat',
                  'field'     =>  'slug',
                  'terms'     =>  $cat_term->slug,
              ),
          ),
          //'orderby'       => 'count',
          'post__in'          => $selected_reviews,
          'posts_per_page' => 5
      );
      $query = new WP_Query( $args );
      // The Loop
      if ( $query->have_posts() ) {
      	echo "hello";
          while ( $query->have_posts() ) {
              $query->the_post();
              $id = get_the_ID();

              $quote = custom_field_excerpt();
              $owner = get_field('review-owner');
              $location = get_field('review-location');
              $raw_score = get_field('review-score');
              $score = round($raw_score);
             $score_img = '//watkinsdealer.s3.amazonaws.com/images/sitewide/'.$score.'_star_rating.png';

              echo '<div>';
              echo '';

              if (!empty($raw_score)){
                  echo '<img src="'.$score_img.'" alt="'.$score_img.' Review Score Icon" class="review-score-img" />';
              }

              echo '<p class="quote-author">'.get_the_title().'</p>';
              echo '<blockquote>'. $quote .'</blockquote>';
              if( (!empty($owner))  && (!empty($location)) ){
                  echo '<p class="quote-author">'. $owner .' - '.$location.'</p>';
              }
              echo '</div>';

          }
      } else {
          echo '<p class="quote-author">There are no reviews for this category at this time</p>';

      }
      wp_reset_postdata();
                              ?>
                          </div>
                      </div>
                      <div class="right-quote large-2 medium-2 small-1 xsmall-1 columns"><img src="<?php echo $right_quote_white; ?>" alt="Right Quote Icon" style="float:left;" /></div>
                      <div style="clear:both;"></div>

                      <div style="clear:both;"></div>
                  </div>

              </div><!--end review side-->
          </div>
      </div>



  <?php } // end if video ?>
