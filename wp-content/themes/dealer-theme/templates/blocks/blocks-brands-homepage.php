 <?php
/*--------------- BRANDING SECTION - NEW ------- */
$show_brands_section = get_field('show_brands_section');
if(!empty($show_brands_section)){
?>
<div class="boxes-section">
   <a id="hot-tubs-section-link"></a>
    <div class="row">
        <h2 class="title"><?php the_field('brands_section_header'); ?></h2>
        
        <?php
        if( have_rows('brands') ):
            while ( have_rows('brands') ) : the_row();
                //Get Row Variables
                $NumBlocks = get_sub_field('blocks_per_row');
                
                $BlockSize = (12/$NumBlocks);
                $BlockPadding = get_sub_field('top_bottom_padding');
                
                for($i = 1; $i<=$NumBlocks; $i++){
                    $image = get_sub_field('brand_'.$i.'_image');
                    $image_off = '\''.get_sub_field('brand_'.$i.'_image').'\'';
                    $image_hover = '\''.get_sub_field('brand_'.$i.'_image_trans').'\'';
                    $image_hovered = get_sub_field('brand_'.$i.'_image_trans');
                    $link = get_sub_field('brand_'.$i.'_link');
                    $color1 = get_sub_field('brand_'.$i.'_color1');
                    $color2 = get_sub_field('brand_'.$i.'_color2');
                    $hover = 'hover';
                    echo '<div class="brand-outer-block flip-container large-'.$BlockSize.' medium-'.$BlockSize.' small-6 xsmall-12 columns" ontouchstart="this.classList.toggle('.$hover.');">';
                        echo '<a href="'.$link.'">';
                            /* FLIPPER */
                            
                            echo '<div class="flipper">';
                                /* FRONT */
                                echo '<div class="brand-inner-block front" style="background:#fff; padding:'.$BlockPadding.' 0;border:1px solid#ccc;">';
                                    echo '<img src="'.$image.'" width="100%" />';
                                echo '</div>';
                                /* BACK */
                                /*echo '<div class="brand-inner-block back" style="background: linear-gradient('.$color1.','.$color2.'); padding:'.$BlockPadding.' 0;">';
                                    echo '<img src="'.$image_hovered.'" width="100%" />';
                                echo '</div>';*/
                            echo '</div>';
                    
                            /*echo '<img src="'.$image.'" width="100%" style="opacity:0; visibility:hidden; height:0px;"/>';*/
                        echo '</a>';
                    echo '</div>'; 
                    
                }
                
            endwhile;
        else :
            // no rows found
        endif;
        ?>
    </div>
</div>    
<?php } ?>
<!--------------- END NEW BRAND SECTION  ------->