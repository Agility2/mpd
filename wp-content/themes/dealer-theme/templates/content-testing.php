<?php //the_content(); ?>
<?php //wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>


<div class="test" style="padding: 40px 0;">
    <div class="row">
        <h2 class="title">Grandee Reviews</h2>




        <div id="hotSpringSpaModelGrandee" itemscope itemtype="http://schema.org/Product">

            <!-- Content Container -->
            <span id="UgcSummary_a0004" style="font-family: Arial, sans-serif; font-size: 12px; color: #5A5A5A;"></span>
            <!--<a href="http://www.hotspring.com/shop-hot-tub-models/highlife/grandee#first" title="Hot Spring Spas" target="_blank" style="display: block; padding: 5px 0 0 0;"><img border="0" alt="Hot tubs Reviews | 7 Person Spa by Hot Spring Spas" src="http://media.hotspring.com/logos/hss-video-logo.png" width="97" height="30" /></a>-->

            <!-- Insert Content -->
            <script type="text/javascript">
                function BVHandleSummaryResults(jsonData) {
                    var ugcSummary = document.getElementById('UgcSummary_' + jsonData.subjectID);
                    if(ugcSummary) {
                        if(jsonData.totalReviews > 0 && jsonData.reviewsUrl.length > 0) {

                            var readReviewStr = "Read <span itemprop='reviewCount'>" + jsonData.totalReviews + "</span> reviews";
                            var averageRating = jsonData.averageRating.toFixed(2);
                            var averageRatingSplit = averageRating.split('.');
                            var averageRatingUrl = averageRatingSplit[0] + '_' + averageRatingSplit[1];
                            var starRating = 'https://hotspring.ugc.bazaarvoice.com/0526-en_us/'+ averageRatingUrl +'/5/rating.gif';

                            ugcSummary.innerHTML +=
                                /*"<span style='font-weight:bold;'>" +
                                "Average <span itemprop='model'>Grandee</span> Rating: " +
                                "</span>"+*/
                                "<br />" +
                                "<span class='hotSpringSpaModelRating' itemprop='aggregateRating' itemscope itemprop='http://schema.org/AggregateRating'>" +
                                "<span itemprop='ratingValue'><img src="+ starRating +" alt="+ jsonData.averageRating.toFixed(2) +"  style='padding: 5px 0;' /></span>" +
                                "<br>" +
                                "<a class=\"fancybox\" href=" + jsonData.reviewsUrl + " title='Read Hot Spring Reviews' target='_blank' style='color: #4E7682; text-decoration: none;'>" +
                                "Read <span itemprop='reviewCount'>" + jsonData.totalReviews + "</span> reviews</a>" +
                                "</span>";

                        }
                        /*if(jsonData.totalQuestions > 0 && jsonData.questionsUrl.length > 0) {
                            var readQAStr = "Read " + jsonData.totalQuestions + " questions and " + jsonData.totalAnswers + " answers"; ugcSummary.innerHTML += "<br>" + readQAStr.link(jsonData.questionsUrl);
                        } */
                    }
                }
            </script>

            <!-- Dynamic Content -->
            <script type="text/javascript" src="https://hotspring.ugc.bazaarvoice.com/0526-en_us/a0004/statisticssummary.js?format=embedded"></script>

        </div>




    </div>
</div>
