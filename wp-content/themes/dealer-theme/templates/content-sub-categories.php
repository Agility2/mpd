<?php 
    /**** TITLE *******/
/*
    $override_title = get_field('title_overide');
    $title = get_the_title(); 
    
    if(!empty($override_title)) {
        echo '<h2 class="title">'.$override_title.'</h2>';
    } else {
        echo '<h2 class="title">'.$title.'</h2>';
    }*/
?>
<!-------------------- PRODUCT LIST ---->
<?php 
//if (!empty(get_field('show-product-slider') ) ) :
?>
<hr style="border:none; border-bottom:1px solid #ccc;" >
<div class="product-carousel">
    <div class="row">
       <h2 class="title"><?php the_field('models_section_header'); ?></h2>
        <div class="product-model-section">
            
                <?php
    
                        remove_all_filters('posts_orderby');
                        $current_category = get_field('sub_category');
                        $args = array(
                            'post_type'     => 'product',
                            //'product_tag'   => $current_category,
                            'posts_per_page' => -1,
                            'tax_query'             => array(
                                array(
                                    'taxonomy'      => 'product_cat',
                                    'field' => 'term_id', //optional, as it defaults to 'term_id'
                                    'terms'         => $current_category,
                                    'operator'      => 'IN' //Possible values are 'IN', 'NOT IN', 'AND'. 
                                        )
                                    )
                                );
                            $query = new WP_Query( $args );
                            $count = 0;
                            // The Loop
                            if ( $query->have_posts() ) {  
                                while ( $query->have_posts() ) {
                                    $query->the_post();
                                    $product_name = get_the_title();
                                    $collection_field = get_field('product-collection');
                                    $collection = get_term($collection_field, 'product_cat');
                                    $collection_slug = $collection->slug;
                                    $collection_name = $collection->name;
                                    $top_image = get_field('top_down_image_thumbnail');
                                    $product_cap = get_field('product_cap');
                                    $link = get_permalink($query->post->ID);
                                    $width = get_field('width');
                                    $length = get_field('length');
                                    $height = get_field('height');
                                    $dim = $width. ' x ' .$length. ' x ' .$height;
                                    $vol = get_field('product_volume');
                                    $dim_toggle = 0;
                                        if ( (!empty($width)) && (!empty($length)) && (!empty($height))  ) { $dim_toggle = 1; }
                                    $feat_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');
                                    
                                    if ((!empty($feat_img[0])) || (!empty($top_image)) ) :
                                    
                                        echo '<div class="product-slider-box large-4 medium-6 small-12">';
                                            echo '<a href="'.$link.'"><p class="product-model-slide-title">'.$product_name.' '.$product_cap.'</p>';
                                            if ( $dim_toggle == 1 ) {
                                            echo '<p>'.$dim.' | '.$vol.'</p>';
                                            }
                                            echo '</a>';
                                            if (!is_array($collection_name) && ($current_category == 'Hot-Tubs') ) :
                                                echo '<a href="'.$collection_link.'" class="collection-link"><p>'.$collection_name.'</p></a>';
                                            endif;
                                            if(!empty($top_image) ): echo '<a href="'.$link.'"><img src="'.$top_image.'" alt="'.$title.'"/></a>';
                                            else :
                                                //$feat_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');
                                                $alt_img = get_field('jet-top-image');
                                                if (!empty($alt_img)) :
                                                    echo '<a href="'.$link.'"><img src="'.$alt_img.'" alt="'.$title.'"/></a>'; 
                                                else : echo '<a href="'.$link.'"><img src="'.$feat_img[0].'" alt="'.$title.'"/></a>';
                                                endif;
                                            endif;
                                    echo '<div style="clear:both;"></div>';
                                       
                                    echo '</div>';
                                    $count++;
                                    //if (($count % 3) === 0 && ($count > 0 )) : echo '<div style="clear:both;"></div><hr>'; endif;
                                    else : continue;
                                    
                                    endif;
                                }
                            } else {
                                echo " ";
                            }
                        wp_reset_postdata();
                    ?>
                    
            <div style="clear:both;"></div>
        </div>
    </div>
</div>
<?php 
//else:
//endif;
?>
 