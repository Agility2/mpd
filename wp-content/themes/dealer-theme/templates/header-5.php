<?php
//FET VARIABLES FROM PAGE

$dsHaveCart = get_field('have_cart', 'option');
$containerWidth = get_field('field_564bafc5095ad', 'option');

?>


<script>
jQuery(function($){
   $('html').addClass('header-5');



    //$('ul.sub-menu li.menu-item-has-children').css("outline", "solid 1px red");

   // $('ul.sub-menu .menu-item-has-children a').append('<p class="submenu-toggle" style="display: inline-block;">+</p>');

    //$('ul.sub-menu .menu-item-has-children').find('li:first-of-type').css("outline", "solid 1px green");

    //$('.submenu-toggle').click(function() {
        //$('ul.sub-menu li.menu-item-has-children').css("outline", "solid 1px green");

    //});
});
</script>
<a id="top"></a>
<div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
       <div class="header5 the-header">

           <?php include('mobile-header.php') ?>

            <!-- header -->
            <header class="desktop nav">

                <div class="util">
                  <?  if($containerWidth == 'auto') { ?>
                      <div class="container util">
                  <? } else { ?>
                      <div class="ds-nav-container util">
                  <? } ?>

                <nav class="utilities nav">
                  <?  if($containerWidth == 'auto') { ?>
                      <div class="container cf">
                  <? } else { ?>
                      <div class="ds-nav-container">
                  <? } ?>
                   <div class="themenu cf">
                    <?php $defaults = array(
                                    'theme_location'  => 'util_navigation',
                                    'menu'            => '',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => 'menu utilities',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    // 'fallback_cb'     => 'wp_page_menu',
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                                    'depth'           => 0,
                                    'walker'          => ''
                                );
                                wp_nav_menu( $defaults );

                       if($dsHaveCart == true){

                        // WooCommerce Global variable
                        global $woocommerce;
                        // Header Cart Logic
                        $woo_cart_quantity = $woocommerce->cart->get_cart_contents_count();
                        $cart_link = ($woo_cart_quantity > 0) ? "cart" : "store";
                        ?>
                        <div class="cart-search-combo cf">
                            <a class="cart" href="<?= $woocommerce->cart->get_cart_url(); ?>" title="Cart"><span class="the-cart-quantity"><?php echo $woo_cart_quantity; ?></span><img src="//watkinsdealer.s3.amazonaws.com/Images/Icons/cart.png" alt="shopping cart" /></a>

                            <?php }else{ ?>

                                <div class="cart-search-combo">

                            <?php } ?>

                            <a class="the-search-icon">Search</a>
                        </div>

                        </div>
                        </div>
                    </nav>

                    </div>
                    </div>

                <nav class="global nav">
                  <?  if($containerWidth == 'auto') { ?>
                      <div class="container cf">
                  <? } else { ?>
                      <div class="ds-nav-container cf">
                  <? } ?>
                            <?php $defaults = array(
                                    'theme_location'  => 'header_5_left',
                                    'menu'            => '',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => 'main nav 2 cf',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    // 'fallback_cb'     => 'wp_page_menu',
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                                    'depth'           => 0,
                                    'walker'          => ''
                                );
                                wp_nav_menu( $defaults ); ?>

                                    <?php $image = get_field('header_logo', 'option'); ?>

                                    <h1><a href="<?= esc_url(home_url('/')); ?>"><?php the_title(); ?> | <?php bloginfo('name'); ?><img src="<?php echo $image['url']; ?>" alt=""></a></h1>

                                <?php $defaults = array(
                                    'theme_location'  => 'header_5_right',
                                    'menu'            => '',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => 'main nav cf',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    // 'fallback_cb'     => 'wp_page_menu',
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                                    'depth'           => 0,
                                    'walker'          => ''
                                );
                                wp_nav_menu( $defaults ); ?>
                        </div>
                   </nav>

                   <div class="form-wrapper">
                       <div class="esc-form"><a href="javascript:;" class="close-search">x</a></div>
                        <form id="fast-search-desktop form-wrapper" class="form-wrapper" style="display:block !important;" role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
                        <label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
                        <input class="popup-search-bar search" type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search Products&hellip;', 'placeholder', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'woocommerce' ); ?>" />
                        <input type="hidden" name="post_type" value="product" />
                    </form>
                </div>

            </header>
                </div>
