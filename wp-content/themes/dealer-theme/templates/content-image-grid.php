<div class="rento-linens-content">
    <div class="page-top-heading">
        <div class="container">
           <h2><?php echo get_field('image_grid_title'); ?></h2>
        </div>
    </div>
</div>

<?php
// Retrieve General Content Rows
$general_rows = get_field( 'general_content_block_selector' );

$general_rows_orders = array();

// Place General Content Row orders into array
$num_general_rows = 0;

if ( $general_rows ) {

    foreach ( $general_rows as $row ) {

        $general_rows_orders[ $num_general_rows ] = $row['global_page_order'];

        $num_general_rows ++;
    }
}

// Add main section orders to single array - $sections_array
$sections_array = array( $reviews_order, $products_order, $brands_order );

// Add General Content Row orders to array - $sections_array
if ( ! empty( $general_rows_orders ) ) {

    foreach ( $general_rows_orders as $order ) {

        array_push( $sections_array, $order );
    }
}

/*
 * Loop through the all sections in $sections_array.
 * Displays them according to their section orders.
 * Sections with equivalent orders will cause errors.
 */
$order_count = count( $sections_array ) + 1;
for ( $i = 0; $i < 10; $i ++ ) {
    $row_num = 0;
    if ( ! empty( $general_rows ) ) {
        foreach ( $general_rows as $row ) {
            if ( $row['global_page_order'] == $i ) {
                set_query_var( 'row_num', $row_num );
                /*----- GENERAL CONTENT ROW (GCR) SECTION -----*/
                /*------- can display multiple GCRs -----------*/
                get_template_part( '/templates/blocks/blocks', 'general' );
            } else {
                $row_num ++;
            }
        }
    }
}
?>

<?php

if ( ! empty( $show_boxes ) ) {
?>
    <a id="collections"></a>
    <div class="collection-wrapper">
        <div class="row">
            <?php if ( ! empty( $show_title_bar ) ) { ?>
                <div class="title-bar">
                    <h2 class="title collections-page-title"
                        style="text-align:<?php the_field( 'text_align' ); ?> !important; float:none;"><?php echo $collection_page_title; ?></h2>

                    <?php if ( ! empty( $show_anchor ) ) {

                        if ( ! empty( $custom_static_menu ) ) {
                            if ( have_rows( 'collections_static_links' ) ) {
                                echo '<div class="anchor-wrap">';
                                while ( have_rows( 'collections_static_links' ) ) : the_row();

                                    $collections_static_link_title = get_sub_field( 'collections_static_link_title' );
                                    $collections_static_link_url   = get_sub_field( 'collections_static_link_url' );

                                    echo '<a class="anchor-link-top smooth-scroll" href="' . $collections_static_link_url . '"><div class="anchor-button">' . $collections_static_link_title . '</div></a>';
                                endwhile;
                                echo '</div>';
                            }
                        } else {

                            echo '<div class="anchor-wrap">';
                            echo '<a class="anchor-link-top smooth-scroll" href="#pricing"><div class="anchor-button">' . $pricing_link . '</div></a>';
                            if ( ! empty( $show_products ) ) {
                                echo '<a href="#models" class="smooth-scroll"><div class="anchor-button">' . $products_link . '</div></a>';
                            }
                            if ( ! empty( $show_videos ) ) {
                                echo '<a href="#videoreview" class="smooth-scroll"><div class="anchor-button">' . $videos_link . '</div></a>';
                            }
                            echo '</div>';
                        } ?>

                    <? } ?>

                    <?php if ( ! is_front_page() ): ?>

                        <div class="breadcrumbs">
                            <div class="row" style="text-align:center !important;">
                                <? if ( ! empty( $show_breadcrumbs ) ) { ?>
                                    <?php //BREADCRUMBS
                                    if ( function_exists( 'yoast_breadcrumb' ) ) {
                                        yoast_breadcrumb( '', '' );
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>


            <?php // TOP CONTENT AREA

            $show_top_content = get_field( 'show_top_content_area' );
            if ( ! empty( $show_top_content ) ) {
                $top_body = get_field( 'col_top_body_content' );
                ?>
                <div class="collection-top-content cf">
                    <div class="top-body-content">
                        <div class="content-container"
                            style="max-width:<?php the_field( 'container_width' ); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field( 'container_padding_top' ); ?>px; padding-bottom:<? the_field( 'container_padding_bottom' ); ?>px;">
                            <? echo $top_body; ?>
                        </div>
                    </div>
                </div>
            <? } else { ?>


                <div class="collection-top-content cf">
                    <div class="top-body-content">
                        <div class="content-container"
                            style="max-width:<?php the_field( 'container_width' ); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field( 'container_padding_top' ); ?>px; padding-bottom:<? the_field( 'container_padding_bottom' ); ?>px;">
                            <?php echo the_content(); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>


            <?php


            $num_collections = get_field( 'num-collections' );
            $columns         = 3;
            $box_classes     = '';
            if ( $num_collections == '3' ) {
                $large       = 4;
                $medium      = 6;
                $small       = 6;
                $xsmall      = 12;
                $box_classes = 'three-col';
            } elseif ( $num_collections == '4' ) {
                $large       = 3;
                $medium      = 6;
                $small       = 6;
                $xsmall      = 12;
                $box_classes = 'four-col';
            } elseif ( $num_collections == '2' ) {
                $large       = 6;
                $medium      = 6;
                $small       = 6;
                $xsmall      = 12;
                $box_classes = 'two-col';
            }
            $current_category = get_field( 'product-category-slider' );
            // check if the repeater field has rows of data
            if ( have_rows( 'hot-tubs' ) ):
                $count = 0;
                // loop through the rows of data
                while ( have_rows( 'hot-tubs' ) ) : the_row();
                    $image      = get_sub_field( 'collection-image' );
                    $title      = get_sub_field( 'collection-title' );
                    $desc       = get_sub_field( 'collection-description' );
                    $cdn_image  = get_sub_field( 'collection_image_cdn_url' );
                    $tub_link   = get_sub_field( 'collection-link' );
                    $page_title = get_the_title();
                    if ( $page_title == 'saunas' ) :
                        $title = str_ireplace( 'sauna', '', $title );
                    endif;
                    $large_pull = 0;
                    $med_pull   = 0;
                    // $title_gradient_bg = "get_field('title_gradient_bg', 'option')";

                    if ( get_field( 'title_gradient_bg' ) ) {
                        $title_gradient_bg = "background: -webkit-linear-gradient(rgba(0,0,0,0.6) 30%, transparent 100%); background: -moz-linear-gradient(rgba(0,0,0,0.6) 30%, transparent 100%); background: -o-linear-gradient(rgba(0,0,0,0.6) 30%, transparent 100%); background: linear-gradient(rgba(0,0,0,0.6) 30%, transparent 100%);";
                    } else {
                        $title_gradient_bg = "background: none;";
                    }
                    //if (($num_collections == '3') && ($count == ($num_collections-1))) {  $med_pull = 6; }
                    //else { $pull = 0; }
                    echo '<div class="collection-box large-' . $large . ' medium-6 small-12 xsmall-12 ' . $box_classes . ' ">';

                    echo '<a href="' . $tub_link . '" target="' . $link_target . '" >

                            <div class="image-title-box  ' . $box_classes . ' ">';

                    //Image from media library
                    if ( ! empty( $image ) ) {
                        echo '<img src="' . $image . '" alt="' . $title . ' Family Image" />';
                    }

                    //CDN image
                    if ( ! empty( $cdn_image ) ) {
                        echo '<img src="' . $cdn_image . '" alt="' . $title . ' Family Image" />';
                    }


                    if ( ! empty( $title ) ) { ?>
                        <h3 style="<? echo $title_gradient_bg; ?>" class="title"><? echo $title ?></h3> <? }
                    echo '</div></a><!-- end image-title-box -->';
                    //description box
                    if ( ! empty( $desc ) ) {
                        echo '<div class="description-box">' . $desc . '</div><!-- end desc box -->';
                    }

                    echo '</div><!-- end collection box -->';
                    $count ++;
                endwhile;
                    //                          if(($count%4) != 0){
                    //                              echo '<div class="collection-box collection-extra large-'.$large.' medium-6 small-6 xsmall-12 ">';
                    //                              echo '<div class="description-box" >Your source for '.$current_category.' in the ((Location)) area!</div><!-- end desc box -->';
                    //                              echo '</div><!-- end collection box -->';
                    //                          } else {}
            else :
                // no rows found
            endif;


            ?>


            <?php } ?>


        </div>
        <!-- CLOSE row -->

    </div>
    <!-- CLOSE collection-wrapper -->




<div class="rento-linens-content">
    <div class="page-description">
        <div class="container">
           <p><?php echo get_field('description'); ?></p>
        </div>
    </div>
    <div class="rento-linens-cards">
        <div class="container">
            
            <?php
            // echo '<pre>';
            // print_r(get_field('image_grid_row'));
            // echo '</pre>';

            // check if the repeater field has rows of data
                if( have_rows('image_grid_row') ):
                        // loop through the rows of data
                        while ( have_rows('image_grid_row') ) : the_row();
                            $cards = get_sub_field('image_grid_cards');
                            // print_r($cards);
                            echo '<div class="rento-cards">';
                                foreach ($cards as $card) {
                                    echo '<div class="card">
                                        <img src="'.$card["s3_image_link"].'" alt="" />
                                        <p>'.$card["image_card_text"].'</p>
                                    </div>';
                                }
                            echo '</div>';
                        endwhile;

                else :

                        // no rows found

                endif;

            ?>
            
        </div>
    </div>
</div>