<style>
#middle-col {
    width:680px;
    margin:0 auto;
}

#middle-col p {
  padding:15px 0px;
}

.page-template-template-faq h2 {
    text-align:center !important;
}

/* FAQ PAGE */
ul.faq {
/*    max-width: 550px;*/
    max-width:750px;
    margin: 0 auto;

}
ul.faq > li::before {
    content: '+ ';
    color: #1e73be;
    cursor: pointer;
    display: inline-block;
    float: left;
    margin-top: 7px;
    font-weight: 600;
    font-family: sans-serif;
    margin-right: 20px;
}


ul.faq li p {
    font-size: 20px;
    line-height: 26px;
    text-align: left;
    padding-left: 29px;
}

ul.faq ul li,
ul.faq ol li {
        font-size: 20px;
        line-height:26px;
    }

.page-template-template-faq h3 {
  padding-left: 29px;
  text-align:left;
}


@media screen and ( max-width: 980px) {
    #middle-col {
      width:95%;
      margin:0 auto;
    }
}
</style>

<?php
$title = get_the_title();
$show_title_bar = get_field('show_title_bar');
$text_align = get_field('text_align');
$show_breadcrumbs = get_field('show_breadcrumbs');
$show_anchor = get_field('show_anchor_links'); ?>

<!-- Aspot -->
<?php echo mainAspot(); ?>


<?php if( !empty($show_title_bar) ){ ?>
    <div class="page-50-50-header">
        <div class="row">
            <div class="title-bar">
                <h2 class="title collections-page-title" style="text-align:<?php echo $text_align ?>!important; float:none;"><?php echo $title; ?></h2>
                <?php if(!empty($show_breadcrumbs)){ ?>
                    <?php if( !is_front_page() ): ?>
                              <div class="breadcrumbs">
                                <div class="row" style="text-align:<?php echo $text_align ?>;">
                                  <?php //BREADCRUMBS
                                  if ( function_exists('yoast_breadcrumb') ) {
                                    yoast_breadcrumb('','');
                                  }
                                  ?>
                                </div>
                              </div>
                            <?php endif; ?>
                    <?php } ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="row">
  <div class="faq-entry small-12 columns text-center">
  <?php echo the_content(); ?>
  </div>

</div>
 <div class="row">
    <?php
    $faqPageImage = get_field('faq_page_image');

     if(!empty($faqPageImage)) {

     ?>
     <div class="small-12 medium-6 columns" style="max-height: 415px;overflow: hidden;margin-bottom: 1em;">
        <img src="<?php echo $faqPageImage; ?>" style="outline: 1px solid rgba(204, 204, 204, 0.33);margin:0 auto 1em auto;display: block;">
    </div>


  <div class="small-12 medium-6 columns">

    <?php } else { ?>

      <div class="small-12 medium-12 columns" style="max-height: 10000px;margin-bottom: 1em;">

      <?php } ?>

      <div class="text-center">
<?php
          if( get_field('faq') )
          {
              echo '<ul class="faq">';
              $qnum = 1;
              $is_active = "is-active";
              while( the_repeater_field('faq') )
              {
                  $qnum++;
                  echo '<li class="dsAccordion' . $is_active . '">';
                  echo '<a><h3>'. get_sub_field('question') .'</h3></a>';
                  echo '<div class="panel '.$qnum.'d">' . get_sub_field('answer');
                  echo '</div></li>';
                  $is_active = "";
              }

              echo '</ul>';
          }

          ?>

        </div>
      </div>





</div>
