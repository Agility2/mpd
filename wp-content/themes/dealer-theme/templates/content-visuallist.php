<?php $show_title_bar = get_field('show_title_bar') ?>
<?php
          echo mainAspot();
?>

       <?php if(!empty($show_title_bar)){?>
        <div class="title-bar" style="max-width:960px; margin:0 auto;">
            <h2 class="title page-title" style="!important; float:none; text-align:center;"><?php echo the_title(); ?></h2>

                          <div class="breadcrumbs">
                            <div class="row" style="text-align:center;">
                              <?php //BREADCRUMBS
                              if ( function_exists('yoast_breadcrumb') ) {
                                yoast_breadcrumb('','');
                              }
                              ?>
                            </div>
                          </div>

            <div class="clearfix"></div>
        </div>
        <?php } ?>
<div class="content-container" style="max-width:<?php the_field('container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('container_padding_top'); ?>px; padding-bottom:<? the_field('container_padding_bottom'); ?>px;">
  <div class="row">
  <?php the_content(); ?>
  </div>
</div>

<?php if( get_field('visual_list') ){ ?>
   <div class="the-vl cf">

    <?php while( the_repeater_field('visual_list') ){ ?>

    <div class="vl-item cf imgp_<?php echo get_sub_field('vl_img_position') ?>">

<?php if(get_sub_field('vl_type') == 'image') { ?>
        <div class="fifty-50 the-image">

            <img src="<?php echo get_sub_field('vl_image')  ?>" alt="<?php echo the_title(); ?> Visual List Item Image">
        </div>
<?php } ?>
<?php if(get_sub_field('vl_type') == 'video') { ?>
        <div class="fifty-50 the-video">

          <?php echo get_sub_field('vl_embed_video_code') ?>
        </div>
<?php } ?>

<?php if(get_sub_field('vl_type') == 'text') { ?>
        <div class="fifty-50 the-text">

          <p>
            <?php echo get_sub_field('vl_text') ?>
          </p>
        </div>
<?php } ?>
        <div class="fifty-50 the-content">
            <?php echo get_sub_field('vl_content') ?>
        </div>


    </div>

    <?php }//end while visual_list ?>



    </div>
<?php }//end if visual_list?>
