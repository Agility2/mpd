<?php
$show_featured_products = get_field('show_featured_products');
if (!empty($show_featured_products) ) { ?>
    <div class="product-carousel">
    <div class="row">
       <h2 class="title" ><?php the_field('featured_section_header'); ?></h2>
        <div class="product-model-section">
            <div class="product-model-slider">
                <?php
                        $selected_products = get_field('select_products');
                        if($selected_products){
                           foreach($selected_products as $post){
                               setup_postdata($post);
                                    $product_name = get_the_title();
                               //line right below removed by alex called category even tho its on homepage no need for category call
//                                    $current_category = get_category();
                                    /*$collection_field = get_field('product-collection');
                                    $collection = get_term($collection_field, 'product_cat');
                                    $collection_slug = $collection->slug;
                                    $collection_name = $collection->name;
                                    $product_cap = get_field('product_cap');
                                    $collection_link = '/hot-tubs/'.$collection_slug;
                                    $width = get_field('width');
                                    $length = get_field('length');
                                    $height = get_field('height');
                                    $dim = $width. ' x ' .$length. ' x ' .$height;
                                    $vol = get_field('product_volume');*/

                                    $mini_description = get_field('snappy_description');

                                    $link = get_permalink($query->post->ID);

                                    echo '<div class="product-slider-box columns">';
                                            $feat_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');
                                            if(!empty('the_field("top_down_image_thumbnail_url")')){
                                              $top_image = get_field('top_down_image_thumbnail_url');
                                            } else {
                                              $top_image = get_field('top_down_image_thumbnail');
                                            }

                                            // TONY PREVIOUSLY WANTED THE FEATURED IMAGE TO APPEAR BEFORE THE TOP DOWN IMAGE, CHANGED HIS MIND LATER -TODD 5/17/16
                                            // if (!empty($feat_img[0])) :
                                            //     echo '<a href="'.$link.'"><img src="'.$feat_img[0].'" alt="'.$product_name.' Product Image" /></a>';
                                            // else :
                                            //
                                            //     if(!empty($top_image) ):
                                            //           echo '<a href="'.$link.'"><img src="'.$top_image.'" alt="'.$product_name.' Product Image"/></a>';
                                            //     else : echo '<p class="text-centered">NO IMAGE</p>';
                                            //     endif;
                                            // endif;
                                            if(!empty($top_image) ):
                                                  echo '<a href="'.$link.'"><img src="'.$top_image.'" alt="'.$product_name.' Product Image"/></a>';
                                            else : echo '<p class="text-centered"></p>';
                                                if (!empty($feat_img[0])) :
                                                    echo '<a href="'.$link.'"><img src="'.$feat_img[0].'" alt="'.$product_name.' Product Image" /></a>';
                                                else :
                                                endif;
                                            endif;

                                            echo '<a href="'.$link.'"><p class="product-model-slide-title"><span>'.$product_name.'</span></p>';
                                            echo '<p>'.$mini_description.'</p></a>';
                                        echo '<div class="clearfix"></div>';
                                    echo '</div>';
                           }
                        wp_reset_postdata();
                        }
                    ?>

            </div>
        </div>
    </div>
</div>
<?php } ?>
