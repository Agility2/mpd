<?php
    $header_img = get_field('blog_header_image2', 'option');
    $header_subtitle = get_field('blog_sub_title', 'option');
    $header_overlay = get_field('blog_overlay_color', 'option');
    $overlay_opacity = get_field('blog_overlay_opacity', 'option');
    $blog_search = get_field('field_5729125fce666', 'option');
?>
<?php if(!empty($header_img)){ ?>
    <div class="hero" style="background-image:url('<?php echo $header_img ?>');">
       <?php //if( !empty($header_overlay) ){ ?>
            <div class="overlay" style="background-color:<?php echo $header_overlay ?>; opacity:.<?php echo $overlay_opacity ?>;"></div>
        <?php //} ?>
        <img src="<?php echo $header_img ?>" alt="">
        <div class="the-content">

            <h2><?php echo the_title(); ?></h2>

            <?php if( !empty($header_subtitle) ){ ?>
                <h3><?php echo $header_subtitle ?></h3>
            <?php } ?>

        <?php if( !empty($blog_search) ){ ?>
            <!-- Search Blog posts -->

            <form role="search" class="blog-search" method="get" action="<?= esc_url(home_url('/')); ?>">

            <input type="search" size="16" value="" name="s" class="search-field form-control" placeholder="Search Blog Posts" required>

            <input type="hidden" name="post_type" value="blog-posts" /> <!-- // hidden 'blog-posts' value -->

            </form>

        <? } ?>



            <div class="dropdown">
                <a href="javascript:;" class="dropdown"><?php if (!empty($current_category)) echo $current_category[0]->name; else echo 'Categories'; ?></a>
                <ul>
                <?php
                    $args = array(
                    //'show_option_all'    => '',
                    'orderby'            => 'name',
                    'order'              => 'ASC',
                    'style'              => 'list',
                    //'show_count'         => 0,
                    'hide_empty'         => 1,
                    'use_desc_for_title' => 1,
                    //'child_of'           => 0,
                    //'feed'               => '',
                    //'feed_type'          => '',
                    //'feed_image'         => '',
                    //'exclude'            => '',
                    //'exclude_tree'       => '',
                    //'include'            => '',
                    'hierarchical'       => 1,
                    //'title_li'           => __( 'Categories' ),
                    'show_option_none'   => __( '' ),
                    'number'             => null,
                    'echo'               => 1,
                    //'depth'              => 0,
                    //'current_category'   => 0,
                    //'pad_counts'         => 0,
                    'taxonomy'           => 'category',
                    'walker'             => null
                    );
                    wp_list_categories('title_li=0');
                ?>
                </ul>
            </div>

            <!-- Date Filtered dropdown -->
            <div class="dropdown">
                <a href="javascript:;" class="dropdown">Date</a>
                <ul>

                <?php
                    $args = array(
                    //'show_option_all'    => '',
                    'orderby'            => 'name',
                    'order'              => 'DESC',
                    'style'              => 'list',
                    //'show_count'         => 0,
                    'hide_empty'         => 1,
                    'use_desc_for_title' => 1,
                    //'child_of'           => 0,
                    //'feed'               => '',
                    //'feed_type'          => '',
                    //'feed_image'         => '',
                    //'exclude'            => '',
                    //'exclude_tree'       => '',
                    //'include'            => '',
                    'hierarchical'       => 1,
                    //'title_li'           => __( 'Categories' ),
                    'show_option_none'   => __( '' ),
                    'number'             => null,
                    'echo'               => 1,
                    //'depth'              => 0,
                    //'current_category'   => 0,
                    //'pad_counts'         => 0,
                    'type'               => monthly,
                    'walker'             => null
                    );
                    wp_get_archives('title_li=0');
                ?>
                </ul>
            </div>
            <?//= custom_search_form( null, 'Search posts', 'post'); ?>
        </div>
    </div>
<?php } ?>

<?php
$blog_style = get_field('blog_style' , 'option');
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type'     => 'post',
    /*'reviews_cat'   => $cat_term->slug,*/
    'posts_per_page' => -1
    );
$query = new WP_Query( $args );
    // The Loop
    if ( $query->have_posts() ) {
?>
<div class="the-posts <?php echo $blog_style ?>-grid" style="max-width:1200px; margin:0 auto; padding: 1em;">

    <?php
        if( $blog_style == 'masonry' ){
    ?>
        <?php
            while ( $query->have_posts() ) {
                $query->the_post();
                $title = get_the_title();
                $featured_img = $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(450, 250) );
                $summary = get_field('blog_summary');
        ?>
        <article <?php post_class('masonry-item') ?>>
           <?php if($featured_img){ ?>
               <a href="<?php the_permalink() ?>">
                    <div class="head">
                        <img src="<?php echo $featured_img[0] ?>" alt="<?php echo $title ?>">
                    </div>
                </a>
            <?php } ?>
            <div class="body">
                <a href="<?php the_permalink() ?>"><h3><?php echo $title ?></h3></a>
                <?php if( !empty($summary) ){ ?>
                    <p><?php echo $summary ?></p>
                    <a href="<?php echo the_permalink() ?>">Read More</a>
                <?php }else{ ?>
                <p><?php the_excerpt() ?></p>
                <?php } ?>
            </div>
        </article>
        <?php }//end while ?>
        <?php wp_reset_postdata(); ?>
    <?php } //end if masonry style
        if( $blog_style == 'list' ){
    ?>
        <?php
            while ( $query->have_posts() ) {
            $query->the_post();
            $title = get_the_title();
            $featured_img = $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(450, 250) );
        ?>

            <article <?php post_class(); ?>>
                <header>
                <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <?php get_template_part('templates/entry-meta'); ?>
                </header>
                <div class="entry-left-side large-4 small-12 xsmall-12">
                <?php
                if($featured_img){
                echo '<img src="'.$featured_img[0].'" alt="'.$title.'">';
                }
                ?>
                </div>
                <div class="entry-summary entry-right-side large-8 small-12 xsmall-12">
                <?php the_excerpt(); ?>
                </div>
                <div class="clear"></div>
            </article>

        <?php }//end while ?>
        <?php wp_reset_postdata(); ?>
    <?php }//end if list style ?>
</div>

<?php }//end if have psots ?>
