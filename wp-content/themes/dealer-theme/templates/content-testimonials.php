<?php
// Extract Page Data Field Variables
$title = get_the_title();
$content = get_the_content();

//get image variables
$left_quote = '/wp-content/uploads/2015/06/left-quote.png';
$right_quote = '/wp-content/uploads/2015/06/right-quote.png';
$quote = get_field('review-quote');
$owner = get_field('review-owner');
$location = get_field('review-location');
$score = get_field('review-score');
$raw_score = get_field('review-score');
$score = round($raw_score);
$score_img = '/wp-content/uploads/2015/07/'.$score.'_star_rating.png';

//Display Fields
?>
<style>

#middle-col {
    width:100%;
    margin:0 auto;
}

#middle-col p {
  padding:15px 0px;
}

.page-template-template-middle-col h2 {
    text-align:center;
}

/*************************************/
/***** TESTIMONIALS TEMPLATE PAGE ****/
/*************************************/


article.review {
  padding:40px;
  margin:80px 0;
  box-shadow: 0 0.15em 0.35em 0 rgba(0, 0, 0, 0.135);
}

article.review:first-of-type {
  margin-top:40px;
}

section.testimonials.cf article.review:nth-child(odd) {
  background:#F2F2F2;
}

section.testimonials.cf article.review:nth-child(2n) {
  background:#ccc;
}

section.testimonials.cf h2.entry-title {
  font-size:4em;
  line-height: 1.2;
  margin-bottom:10px;

}

section.testimonials.cf p.review-location {
  font-size: 2em;
  margin-bottom:0;
  padding-bottom:0;
}

section.testimonials.cf p.review-quote {
  font-size: 1.75em;
  line-height: 1.4;
  font-style: italic;
  max-height:178px;
  overflow:hidden;
  margin-bottom:0;
  padding-bottom:0;
  transition-duration: 1s;
}


img.star-rating {
  margin:30px 0;
}

a.review-expander {
  margin-bottom: 20px;
  display: inline-block;
  font-size:20px;
}

section.testimonials.cf p.review-quote.expander-active {
  max-height: 1000px;
}


@media screen and ( max-width: 980px) {
    #middle-col {
      width:95%;
      margin:0 auto;
    }
}
</style>

 <?php mainAspot(); ?>
 
  <div class="page-middle-col-content">
    <div class="row">
      <div id="middle-col">
        <?php the_content(); ?>

<?php
        global $post;

//     $current_post_type = get_post_type( $post );

//     $category = get_the_category($post->ID);
//     $category = $category[0]->cat_ID;

     $args = array(
         'posts_per_page' => 12,
         'order' => 'DESC',
         'orderby' => 'name',
         'tag' => 'dealer-reviews',
         'post_type' => 'reviews',
     );

     // Create the related query
     $rel_query = new WP_Query( $args );

     // Check if there is any related posts
     if( $rel_query->have_posts() ) :
     ?>
     <section class="testimonials cf">
         <?php
             // The Loop
             while ( $rel_query->have_posts() ) :
                 $rel_query->the_post();
         ?>

                     <article class="review" style="text-align:center;">
                       <div class="left-quote large-2 medium-2 small-1 columns"><img src="<?php echo $left_quote; ?>" style="float:right;"/></div>
                       <div class="large-8 medium-8 small-12 columns"><h2 class="entry-title"><?php the_title(); ?></h2></div>
                       <div class="right-quote large-2 medium-2 small-1 columns"><img src="<?php echo $right_quote; ?>" style="float:left;" /></div>
                       <div class="clearfix"></div>

                        <div class="large-12 columns mobile-quotes"><img src="/wp-content/uploads/2015/05/commas-174x54.png" /></div>



                        <?php $revScore = get_field('review-score');
                        if($revScore == 5){
                          echo '<img class="star-rating" src="/wp-content/uploads/2015/07/5_star_rating.png" />';
                        }
                        elseif($revScore == 4){
                          echo '<img class="star-rating" src="/wp-content/uploads/2015/07/4_star_rating.png" />';
                        }
                        elseif($revScore == 3){
                          echo '<img class="star-rating" src="/wp-content/uploads/2015/07/3_star_rating.png" />';
                        }
                        elseif($revScore == 2){
                          echo '<img class="star-rating" src="/wp-content/uploads/2015/07/2_star_rating.png" />';
                        }
                        elseif($revScore == 1){
                          echo '<img class="star-rating" src="/wp-content/uploads/2015/07/1_star_rating.png" />';
                        }
                         ?>

                         <div class="review-container">
                           <p class="review-quote blockquote"><?php echo strip_tags(get_field('review-quote')); ?></p>
                           <a class="review-expander">...Read more test</a>
                         </div>
                         <p class="review-location"><span class="review-owner"><?php echo get_field('review-owner'); ?> &mdash; </span><?php echo get_field('review-location'); ?></p>
                     </article>

         <?php
             endwhile;
         ?>

     </section>
     <?php
     endif;

     // Reset the query
     wp_reset_query();

  ?>

      </div>
    </div>
