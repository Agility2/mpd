<?php $show_title_bar = get_field('show_title_bar') ?>

       <?php if(!empty($show_title_bar)){?>
        <div class="title-bar" style="max-width:960px; margin:1em auto 1.5em;">
            <h2 class="title page-title" style="!important; float:none; text-align:center; margin: 0;"><?php echo the_title(); ?></h2>
            <div class="clearfix"></div>
        </div>
        <?php } ?>

        <div class="container">
          <div class="content-container" style="max-width:<?php the_field('container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('container_padding_top'); ?>px; padding-bottom:<? the_field('container_padding_bottom'); ?>px;">
            <?php the_content(); ?>
          </div>
        </div>


<div id="gallery">

<?php $images = get_field('gallery'); ?>

<?php if( $images ): ?>
<div class="gallery-page">
    <?php foreach( $images as $image ): ?>
        <div><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['caption']; ?>"><div class="caption"><a href="javascript:;">Read More</a><p><?php echo $image['caption']; ?></p></div></div>
    <?php endforeach; ?>
</div>
<?php endif; ?>


<?php if( $images ): ?>
<div class="gallery-page-nav">
    <?php foreach( $images as $image ): ?>
        <div><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['caption']; ?>"></div>
    <?php endforeach; ?>
</div>
<?php endif; ?>

<script>
   $('.gallery-page-slider').slick({
     slidesToShow: 1,
     slidesToScroll: 1,
     arrows: true,
     fade: true,
     asNavFor: '.slider-nav'
   });

   $('.slider-nav').slick({
     slidesToShow: 3,
     slidesToScroll: 1,
     asNavFor: '.gallery-page-slider',
     dots: true,
     centerMode: true,
     focusOnSelect: true
   });
</script>

<?php
// Retrieve General Content Rows
$general_rows = get_field('general_content_block_selector');

$general_rows_orders = array();

// Place General Content Row orders into array
$num_general_rows = 0;

if($general_rows){

    foreach($general_rows as $row){

        $general_rows_orders[$num_general_rows] = $row['global_page_order'];

        $num_general_rows++;
    }
}

// Add main section orders to single array - $sections_array
$sections_array = array($reviews_order, $products_order, $brands_order);

// Add General Content Row orders to array - $sections_array
if(!empty($general_rows_orders)){

    foreach($general_rows_orders as $order){

        array_push($sections_array, $order);
    }
}

/*
 * Loop through the all sections in $sections_array.
 * Displays them according to their section orders.
 * Sections with equivalent orders will cause errors.
 */
$order_count = count($sections_array) + 1 ;
for($i=0; $i<10; $i++){
        $row_num = 0;
        foreach($general_rows as $row){
            if($row['global_page_order'] == $i){
               set_query_var('row_num', $row_num);
                /*----- GENERAL CONTENT ROW (GCR) SECTION -----*/
                /*------- can display multiple GCRs -----------*/
                get_template_part('/templates/blocks/blocks', 'general');
            }
            else {
                $row_num++;
            }
        }
}
            ?>
