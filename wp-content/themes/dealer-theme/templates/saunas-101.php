<?php
/**
 * Template Name: Saunas 101 Page
 */
?>

<?php echo mainAspot(); ?>

<main class="main-saunas-101">
	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<div class="row">
				<div class="columns small-12">
					<div class="saunas-content">
						<?php the_content(); ?>
					</div>
				</div>
			</div>

			<?php if ( have_rows( 'links_pages' ) ): ?>
				<section class="saunas-links">
					<div class="row">
						<?php while ( have_rows( 'links_pages' ) ) : the_row(); ?>
							<div class="columns <?php echo ( get_row_index() == 1 ) ? 'xsmall-12 large-6' : 'xsmall-6 large-3'; ?>">
								<div class="saunas-links__item">
									<div class="saunas-links__image" style="background-image: url(<?php echo get_sub_field( 'image_link' ); ?>)">
										<?php
										if ( $link = get_sub_field( 'link' ) ):
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self';
											?>
											<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
										<?php endif; ?>
									</div>
									<?php if ( $description = get_sub_field( 'description' ) ): ?>
										<div class="saunas-links__description">
											<p><?php echo $description; ?></p>
										</div>
									<?php endif ?>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
				</section>
			<?php endif; ?>

			<?php if ( have_rows( 'benefits' ) ): ?>
				<section class="saunas-benefits">
					<div class="row flex flex-wrap">
						<?php if ( $title = get_field( 'benefits_title' ) ): ?>
							<div class="columns small-12 text-center">
								<h2 class="saunas-benefits__title"><?php echo $title; ?></h2>
							</div>
						<?php endif ?>
						<?php while ( have_rows( 'benefits' ) ) : the_row(); ?>
							<div class="columns small-12 medium-6 large-4 text-center">
								<div class="saunas-benefits__item">
									<?php echo wp_get_attachment_image( get_sub_field( 'icon' ), 'thumbnail' ); ?>
									<?php if ( $title = get_sub_field( 'title' ) ): ?>
										<h4><?php echo $title; ?></h4>
									<?php endif ?>
									<?php if ( $content = get_sub_field( 'content' ) ): ?>
										<p><?php echo $content; ?></p>
									<?php endif ?>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
				</section>
			<?php endif; ?>

			<section class="saunas-brochure">
				<div class="row flex flex-wrap align-center">
					<div class="columns small-12 large-7 large-push-5">
						<div class="saunas-brochure__placeholder" style="background-image: url(<?php echo get_field( 'brochure_placeholder' ); ?>)">
							<?php if ( $bg_video_url = get_field( 'brochure_video' ) ): ?>
								<a class="saunas-brochure__play fancybox" href="<?php echo $bg_video_url; ?>"><i class="fa fa-play" aria-hidden="true"></i></a>
							<?php endif ?>
						</div>
					</div>
					<div class="columns small-12 large-5 large-pull-7">
						<div class="saunas-brochure__content">
							<?php the_field( 'brochure_content' );
							if ( $link = get_field( 'brochure_button' ) ):
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
								?>
								<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</section>

		<?php endwhile; ?>
	<?php endif; ?>
</main>
