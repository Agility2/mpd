<?php

// Check if footer background image is set
if ( get_field( 'add_footer_bg_image', 'option' ) ) { ?>

<!-- Sets image and other properties -->
    <style>
        footer {
            background-image: url(<?php the_field('footer_bg_image', 'option'); ?>);
            background-size: cover;
            background-repeat: no-repeat;
        }

    </style>

<?php } ?>

<div class="clearfix"></div>
<a id="pricing"></a>
<footer class="cf footer2">
    <a name="contact"></a>
    <section class="maps">
        <h2><? echo get_field( 'footer_contact_title', 'option' ); ?></h2>
        <nav>
            <?php $defaults = array(
                'theme_location' => '',
                'menu'           => 'Footer Contact',
                'container'      => '',
                'menu_class'     => 'footer-menu',
                'menu_id'        => '',
                'echo'           => true,
                'fallback_cb'    => 'wp_page_menu',
                'before'         => '',
                'after'          => '',
                'link_before'    => '',
                'link_after'     => '',
                'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth'          => 0,
                'walker'         => ''
            );
            wp_nav_menu( $defaults ); ?>
        </nav>
        <?php
        if ( get_field( 'footer_content_editor', 'option' ) ){
        $content_count = 0;
        ?>
        <div class="content-wrapper cf">
            <div class="gencontent-wrap cf">
                <?php
                while ( the_repeater_field( 'footer_content_editor', 'option' ) ) {
                    $content_count ++;
                    $alignment = get_sub_field( 'title_social_media_align' );
                    ?>
                    <div class="gencontent fcount<?php echo $content_count; ?>">

                        <h3 style="text-align:<? echo $alignment; ?>; margin-bottom:20px;"><?php echo get_sub_field( 'tab_title' ); ?> </h3>


                        <?php echo get_sub_field( 'footer_content' ); ?>


                        <div class="cf social-button-wrap" style="margin-bottom:20px;text-align:<? echo $alignment; ?>">
                            <?php
                            while ( the_repeater_field( 'location_specific_social_media', 'option' ) ) {

                                $social_venue = get_sub_field( 'location_social_media_selector' );
                                $social_url   = get_sub_field( 'social_media_url' );
                                ?>
                                <div class="social-button col-social" style="text-align:left; width:auto;">
                                    <a style="width:30px;height:30px;float:left;margin:5px 5px 0;"
                                       href="<? echo $social_url; ?>"
                                       class="<? echo $social_venue; ?>-social social-icon" target="_blank"></a>
                                </div><!--end social-button -->

                                <?php
                            } //end while
                            ?>
                        </div><!--end social-button-wrap -->

                    </div><!--end gen-content -->
                    <?php
                }//end while
                }//end if
                ?>
            </div><!--end gencontent-wrap -->

            <?php
            if ( get_field( 'footer_content_editor2', 'option' ) ){
            // $content_count2 = 0;

            ?>
            <div class="gencontent-wrap gencontent-wrap2 cf" style="padding:0em 1em 3em;"> <?
                ?>

                <?php
                while ( the_repeater_field( 'footer_content_editor2', 'option' ) ) {

                    $alignment2 = get_sub_field( 'title_social_media_align2' );
                    ?>
                    <div class="gencontent2 fcount<?php echo $content_count; ?>" style="float:left;">
                        <h3 style="text-align:<? echo $alignment2; ?>; margin-bottom:20px;"><?php echo get_sub_field( 'tab_title2' ); ?> </h3>
                        <div class="cf social-button-wrap"
                             style="text-align:<? echo $alignment2; ?>; margin-bottom:20px;">
                            <?php
                            while ( the_repeater_field( 'location_specific_social_media2', 'option' ) ) {

                                $social_venue2 = get_sub_field( 'location_social_media_selector2' );
                                $social_url2   = get_sub_field( 'social_media_url2' );
                                ?>
                                <div class="social-button" style="text-align:left; width:auto;">
                                    <a style="width:30px;height:30px;float:left;padding:2px;"
                                       href="<? echo $social_url2; ?>"
                                       class="<? echo $social_venue2; ?>-social social-icon" target="_blank"></a>
                                </div><!--end social-button -->
                                <?php
                            } //end while
                            ?>

                        </div><!--end social-button-wrap -->

                        <?php echo get_sub_field( 'footer_content2' ); ?>

                    </div><!--end gencontent2 -->
                    <?php
                }//end while
                }//end if
                ?>
            </div><!--end gencontent-wrap2 -->

            <?php
            $footer_map = get_field( 'footer_map', 'option' );
            if ( ! empty( $footer_map ) ) {
                ?>
                <div class="gmap">
                    <?php echo $footer_map ?>
                </div><!--end gmap -->
                <?php
            }//end if footer map
            ?>
            <div class="copy social">

                <div class="social-button footer2">
                    <?php

                    $social_icons = get_field( 'footer_social_icons', 'option' );

                    if ( in_array( 'Facebook', $social_icons ) ) {
                        $facebook_url = get_field( 'facebook_url', 'option' );
                        echo '<a href="' . $facebook_url . '" class="facebook-social social-icon" target="_blank"></a>';
                    }
                    if ( in_array( 'Youtube', $social_icons ) ) {
                        $youtube_url = get_field( 'youtube_url', 'option' );
                        echo '<a href="' . $youtube_url . '" class="youtube-social social-icon" target="_blank"></a>';
                    }
                    if ( in_array( 'Yelp', $social_icons ) ) {
                        $yelp_url = get_field( 'yelp_url', 'option' );
                        echo '<a href="' . $yelp_url . '" class="yelp-social social-icon" target="_blank"></a>';
                    }
                    if ( in_array( 'Houzz', $social_icons ) ) {
                        $houzz_url = get_field( 'houzz_url', 'option' );
                        echo '<a href="' . $houzz_url . '" class="houzz-social social-icon" target="_blank"></a>';
                    }
                    if ( in_array( 'Twitter', $social_icons ) ) {
                        $twitter_url = get_field( 'twitter_url', 'option' );
                        echo '<a href="' . $twitter_url . '" class="twitter-social social-icon" target="_blank"></a>';
                    }
                    if ( in_array( 'Google+', $social_icons ) ) {
                        $gplus_url = get_field( 'gplus_url', 'option' );
                        echo '<a href="' . $gplus_url . '" class="gplus-social social-icon" target="_blank"></a>';
                    }
                    if ( in_array( 'Instagram', $social_icons ) ) {
                        $gplus_url = get_field( 'insta_url', 'option' );
                        echo '<a href="' . $insta_url . '" class="insta-social social-icon" target="_blank"></a>';
                    }
                    if ( in_array( 'Pinterest', $social_icons ) ) {
                        $pinterest_url = get_field( 'pinterest_url', 'option' );
                        echo '<a href="' . $pinterest_url . '" class="pinterest-social social-icon" target="_blank"></a>';
                    }
                    if ( in_array( 'LinkedIn', $social_icons ) ) {
                        $linkedin_url = get_field( 'linkedin_url', 'option' );
                        echo '<a href="' . $linkedin_url . '" class="linkedin-social social-icon" target="_blank"></a>';
                    }

                    ?>
                </div><!--end social-button-->

                <div class="footer2-lower-menu">

                    <?php
                    $defaults = array(
                        'theme_location'  => '',
                        'menu'            => 'Lower Footer',
                        'container'       => 'div',
                        'container_class' => 'menu-footer-lower-footer-container',
                        'container_id'    => '',
                        'menu_class'      => 'menu',
                        'menu_id'         => 'menu-footer-lower-footer',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'before'          => '',
                        'after'           => '',
                        'link_before'     => '',
                        'link_after'      => '',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth'           => 0,
                        'walker'          => ''
                    );

                    wp_nav_menu( $defaults );
                    ?>

                </div><!--end footer2-lower-menu -->

                <div class="made-by text-center">
                    <a href="https://www.designstudio.com" target="_blank">SITE BY <img src="
<?php echo get_template_directory_uri(); ?>/dist/img/design-studio-logo.png" alt="Design Studio Logo"/><?= $site_by; ?>
                    </a>
                    <p class="copy-right"> &nbsp;|&nbsp; &copy;<?php echo strip_tags( date( 'Y' ) ) ?> All rights
                        reserved. <span class="version"><?php echo MPDversion(); ?></span></p>

                </div><!--end made-by -->

            </div><!--end copy-social -->

        </div><!--end content-wrapper -->


    </section>
</footer>
