<style>
    .entry-title {
        display:none;
    }
    .default.body_container h2 {
        line-height: 1.3;
    }
    .default.body_container p {
        /*font-size: 24px;
        line-height: 30px;*/
    }
    .default.body_container ul, .default.body_container ol, .default.body_container dl {
    font-size: 24px;
    line-height: 30px;
    margin-bottom: 1.25rem;
    list-style-position: outside;
    font-family: inherit;
}
</style>


<?php
// DEFAULT PAGE USES THIS TEMPLATE

// ?>
<?php //wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

<?php

$floating_column = get_field('floating_right_column');
if(!empty($floating_column)) {
  $floating_column = "floating_column";
}


// Extract Page Data Field Variables
$title = get_the_title();
$type_page = get_field('type_page');
$top_block = get_field('top_block');
$bottom_block = get_field('bottom_block');
$left_block = get_field('left_block');
$right_block = get_field('right_block');

$show_title_bar = get_field('show_title_bar');
$text_align = get_field('text_align');
$show_breadcrumbs = get_field('show_breadcrumbs');
$show_anchor = get_field('show_anchor_links');
//Display Fields
?>
<!-- PAGE TITLE test -->

<div class="default body_container <? echo $floating_column ?>">

  <?php mainAspot(); ?>

  <?php if( !empty($show_title_bar) ){ ?>
      <div class="page-50-50-header">
          <div class="row">
              <div class="title-bar">
                  <h2 class="title collections-page-title" style="text-align:<?php echo $text_align ?>!important; float:none;"><?php echo $title; ?></h2>
                  <?php if(!empty($show_breadcrumbs)){ ?>
                      <?php if( !is_front_page() ): ?>
                                <div class="breadcrumbs">
                                  <div class="row" style="text-align:<?php echo $text_align ?>;">
                                    <?php //BREADCRUMBS
                                    if ( function_exists('yoast_breadcrumb') ) {
                                      yoast_breadcrumb('','');
                                    }
                                    ?>
                                  </div>
                                </div>
                              <?php endif; ?>
                      <?php } ?>
                  <div class="clear"></div>
              </div>
          </div>
      </div>
  <?php }else{ ?>
    <div class="page-50-50-content" style="padding-top:32px;">
     <?php } ?>
      <div class="row">
          <!-- TOP BLOCK -->

          <!-- <div class="xsmall-12 columns">
              <?php the_content(); ?>

          </div>
           -->


          <?php if(!empty($top_block)) { ?>
          <div class="xsmall-12 columns">
              <div class="top-block-50-50">
                <div class="content-container" style="max-width:<?php the_field('container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('container_padding_top'); ?>px; padding-bottom:<? the_field('container_padding_bottom'); ?>px;">
                  <?php echo $top_block; ?>
                </div>
                  <div class="clear"></div>
              </div>
          </div>
          <div class="clear"></div>
          <?php } else { ?>
            <div class="xsmall-12 columns">
                <div class="top-block-50-50">
                  <div class="content-container" style="max-width:<?php the_field('container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('container_padding_top'); ?>px; padding-bottom:<? the_field('container_padding_bottom'); ?>px;">
                    <?php the_content(); ?>
                  </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
        <?  } ?>
          <div class="two-column xsmall-12">

              <!-- LEFT BLOCK -->











              <?php if(!empty($left_block)) { ?>
              <div class="large-6 medium-6 xsmall-12 columns">
                  <div class="left-block-50-50">
                      <?php echo $left_block; ?>
                      <div class="clear"></div>
                  </div>
                  <?php
                  // check if the repeater field has rows of data
                  if( have_rows('left_slider') ):
                      echo '<div class="left-block-slider">';
                          echo '<div class="slider">';
                      while ( have_rows('left_slider') ) : the_row();
                          $slide_type = get_sub_field('slide_type');

                          echo '<div>';
                              if($slide_type == 'Image'){
                                  $image = get_sub_field('l-image');
                                  if(!empty($image)){
                                  echo '<img src="'.$image.'" />';
                                  }
                              }
                              elseif($slide_type == 'Video'){
                                  $video = get_sub_field('l-video');
                                  echo $video;
                              }
                              else { /*nothing*/  }
                              echo '</div><!-- End current slide-->';
                      endwhile;

                      echo '</div><!-- end slider-->';
                      echo '</div>';

                  else :
                      // no rows found
                  endif;
                  ?>
              </div>
              <?php } ?>
              <!-- RIGHT BLOCK -->
              <?php if(!empty($right_block)) { ?>
              <div class="large-6 medium-6 xsmall-12 columns">
                  <div class="right-block-50-50">
                      <?php echo $right_block; ?>
                      <div class="clear"></div>
                  </div>
                  <?php
                  // check if the repeater field has rows of data
                  if( have_rows('right_slider') ):
                      echo '<div class="right-block-slider">';
                          echo '<div class="slider">';
                      while ( have_rows('right_slider') ) : the_row();
                          $slide_type = get_sub_field('slide_type');
                          echo '<div>';
                              if($slide_type == 'Image'){
                                  $image = get_sub_field('image');
                                  if($image){
                                  echo '<img src="'.$image.'" />';
                                  }
                              }
                              elseif($slide_type == 'Video'){
                                  $video = get_sub_field('video');
                                  echo $video;
                              }
                              else { /*nothin*/  }
                          echo '</div><!-- End current slide-->';
                      endwhile;
                      echo '</div><!-- end slider-->';
                      echo '</div>';
                  else :
                      // no rows found
                  endif;
                  ?>
              </div>
              <?php } ?>
              <div class="clear"></div>
          </div>
          <div class="clear"></div>

          <!-- BOTTOM BLOCK -->
          <?php if(!empty($bottom_block)) { ?>
          <div class="xsmall-12 columns">
              <div class="top-block-50-50">
                <div class="content-container" style="max-width:<?php the_field('bottom_container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('bottom_container_padding_top'); ?>px; padding-bottom:<? the_field('bottom_container_padding_bottom'); ?>px;">
                  <?php echo $bottom_block; ?>
                </div>
                  <div class="clear"></div>
              </div>
          </div>
          <div class="clear"></div>
          <?php } ?>

      </div>
  </div>




              <?php
  // Retrieve General Content Rows
  $general_rows = get_field('general_content_block_selector');

  $general_rows_orders = array();

  // Place General Content Row orders into array
  $num_general_rows = 0;

  if($general_rows){

      foreach($general_rows as $row){

          $general_rows_orders[$num_general_rows] = $row['global_page_order'];

          $num_general_rows++;
      }
  }

  // Add main section orders to single array - $sections_array
  $sections_array = array($reviews_order, $products_order, $brands_order);

  // Add General Content Row orders to array - $sections_array
  if(!empty($general_rows_orders)){

      foreach($general_rows_orders as $order){

          array_push($sections_array, $order);
      }
  }

  /*
   * Loop through the all sections in $sections_array.
   * Displays them according to their section orders.
   * Sections with equivalent orders will cause errors.
   */
  $order_count = count($sections_array) + 1 ;
  for($i=0; $i<10; $i++){
          $row_num = 0;
          if(!empty($general_rows)){
              foreach($general_rows as $row){
                  if($row['global_page_order'] == $i){
                     set_query_var('row_num', $row_num);
                      /*----- GENERAL CONTENT ROW (GCR) SECTION -----*/
                      /*------- can display multiple GCRs -----------*/
                      get_template_part('/templates/blocks/blocks', 'general');
                  }
                  else {
                      $row_num++;
                  }
              }
          }
  }
              ?>
</div>
