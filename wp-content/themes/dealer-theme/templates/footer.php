<?php   //FOOTER DATA VARIABLES
$footer_title = get_field('footer_contact_title','option');
$footer_left_content = get_field('footer_left_content','option');
$footer_address = get_field('footer_contact_info_1','option');
$footer_contact = get_field('footer_contact_info_2','option');
/*$footer_social = get_field('footer_contact_social','option');*/
$footer_map = get_field('footer_map','option');
$copyright = get_field('copyright','option');
$privacy_page = get_field('privacy_policy_page', 'options');
$privacy_title = get_field('privacy_policy_page_title','options');
$site_by = get_field('site_by','option');


$social_icons = get_field('footer_social_icons', 'option');
/*list($facebook, $youtube, $yelp, $houzz) = explode($social_icons);*/

if ( empty($privacy_title) ) {
    $privacy_title = $privacy_page->post_title;
}
?>

<?php

if (get_field('add_footer_bg_image', 'option')) {
    
}
 ?>
<style>
    footer {
        background-image: url(<?php the_field('footer_bg_image', 'option'); ?>);
    }

</style>
 <img src="<?php echo the_field('footer_bg_image', 'option'); ?>"


<div class="clearfix"></div>
<?php //PRINT THE FOOTER
//if( !is_front_page() ): 
?>
<a id="pricing"></a>

<div class="category-contact">
    <a id="contact"></a>
    <h2 style="margin:0 0 40px 0; text-align:center;"><?php echo $footer_title; ?></h2>

    <?php
        $defaults = array(
            'theme_location'  => '',
            'menu'            => 'Footer Contact',
            'container'       => 'div',
            'container_class' => 'menu-footer-contact-footer-container',
            'container_id'    => '',
            'menu_class'      => 'menu',
            'menu_id'         => 'menu-footer-contact-footer',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
        );

        wp_nav_menu( $defaults );
    ?>
    <?php //dynamic_sidebar('Footer Menu')?> 
    <?php //dynamic_sidebar('footer')?>
    <!-- CONTACT INFO -->
    <div class="footer-main">
        <div class="row"> 
            <div class="contact-info large-5 xsmall-12 columns">
                <!-- SECTION TITLES -->
                <?php echo $footer_left_content; ?>
                <!-- ADDRESS -->
                <div class="contact-address">
                    <?php echo $footer_address; ?>
                    <!-- CONTACT DETAILS -->
                    <div class="contact-numbers contact-address">
                        <?php echo $footer_contact; ?>
                    </div>
                    <!-- SOCIAL -->
                    <?php //echo $footer_social; ?>  
                    <div class="social-button " style="padding-bottom: 0;">
                    <?php 
                    
                     if( in_array('Facebook', $social_icons) ) {
                        $facebook_url = get_field('facebook_url','option');
                        echo '<a href="'.$facebook_url.'" class="facebook-social social-icon" target="_blank"></a>';
                    }
                    if( in_array('Youtube', $social_icons) ) {
                        $youtube_url = get_field('youtube_url','option');
                        echo '<a href="'.$youtube_url.'" class="youtube-social social-icon" target="_blank"></a>';
                    }
                    if( in_array('Yelp', $social_icons) ) {
                        $yelp_url = get_field('yelp_url','option');
                        echo '<a href="'.$yelp_url.'" class="yelp-social social-icon" target="_blank"></a>';
                    }
                    if( in_array('Houzz', $social_icons) ) {
                        $houzz_url = get_field('houzz_url','option');
                        echo '<a href="'.$houzz_url.'" class="houzz-social social-icon" target="_blank"></a>';
                    }
                    ?> 
                    </div>
                </div>
            </div>


            <div class="contact-map large-7 xsmall-12 columns">
                <p><?php echo $footer_map; ?></p>
            </div>
            <div class="clearfix"></div>

        </div>
    </div>
</div>    

<footer class="footer" id="footer">

    <div class="row">
        <div class="small-12 xsmall-12 small-centered text-center columns">
            <span class="copyright"><?php echo $copyright; ?></span>

            
            <?php
                $defaults = array(
                    'theme_location'  => '',
                    'menu'            => 'Lower Footer',
                    'container'       => 'div',
                    'container_class' => 'menu-footer-lower-footer-container',
                    'container_id'    => '',
                    'menu_class'      => 'menu',
                    'menu_id'         => 'menu-footer-lower-footer',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth'           => 0,
                    'walker'          => ''
                );

                wp_nav_menu( $defaults );
            ?>
            

            <div class="made-by text-center"><a href="https://www.designstudio.com" target="_blank"><span><?php echo $site_by; ?></span></a></div>
        </div>
    </div>

</footer>

</div>


<a class="exit-off-canvas"></a>

</div>
</div>



