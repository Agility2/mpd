<div class="form-wrapper">
    <div class="esc-form"><a href="javascript:;" class="close-search">x</a></div>
    <form id="fast-search-desktop form-wrapper" class="form-wrapper" style="display:block !important;" role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
        <label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
        <div class="goSearch">
            <input class="popup-search-bar search" type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search Products&hellip;', 'placeholder', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'woocommerce' ); ?>" />
            <input type="Submit" value="Go">
<!--            <a class="goSearch" href="javascript:;">Go</a>-->
        </div>
        <input type="hidden" name="post_type" value="product" />
    </form>
</div>
