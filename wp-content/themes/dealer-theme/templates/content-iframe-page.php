<!-- <h2><?php the_title(); ?></h2> -->

<?php
$title = get_the_title();
$show_title_bar = get_field('show_title_bar');
$text_align = get_field('text_align');
$show_breadcrumbs = get_field('show_breadcrumbs');
$show_anchor = get_field('show_anchor_links');
$text_align = get_field('text_align');

echo mainAspot();

if( !empty($show_title_bar) ){ ?>
        <div class="row">
            <div class="title-bar">
                <h2 class="title collections-page-title" style="text-align:<?php echo $text_align ?>!important; float:none;"><?php echo $title; ?></h2>
                <?php if(!empty($show_breadcrumbs)){ ?>
                    <?php if( !is_front_page() ): ?>
                              <div class="breadcrumbs">
                                <div class="row" style="text-align:<?php echo $text_align ?>;">
                                  <?php //BREADCRUMBS
                                  if ( function_exists('yoast_breadcrumb') ) {
                                    yoast_breadcrumb('','');
                                  }
                                  ?>
                                </div>
                              </div>
                            <?php endif; ?>
                    <?php } ?>
                <div class="clear"></div>
            </div>
          </div>
<?php } ?>
      <div class="content-container" style="max-width:<?php the_field('container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('container_padding_top'); ?>px; padding-bottom:<? the_field('container_padding_bottom'); ?>px;">
        <?php the_content(); ?>
    </div>

<?php
    $iframe = get_field('iframe_link');
    $iframe_height = get_field('iframe_height');
    $iframe_width = get_field('iframe_width');
    $iframe_scroll = get_field('iframe_scroll');
    if($iframe_scroll == 'True') {
      $iframe_scroll = 'Yes';
    } else {
      $iframe_scroll = 'No';
    }
?>

<div class="iframe-wrap">
<iframe scrolling="<?php echo $iframe_scroll ?>" src="<?php echo $iframe ?>" height="<?php echo $iframe_height ?>" width="<?php echo $iframe_width ?>" frameborder="0"></iframe>
</div>
