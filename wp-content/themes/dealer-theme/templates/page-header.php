<?php
  //use Roots\Sage\Titles;
    if( is_page_template( 'template-collections.php' ) || is_page_template('template-faq.php') || is_page_template('template-categories.php') || is_page_template( 'template-50-50.php' ) || is_page_template('page.php') || is_front_page() || is_home() || is_page_template( 'template-testimonials.php' ) ):
    else:
      if ( function_exists('custom_breadcrumb') ) {
        custom_breadcrumb();
      }
      echo '<h2 class="entry-title">' . get_the_title() . '</h2>';
        if( is_page_template('template-blog.php') ): ?>
                          <div class="breadcrumbs">
                            <div class="row">
                              <?php //BREADCRUMBS
                              if ( function_exists('yoast_breadcrumb') ) {
                                yoast_breadcrumb('','');
                              }
                              ?>
                            </div>
                          </div>
                        <?php endif;
    endif;
?>
