<?php

echo mainAspot();

?>

<div class="content-container" style="max-width:<?php the_field('container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('container_padding_top'); ?>px; padding-bottom:<? the_field('container_padding_bottom'); ?>px;">
  <?php the_content(); ?>
</div>

<?

// Retrieve General Content Rows
$general_rows = get_field('general_content_block_selector');

$general_rows_orders = array();

// Place General Content Row orders into array
$num_general_rows = 0;

if($general_rows){

    foreach($general_rows as $row){

        $general_rows_orders[$num_general_rows] = $row['global_page_order'];

        $num_general_rows++;
    }
}

// Add main section orders to single array - $sections_array
$sections_array = array($reviews_order, $products_order, $brands_order);

// Add General Content Row orders to array - $sections_array
if(!empty($general_rows_orders)){

    foreach($general_rows_orders as $order){

        array_push($sections_array, $order);
    }
}

/*
 * Loop through the all sections in $sections_array.
 * Displays them according to their section orders.
 * Sections with equivalent orders will cause errors.
 */
$order_count = count($sections_array) + 1 ;
for($i=0; $i<10; $i++){
        $row_num = 0;
        foreach($general_rows as $row){
            if($row['global_page_order'] == $i){
               set_query_var('row_num', $row_num);
                /*----- GENERAL CONTENT ROW (GCR) SECTION -----*/
                /*------- can display multiple GCRs -----------*/
                get_template_part('/templates/blocks/blocks', 'general');
            }
            else {
                $row_num++;
            }
        }
}
            ?>

<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
