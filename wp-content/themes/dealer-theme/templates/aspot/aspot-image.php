<?php
$desktopImg = get_field('aspot_image_desktop');
$mobileImg = get_field('aspot_image_mobile');
$aspotText = get_field('aspot_text');
$aspotLink1 = get_field('aspot_link_1');
$aspotLink1Text = get_field('aspot_link_1_text');
$aspotLink2 = get_field('aspot_link_2');
$aspotLink2Text = get_field('aspot_link_2_text');
$aspotHeight = get_field('aspot_height');

?>
<?php
	if(!empty($mobileImg)){
?>
<style>
	@media only screen and (max-width:1024px){
		section.static-aspot{
			background-image:url('<?php echo $mobileImg ?>')!important;
		}
	}
</style>
<?php } ?>


<section class="static-aspot" style="background-image:url('<?php echo $desktopImg ?>'); height: <?php if( $aspotHeight ) { echo $aspotHeight . 'vh'; } else { echo "650px"; } ?>;">
	<div class="container">
		<div class="the-content">
			        <h3 class="withLinks"><?php echo $aspotText ?></h3>

                    <?php if(!empty($aspotLink1Text)){ ?>

                    <div class="the-links">

						<?php if(empty($aspotLink2Text)){ $centerLink1 = 'vertical-center'; } ?>

						<a class="<?php echo $centerLink1 ?>" href="<?php echo $aspotLink1?>"><?php echo $aspotLink1Text ?></a>

						<?php if(!empty($aspotLink2Text)){ ?>

						<a href="<?php echo $aspotLink2 ?>"><?php echo $aspotLink2Text ?></a>

						<?php } ?>

                    </div>

                <?php } ?>
		</div>
	</div>
</section>
