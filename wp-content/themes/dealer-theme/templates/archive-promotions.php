<?php 
  $show_title_bar = get_field('show_title_bar');
  $main_hero_image = get_field('main_hero_image');
  $page_title = get_field('page_title');
  $banner_text = get_field('banner_text');
  if(!$banner_text) {
    $banner_text = "Buy Now & SAVE";
  }
  
  date_default_timezone_set('America/New_York');
  echo mainAspot();
          
?>
<style>
.ribbon-section {
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: -40px;
}

.container-ribbon {
  display: flex;
}

.triangle-top_a {
  width: 0;
  height: 0;
  border-right: 15px solid #FF9309;
  border-top: 15px solid #FF9309;
  border-left: 15px solid transparent;
  border-bottom: 15px solid transparent;
}

.triangle-bottom_a {
  width: 0;
  height: 0;
  border-right: 15px solid #FF9309;
  border-top: 15px solid transparent;
  border-left: 15px solid transparent;
  border-bottom: 15px solid #FF9309;
}

.square_a,
.square_b {
  width: 60px;
  height: 60px;
  background: #FF9309;
}

@media screen and (max-width: 768px) {
  .square_a,
.square_b {
    width: 30px;
    height: 60px;
    background: #FF9309;
  }
}

.title-ribbon {
  width: 450px;
  height: 80px;
  background-color: #FF9309;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 0px 0px 10px 0px black;
  z-index: 10;
}

.title-ribbon h2 {
  color: white;
  font-size: 25px;
  font-weight: bold;
}

.triangle-top_b {
  width: 0;
  height: 0;
  border-right: 15px solid transparent;
  border-top: 15px solid #FF9309;
  border-left: 15px solid #FF9309;
  border-bottom: 15px solid transparent;
}

.triangle-bottom_b {
  width: 0;
  height: 0;
  border-right: 15px solid transparent;
  border-top: 15px solid transparent;
  border-left: 15px solid #FF9309;
  border-bottom: 15px solid #FF9309;
}
.promotions .the-content {
  position: absolute;
  top: 0;
  left: 0;

  padding: 4em;
}
h2.promo-title {
  color: #fff;
  font-size: 20px;
  text-transform: uppercase;
  /*font-size: 60px;*/
}
.promotions p {
  color: #fff;
}
.coupon {
  border: 2.5px dashed #fff;
  height: 77%;
}
.promotions .the-content.coupon {
  margin: 6.5% 9%;
  padding: 5%;
  width: 82%;
}
.promo-item {
  margin-bottom: 2em;
}
p.promo-sub-title {
  color: #fff;
  font-size: 18px;
  font-weight: sans-serif;
}
p.promo-card-title {
  color: #fff;
  font-size: 16px;
  font-weight: sans-serif;
}
.promo-gradient {
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  height: 99%;
  width: 100%;

}
.promo-inner-cont {
  max-width: 1400px;
  margin: 0 auto;
}
.content-inner {
  max-width: 314px;
}
@media (min-width: 768px) {
  p.promo-sub-title {
    font-size: 25px;
  }
  h2.promo-title {
    font-size: 40px;

  }
  .content-inner {
    max-width: 414px;
  }
}
.cta-bubble {
  position: absolute;
  bottom: -20%;
  background-color: #000;
  width: 100%;
}
.cta-bubble p {
  text-align: center;
  display: inline-block;
  width: auto;
  padding: .5em 10px;
}
.promo-item {

  max-width: 700px;
  max-height: 396px;
  padding: 0 !important;
  margin-left: 1em !important;
  margin-right: 1em !important;
  margin-bottom: 100px;
}
.cta-bubble-inner p {
  margin-bottom: 0;
  width: 100%;
  font-family: sans-serif;
  text-transform: uppercase;
}
@media (min-width: 64.063em) {

  .promo-item {
    overflow: hidden;
    width: 47% !important;
    margin-bottom: 2em;
  }
  .cta-bubble {
    position: absolute;
    bottom: -10em;
    right: -2em;
    background-color: #000;
    border-radius: 50%;
    
    z-index: 999;
    width: 300px;
    height: 300px;
  }
  .cta-bubble-inner {
    padding-top: 1em;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
  }
  .cta-bubble-inner span{
    display: block;
  }
  span.cta-top {
    font-size: 23px;
    line-height: 23px;
    font-weight: 500;
    margin-bottom: .25em;
  }
  span.cta-mid {
   font-size: 40px;
   line-height: 40px;
   font-weight: 700; 
   margin-bottom: .25em;
  }
  span.cta-bot {
   font-size: 19px;
   line-height: 19px;
   font-weight: 300;
  }
}



</style>
<div class="" style="position:relative;">
  <div style="background-image: url(<?php echo $main_hero_image['url']; ?>); background-color:#000; height:200px; display:flex; justify-content:center; align-items:center;background-position:center center; background-size:cover;">
    <?php if($page_title): ?>
      <h1 style="color:#fff;"><?php echo $page_title; ?></h1>
    <?php endif; ?>
  </div>
  <div class="ribbon-section">
    <div class="container-ribbon">
        <div class="container-triangle_a">
            <div class="triangle-top_a"></div>
            <div class="triangle-bottom_a"></div>
        </div>
        <div class="square_a"></div>
    </div>
    <div class="title-ribbon"><h2><?php echo $banner_text; ?></h2></div>
    <div class="container-ribbon">
        <div class="square_b"></div>
        <div class="container-triangle_b">
            <div class="triangle-top_b"></div>
            <div class="triangle-bottom_b"></div>
        </div>
    </div>
  </div>
</div>
       <?php if(!empty($show_title_bar)){?>
        <div class="title-bar" style="max-width:960px; margin:0 auto;">
            <h2 class="title page-title" style="margin:0!important; margin-top:.25em!important; float:none; text-align:center;"><?php echo the_title(); ?></h2>

                          <div class="breadcrumbs" style="margin-bottom:.5em;">
                            <div class="row" style="text-align:center;">
                              <?php //BREADCRUMBS
                              if ( function_exists('yoast_breadcrumb') ) {
                                yoast_breadcrumb('','');
                              }
                              ?>
                            </div>
                          </div>

            <div class="clearfix"></div>
        </div>
        <?php } ?>

        <?php
        //GET PROMOTIONS CUSTOM POST TYPE AND DISPLAY CONTENT IF DATES ARE CORRECT
        $today = date("Ymd");

        $promo_tags = get_field("promo_tags");

        $activePromosArgs = array(
          'post_type'             => 'promotion',
          //'post_status'           => 'publish',
          'posts_per_page'        => -1,
          'tag'                   => $promo_tags,
        //  'meta_query'     => array(
        //    array(
        //      'key'     => 'promo_start_date',
        //      'compare' =>  '<=',
        //      'value'   =>  $today
        //    ),
        //    array(
        //      'key'     => 'promo_end_date',
        //      'compare' =>  '>=',
        //      'value'   =>  $today
        //    )
        //  )

        );


$activePromos = new WP_Query( $activePromosArgs );

if($activePromos->have_posts()){
?>
<div class="container promotions">
    <div class="promo-inner-cont flex flex-wrap" style="margin-top:5em;">
    <?php

      while($activePromos->have_posts()){
      $activePromos->the_post();

      $startdate = date('Ymd', strtotime(get_field('promo_start_date')));
      $year = substr($startdate, 0, -4);
      $month = substr($startdate, 4, -2);
      $day = substr($startdate, -2);

      $enddate = date('Ymd', strtotime(get_field('promo_end_date')));

      $today = date(Ymd);

      $stillFresh = 'null';

      if($startdate && $enddate) {

        // Checks freshness of Promotion
        if (($startdate < $today) && $enddate > $today ) {
          $stillFresh = true;
        } else {
          $stillFresh = false;
        }
      }


      $startdate = $month . "/" . $day . "/" . $year;



      $year = substr($enddate, 0, -4);
      $month = substr($enddate, 4, -2);
      $day = substr($enddate, -2);

      $enddate = $month . "/" . $day . "/" . $year;


      $its_just_an_image = get_field('its_just_an_image');
      $is_it_a_coupon = get_field('is_it_a_coupon');
      $coupon_class = '';
      $bubble_color = get_field('bubble_color');
      $color_gradient = get_field('color_gradient');
      $sub_title_card = get_field('sub-title_card');
      $short_description_card = get_field('short_description_card');

      $call_action_bubble = get_field('call_action_bubble');
      $call_to_action_sub_title = get_field('call_to_action_sub_title');
      $call_to_action_text = get_field('call_to_action_text');
      if($color_gradient) {
        $color_gradient = '<div class="promo-gradient" style="background: linear-gradient(to right, ' . $color_gradient . ', transparent); opacity:0.9;"></div>';
      }
      if($is_it_a_coupon) {
        $coupon_class = 'coupon';
      }
      if($stillFresh && $enddate) { ?>


        <div class="promo-item position-relative columns large-6 pb-5 imgp_<?php echo get_field('promo_image_position') ?>">
            <div class="the-image">
              <a href="<?php the_permalink(); ?>">
                  <div class="promo-item-inner position-relative">
                    
                    <?php the_post_thumbnail( 'prom_card' ); ?>
                    <?php echo $color_gradient; ?>

                  </div>
                  <?php if($its_just_an_image !== true): ?>
                  <div class="the-content position-absolute <?php echo $coupon_class; ?>">
                     <div class="content-inner">
                        <h2 class="promo-title"> <?php the_title(); ?> </h2>
                        <?php if($sub_title_card != ''): ?>
                          <p class="promo-sub-title"><?php echo $sub_title_card; ?></p>
                        <?php endif; ?>
                        <?php if($short_description_card != ''): ?>
                          <p class="promo-card-title"><?php echo $short_description_card; ?></p>
                        <?php endif; ?>
                      </div>

                  </div>
                <?php endif; ?>
              </a>
            </div>

            <?php if($call_action_bubble || $call_to_action_sub_title || $call_to_action_text): ?>
              <div class="cta-bubble" style="background: <?php echo $bubble_color; ?>">
                  <div class="cta-bubble-inner">
                    <p>
                    <?php if($call_action_bubble): ?>
                      <span class="cta-top"><?php echo $call_action_bubble; ?></span>
                    <?php endif; ?>
                    <?php if($call_to_action_sub_title): ?>
                      <span class="cta-mid"><?php echo $call_to_action_sub_title; ?></span>
                    <?php endif; ?>
                    <?php if($call_to_action_text): ?>
                      <span class="cta-bot"><?php echo $call_to_action_text; ?></span>
                    <?php endif; ?>
                  </p>
                </div>
              </div>
            <?php endif; ?>
        </div>


  <?php  } ?>


      <?php } ?>
      </div>
    </div>
<?php
}
?>
