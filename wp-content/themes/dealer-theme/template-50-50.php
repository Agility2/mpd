<?php
/**
 * Template Name: 2 Column Page
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
 
  <?php get_template_part('templates/content', '50-50'); ?>
<?php endwhile; ?>
