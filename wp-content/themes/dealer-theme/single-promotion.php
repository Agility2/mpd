<?php 
  $promo_form = get_field('form');
  $short_description_title = get_field('short_description_title');
  $short_description = get_field('short_description');
  $long_description_title = get_field('long_description_title');
  $long_description_content = get_field('long_description_content');
  $img_in_post = get_field('img_in_post');
  $bubble_color = get_field('bubble_color');
  if($bubble_color == '') {
    $bubble_color = blue;
  }

  $its_just_an_image = get_field('its_just_an_image');
  $is_it_a_coupon = get_field('is_it_a_coupon');
  $coupon_class = '';
  $bubble_color = get_field('bubble_color');
  $color_gradient = get_field('color_gradient');
  $sub_title_card = get_field('sub-title_card');
  $short_description_card = get_field('short_description_card');

  $call_action_bubble = get_field('call_action_bubble');
  $call_to_action_sub_title = get_field('call_to_action_sub_title');
  $call_to_action_text = get_field('call_to_action_text');
  if($color_gradient) {
    $color_gradient = '<div class="promo-gradient" style="background: linear-gradient(to right, ' . $color_gradient . ', transparent); opacity:0.9;"></div>';
  }
  if($is_it_a_coupon) {
    $coupon_class = 'coupon';
  }
?>
<style>
  .promo-form.columns {
    background: #fff;
    padding: 5em;
    padding-left: 5em !important;
    padding-right: 5em !important;
    margin-right: 1em;
    box-shadow:  0px 2px 9px rgb(0 0 0 / 20%);
  }
  .promo-form h3 {
    text-align: center;
    font-size: 35px;
  }
  .short-content {
    margin-top: 2em;
    padding: 0 1em;
  }
  .short-content h2 {
    margin-bottom: 1em;
  }
  #gform_108 label {
    font-weight: 900;
    font-size: 22px;
  }
  #gform_108 .gfield_required {
    float: left;
    color: #F64040;
    margin-right: 5px;
  }
  #gform_108 input[type='text'],
  #gform_108 select {
    border-radius: 4px;
    border: 2px solid #D8D8D8;
    font-size: 22px !important;
  }
  #gform_108 select {
    height: 50px;
  }
  #gform_108 h3.gform {
    margin-bottom: 2em;
  }
  #gform_108 input[type='submit'] {
    border: 2px solid #FF9309;
    border-radius: 30px !important;
    background-image: linear-gradient(#FCB800, #FF9309);
    color: #222222 !important;
    font-size: 25px !important;
    padding: .5em 2.25em;
  }

  .single-promo-image {
    border-top:  10px solid #fff;
    border-left:  10px solid #fff;
    border-right:  10px solid #fff;
    position: relative;
    overflow: hidden;
  }
  @media (min-width: 768px) {

    .promo-long-img {
      margin-left: auto;
    }
  }
  @media (min-width: 1100px)  {
    .gf_left_half {
      float: left;
      width: 48%;
    }
    .gf_right_half {
      float: right;
      width: 48%;
    }
    .gf_right_half:after {
      content: '';
      display: table;
      clear: both;
    }
  }
  @media (min-width: 1025px) {
    .promo-form.columns {
      margin-left: auto;
    }
  }
  @media (max-width: 641px) {
    .promo-long-img {
      width: 100%;
    }

  }
  @media (min-width: 960px) {

    .long-inner {
      width: 100%;
      max-width: 750px;
      padding: 0 4em 0 4em;
      
    }
  }
  .promo-long-desc {
    max-width: 1400px;
    margin: 0 auto;
    background: #f1f1f1;
  }
  .promo-long-desc-wrap {
    padding: 10em 0;
  }
  .promo-long-img {
    
    background-position: center center;
    min-height: 50vh;
  }
  .promo-long-content {
    align-items: center;
  }

  .promo-long-content {
   padding: 2em 0; 
  }
  .long-inner {
    width: 100%;

    
  }
  .promo-content {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    padding-top: 2em;
    padding-left: 2em;
  }
  .ribbon-section {
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    bottom: -40px;
  }

  .container-ribbon {
    display: flex;
  }

  .triangle-top_a {
    width: 0;
    height: 0;
    border-right: 15px solid #FF9309;
    border-top: 15px solid #FF9309;
    border-left: 15px solid transparent;
    border-bottom: 15px solid transparent;
  }

  .triangle-bottom_a {
    width: 0;
    height: 0;
    border-right: 15px solid #FF9309;
    border-top: 15px solid transparent;
    border-left: 15px solid transparent;
    border-bottom: 15px solid #FF9309;
  }

  .square_a,
  .square_b {
    width: 60px;
    height: 60px;
    background: #FF9309;
  }

  @media screen and (max-width: 768px) {
    .square_a,
  .square_b {
      width: 30px;
      height: 60px;
      background: #FF9309;
    }
  }

  .title-ribbon {
    width: 450px;
    height: 80px;
    background-color: #FF9309;
    display: flex;
    justify-content: center;
    align-items: center;
    box-shadow: 0px 0px 10px 0px black;
    z-index: 10;
  }

  .title-ribbon h2 {
    color: white;
    font-size: 25px;
    font-weight: bold;
  }

  .triangle-top_b {
    width: 0;
    height: 0;
    border-right: 15px solid transparent;
    border-top: 15px solid #FF9309;
    border-left: 15px solid #FF9309;
    border-bottom: 15px solid transparent;
  }

  .triangle-bottom_b {
    width: 0;
    height: 0;
    border-right: 15px solid transparent;
    border-top: 15px solid transparent;
    border-left: 15px solid #FF9309;
    border-bottom: 15px solid #FF9309;
  }
  .promotions .the-content {
    position: absolute;
    top: 0;
    left: 0;

    padding: 4em;
  }
  h2.promo-title {
    color: #fff;
    font-size: 20px;
    text-transform: uppercase;
    /*font-size: 60px;*/
  }
  .promotions p {
    color: #fff;
  }
  .coupon {
    border: 2.5px dashed #fff;
    height: 77%;
  }
  .promo-content.coupon {
    margin: 6.5% 9%;
    padding: 5%;
    width: 82%;
    margin: 3em auto;
  }
  .promo-item {
    margin-bottom: 2em;
  }
  p.promo-sub-title {
    color: #fff;
    font-size: 18px;
    font-weight: sans-serif;
  }
  p.promo-card-title {
    color: #fff;
    font-size: 16px;
    font-weight: sans-serif;
  }
  .promo-gradient {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    height: 99%;
    width: 100%;

  }
  .promo-inner-cont {
    max-width: 1400px;
    margin: 0 auto;
  }
  .content-inner {
    max-width: 314px;
  }
  @media (min-width: 768px) {
    p.promo-sub-title {
      font-size: 25px;
    }
    h2.promo-title {
      font-size: 40px !important;
      line-height: 42px;

    }
    .content-inner {
      max-width: 414px;
    }
  }
  .cta-bubble {
    position: absolute;
    bottom: -20%;
    background-color: #000;
    width: 100%;
  }
  .cta-bubble p {
    text-align: center;
    display: inline-block;
    width: auto;
    padding: .5em 10px;
  }
  .promo-item {

    max-width: 700px;
    max-height: 396px;
    padding: 0 !important;
    margin-left: 1em !important;
    margin-right: 1em !important;
    margin-bottom: 100px;
  }
  .cta-bubble-inner p {
    margin-bottom: 0;
    width: 100%;
    font-family: sans-serif;
    text-transform: uppercase;
    color: #fff;
  }
  @media (min-width: 64.063em) {

    .promo-item {
      overflow: hidden;
      width: 47% !important;
      margin-bottom: 2em;
    }
    .cta-bubble {
      position: absolute;
      bottom: -10em;
      right: -2em;
      background-color: #000;
      border-radius: 50%;
      
      z-index: 999;
      width: 300px;
      height: 300px;
    }
    .cta-bubble-inner {
      padding-top: 1em;
      display: flex;
      flex-wrap: wrap;
      align-items: center;
    }
    .cta-bubble-inner span{
      display: block;
    }
    span.cta-top {
      font-size: 23px;
      line-height: 23px;
      font-weight: 500;
      margin-bottom: .25em;
    }
    span.cta-mid {
     font-size: 40px;
     line-height: 40px;
     font-weight: 700; 
     margin-bottom: .25em;
    }
    span.cta-bot {
     font-size: 19px;
     line-height: 19px;
     font-weight: 300;
    }
  }

</style>
<div style="position:relative;">
  <div class="promotion flex flex-wrap" style="position:relative;z-index:2;max-width:1400px; padding-top:5em; margin:0 auto;">

    <div class="columns large-6 pb-5">
          <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
            <div class="promo-item-inner position-relative">
              <div class="single-promo-image">
                <img src="<?php echo $url; ?>" alt="<?php the_title(); ?>" />
                <?php echo $color_gradient; ?>
                <?php if($its_just_an_image !== true): ?>
                  <div class="promo-content position-absolute <?php echo $coupon_class; ?>">
                     <div class="content-inner">
                        <h2 class="promo-title"> <?php the_title(); ?> </h2>
                        <?php if($sub_title_card != ''): ?>
                          <p class="promo-sub-title"><?php echo $sub_title_card; ?></p>
                        <?php endif; ?>
                        <?php if($short_description_card != ''): ?>
                          <p class="promo-card-title"><?php echo $short_description_card; ?></p>
                        <?php endif; ?>
                      </div>
                  </div>
                <?php endif; ?>
                <?php if($call_action_bubble || $call_to_action_sub_title || $call_to_action_text): ?>
                  <div class="cta-bubble" style="background: <?php echo $bubble_color; ?>">
                      <div class="cta-bubble-inner">
                        <p>
                        <?php if($call_action_bubble): ?>
                          <span class="cta-top"><?php echo $call_action_bubble; ?></span>
                        <?php endif; ?>
                        <?php if($call_to_action_sub_title): ?>
                          <span class="cta-mid"><?php echo $call_to_action_sub_title; ?></span>
                        <?php endif; ?>
                        <?php if($call_to_action_text): ?>
                          <span class="cta-bot"><?php echo $call_to_action_text; ?></span>
                        <?php endif; ?>
                      </p>
                    </div>
                  </div>
                <?php endif; ?>
              </div>

            </div>

          
          <div class="short-content">
            <?php if($short_description_title != ''): ?>
              <h2 class="short-desc"><?php echo $short_description_title; ?></h2>
            <?php endif; ?>
            <?php if($short_description != ''): ?>
              <p><?php echo $short_description; ?></p>
            <?php endif; ?>
          </div>
    </div>
    <div class="columns large-5 pb-5 promo-form shadow">
      <?php if($promo_form): ?>
        <?php echo do_shortcode($promo_form); ?>
      <?php endif; ?>
    </div>
    <div class="the-content">


        <?php
          $startdate = get_field('promo_start_date');
          $year = substr($startdate, 0, -4);
          $month = substr($startdate, 4, -2);
          $day = substr($startdate, -2);


          $startdate = $month . "/" . $day . "/" . $year;

          $enddate = get_field('promo_end_date');


          $year = substr($enddate, 0, -4);
          $month = substr($enddate, 4, -2);
          $day = substr($enddate, -2);

          $enddate = $month . "/" . $day . "/" . $year;

          $hideDates = get_field('hide_promo_dates');

          if(empty($hideDates)){
        ?>

    <? } ?>
    </div>


  </div>
  <div style="position: absolute; top: 0; left: 0; right: 0; width: 100%;height: 265px;background:<?php echo $bubble_color; ?>; z-index:1;"></div>
</div>
<div class="promo-long-desc-wrap">
  <div class="flex flex-wrap promo-long-desc">
    <div class="columns medium-6 flex flex-wrap promo-long-content">
      <div class="long-inner">
        <?php if($long_description_title != ''): ?>
          <h2><?php echo $long_description_title; ?></h2>
        <?php endif; ?>
        <?php if($long_description_content != ''): ?>
          <p><?php echo $long_description_content; ?></p>
        <?php endif; ?>
      </div>
    </div>
    <?php if($img_in_post): ?>
      <div class="promo-long-img columns medium-6 large-5" style="background-image:url(<?php echo $img_in_post['url']; ?>);background-size:cover;"></div>
    <?php endif; ?>
  </div>
</div>