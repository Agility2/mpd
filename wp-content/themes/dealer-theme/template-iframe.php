<?php
/**
 * Template Name: iFrame Page
 */
?>

<?php while (have_posts()) : the_post(); ?>
  
  <?php get_template_part('templates/content', 'iframe-page'); ?>
<?php endwhile; ?>
