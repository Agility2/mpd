<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;

?>
 <?php header("Access-Control-Allow-Origin: https://staging.myproductdata.com"); ?>
<?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?> >

    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->


    <!-- GOOGLE ANALYTICS TRACKING CODE -->
    <?php $googleAnalytics = get_field('google_analytics_account', 'option'); ?>

    <?php $customEmbedCode = get_field('custom_embed_code', 'option'); ?>

    <script>

      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', '<? echo $googleAnalytics ?>', 'auto');
      ga('send', 'pageview');

    </script>

   <?php if(!empty($customEmbedCode)) { ?>

            <?php echo $customEmbedCode ?>

    <?php } ?>

    <?php
      /* Render Header depending on Header Type choice in Theme General Options Tab */
      $header_type = get_field('header_layout', 'option');
      switch($header_type){
          case 1: do_action('get_header'); get_template_part('templates/header');
              break;
          case 2: do_action('get_header', '2'); get_template_part('templates/header-2');
              break;
          case 3: do_action('get_header', '3'); get_template_part('templates/header-3');
              break;
          case 4: do_action('get_header', '4'); get_template_part('templates/header-4');
              break;
          case 5: do_action('get_header', '5'); get_template_part('templates/header-5');
              break;
          case 6: do_action('get_header', '6'); get_template_part('templates/header-6');
              break;
          case 7: do_action('get_header', '7'); get_template_part('templates/header-7');
              break;
          case 8: do_action('get_header', '8'); get_template_part('templates/header-8');
              break;
          case 10: do_action('get_header', '9'); get_template_part('templates/header-10');
              break;
          case 'custom': do_action('get_header', 'custom'); get_template_part('templates/header-custom');
            break;
          default: do_action('get_header'); get_template_part('templates/header');
      }


      if( !is_front_page() && !is_page_template( 'template-content-block-page.php' ) && (!is_page() && !is_page_template()) && !is_page_template( 'template-directions.php' ) && !is_page_template( 'template-promotions.php' ) && !is_page_template( 'template-gallery.php' ) && !is_page_template( 'template-visuallist.php' ) && !is_page_template( 'template-blog.php' ) && !is_single() && !is_archive() && !is_search() ){
    ?>
      <?php if( (
         (!is_page_template( 'template-collections.php' )) &&
         (!is_page_template( 'template-categories.php' )) &&
         (!is_page_template( 'template-faq.php' )) &&
         (!is_page_template( 'single-reviews.php' )) &&
         (!is_page_template( 'single-360-view.php' )) &&
         (!is_page_template( 'template-50-50.php' )) &&
         (!is_product())
                ) ): ?>
      <div class="wrap container" role="document">
        <?php
          after_header();
        ?>
        <div class="content row">
      <?php endif; ?>

          <?php
          //if there is no sidebar lets make main content full width
          $main_width = ( Config\display_sidebar() == false ? 'large-12 xsmall-12' : 'large-9 large-push-3 xsmall-12' );

          //If double-sidebar template - center main
          if( is_page_template( 'template-double-sidebar.php' ) ){
            $new_column = 'large-6';
            $main_width = 'large-6 large-push-3 xsmall-12';
            $left_column = 'large-3 large-pull-6 medium-6 xsmall-12';
            $right_column = 'large-3 medium-6 xsmall-12';
          }

          //If right-sidebar template - push main over
          elseif( is_page_template( 'template-right-sidebar.php' ) ){
            $main_width = 'large-9 xsmall-12';
            $right_column = 'large-3';
          }

          //If categories template - large 12
          elseif ( (is_page_template('template-categories.php'))  || (is_page_template('single-reviews.php')) || (is_page_template('single-360-view.php')) ) {
            $main_width = 'large-12 xsmall-12';
          }

          else {
            // Default settings
              $main_width = 'large-12 xsmall-12';
            //$left_column = 'large-3 large-pull-9';
            //$right_column = 'large-3';
          }



          ?>
          <?php
            if( !is_product() ){ //this is to remove the main container on the single products
          ?>
          <main class="main <?php echo $main_width; ?> columns <?php echo $new_column; ?>" role="main">
            <?php include Wrapper\template_path();?>
          </main><!-- /.main -->
          <?php
            }else{
              echo '<!--Main Content is hidden through base.php #63-->';
                include Wrapper\template_path();
            }
          ?>

          <?php if (Config\display_sidebar()) : ?>
           <?php // left sidebar ?>
            <?php if( is_page_template( 'template-right-sidebar.php' ) ): ?>
            <aside class="sidebar <?php echo $right_column; ?> columns class-template" role="complementary">
              <?php dynamic_sidebar('right-sidebar'); ?>
            </aside><!-- /.sidebar -->
            <?php else : ?>

            <?php if( !is_product_category() ): ?>
              <aside class="sidebar <?php echo $left_column; ?> columns original" role="complementary">
                <?php include Wrapper\sidebar_path(); ?>
              </aside><!-- /.sidebar -->
            <?php endif; ?>

            <?php endif; ?>
          <?php endif; ?>

          <?php if( is_page_template( 'template-double-sidebar.php' ) ): ?>
            <aside class="sidebar <?php echo $right_column; ?> columns class-template-right" role="complementary">
              <?php dynamic_sidebar('right-sidebar'); ?>
            </aside><!-- /.sidebar -->
          <?php endif; ?>

            <?php if( ( (!is_page_template( 'template-collections.php' )) && (!is_page_template( 'template-categories.php' )) && (!is_page_template( 'single-reviews.php' )) && (!is_page_template( 'single-360-view.php' )) && (!is_page_template( 'single-360-view.php' )) && (!is_page_template( 'template-50-50.php' )) ) ): ?>
        </div><!-- /.content -->
      </div><!-- /.wrap -->
      <?php endif; ?>

    <?php
      }else{
        include Wrapper\template_path();
      }

      $footer_type = get_field('footer_layout', 'option');
      switch($footer_type){
          case 1: do_action('get_footer'); get_template_part('templates/footer');
              break;
          case 2: do_action('get_footer', '2'); get_template_part('templates/footer-2');
              break;
          case 3: do_action('get_footer', '3'); get_template_part('templates/footer-3');
              break;
          case 4: do_action('get_footer', '4'); get_template_part('templates/footer-4');
                break;
          case 5: do_action('get_footer', '5'); get_template_part('templates/footer-5');
                break;
          default: do_action('get_footer'); get_template_part('templates/footer');
      }

      wp_footer();
    ?>

<script>
var $bubbles = jQuery('.bubbles');

function bubbles() {

  // Settings
  var min_bubble_count = 20, // Minimum number of bubbles
      max_bubble_count = 60, // Maximum number of bubbles
      min_bubble_size = 3, // Smallest possible bubble diameter (px)
      max_bubble_size = 12; // Maximum bubble blur amount (px)

  // Calculate a random number of bubbles based on our min/max
  var bubbleCount = min_bubble_count + Math.floor(Math.random() * (max_bubble_count + 1));

  // Create the bubbles
  for (var i = 0; i < bubbleCount; i++) {
    $bubbles.append('<div class="bubble-container"><div class="bubble"></div></div>');
  }

  // Now randomise the various bubble elements
  $bubbles.find('.bubble-container').each(function(){

    // Randomise the bubble positions (0 - 100%)
    var pos_rand = Math.floor(Math.random() * 101);

    // Randomise their size
    var size_rand = min_bubble_size + Math.floor(Math.random() * (max_bubble_size + 1));

    // Randomise the time they start rising (0-15s)
    var delay_rand = Math.floor(Math.random() * 16);

    // Randomise their speed (3-8s)
    var speed_rand = 3 + Math.floor(Math.random() * 9);

    // Random blur
    var blur_rand = Math.floor(Math.random() * 3);

    // Cache the this selector
    var $this = jQuery(this);

    // Apply the new styles
    $this.css({
      'left' : pos_rand + '%',

      '-webkit-animation-duration' : speed_rand + 's',
      '-moz-animation-duration' : speed_rand + 's',
      '-ms-animation-duration' : speed_rand + 's',
      'animation-duration' : speed_rand + 's',

      '-webkit-animation-delay' : delay_rand + 's',
      '-moz-animation-delay' : delay_rand + 's',
      '-ms-animation-delay' : delay_rand + 's',
      'animation-delay' : delay_rand + 's',

      '-webkit-filter' : 'blur(' + blur_rand  + 'px)',
      '-moz-filter' : 'blur(' + blur_rand  + 'px)',
      '-ms-filter' : 'blur(' + blur_rand  + 'px)',
      'filter' : 'blur(' + blur_rand  + 'px)',
    });

    $this.children('.bubble').css({
      'width' : size_rand + 'px',
      'height' : size_rand + 'px'
    });

  });
}

// In case users value their laptop battery life
// Allow them to turn the bubbles off
jQuery('.bubble-toggle').click(function(){
  if($bubbles.is(':empty')) {
    bubbles();
    $bubbles.show();
    jQuery(this).text('Bubbles Off');
  } else {
    $bubbles.fadeOut(function(){
      jQuery(this).empty();
    });
    jQuery(this).text('Bubbles On');
  }

  return false;
});

bubbles();
</script>


<script type="text/javascript" src="https://cdn.rawgit.com/mikeflynn/egg.js/master/egg.min.js"></script>
<script>
const context = new window.AudioContext();

function playFile(filepath) {
  // see https://jakearchibald.com/2016/sounds-fun/
  // Fetch the file
  fetch(filepath)
    // Read it into memory as an arrayBuffer
    .then(response => response.arrayBuffer())
    // Turn it from mp3/aac/whatever into raw audio data
    .then(arrayBuffer => context.decodeAudioData(arrayBuffer))
    .then(audioBuffer => {
      // Now we're ready to play!
      const soundSource = context.createBufferSource();
      soundSource.buffer = audioBuffer;
      soundSource.connect(context.destination);
      soundSource.start();
    });
}
var egg = new Egg("s,o,a,k", function() {
  playFile('https://watkinsdealer.s3-us-west-2.amazonaws.com/easteregg/soak.mp3');
}).listen();

</script>

  </body>
</html>
