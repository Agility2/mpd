<?php
/**
 * Template Name: Finnleo Inner Pages
 */

?>

<div class="anchor-links-bar">
    <div class="anchor-links-wrapper">
				<a class="anchor-link-top smooth-scroll" href="#top">Back To Top</a>
				<a class="anchor-link-top smooth-scroll" href="/request-finnleo-sauna-pricing/">Get Pricing</a>    
				<a class="anchor-link-top smooth-scroll" href="/download-finnleo-sauna-brochure/">Get Brochure</a>
		</div>
</div>

<?php
	$heroImg = get_field('hero_image');
	$heroTitle = get_field('hero_title');
	$heroSubTitle = get_field('hero_sub_title');
	if($heroImg) {
?>
<div class="heroImg"
	style="background:url('<?php echo $heroImg; ?>'); background-repeat:non-repeat; background-size:cover; height:515px; width:100%">
	<?php if($heroTitle != '') { ?>
		<div class="aspot-color-overlay" style="background-color:#000000; opacity:0.4;"></div>
	<div class="dsHeroContainer">
		<h2><?php echo $heroTitle; ?></h2>
		<?php if($heroSubTitle != '') { ?>
		<h3><?php echo $heroSubTitle; ?></h3>
		<?php } ?>
	</div>
	<?php } ?>
</div>
<?php } ?>
<div class="container">
	<div class="row">
		<?php the_field('finnleo_inner_page_content'); ?>
		

	  <?php 
		//? check if this page needs to display products
		if(get_field('show_products')) {

			global $post;
			$pageSlug = $post->post_name;
			if($pageSlug == "classic-saunas-accessories") {
        $pageSlug = "classic-sauna-accessories";
      }
			//? now lets get the products from the term id
			$products = wc_get_products(array(
				'limit' => -1,
				'category' => array($pageSlug),
			 ));

			 //var_dump($products);
			?>
			<div class="dsFinnleoProductGrid">
<?php 
			foreach($products as $product) { //? build the way it should display in the website 
				$productID = $product->get_ID();
			?>
				<div class="dsFinnleoProduct">
				<a href="<?php echo $product->get_permalink(); ?>">
				 <img src="<?php echo get_field('single_product_image', $productID); ?>" />
				 <p class="product-model-slide-title"><?php echo $product->name; ?> </p>
			  </a>
			  </div>	
			<?php } ?>
</div>
<?php }?>
	
	</div>
<style>
.heroImg {
	position:relative;
}
.heroImg .dsHeroContainer h2 {
	font-size: 2.5rem;
	color:white;
	text-transform: uppercase;
	font-family: "Roboto", sans-serif;
}
.heroImg .dsHeroContainer h3 {
	font-size: 1.8rem;
	color:white;
	text-transform: uppercase;
	font-family: "Roboto", sans-serif;
}

.heroImg .dsHeroContainer {
	position:absolute;
	  padding: 1em;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    margin: auto auto;
    text-align: center !important;
    height: 280px;
}
.dsFinnleoProduct {
	padding:0px 1.875em 1.875em 1.875em;
}
.dsFinnleoProduct p {
		font-size: 22px;
    text-transform: capitalize;
		font-weight:bold;
		text-align:center;
		padding-top:15px;
}
.dsFinnleoProduct img {
		padding: 0;
    display: block;
    margin: 0 auto;
    width: 100%;
    margin-bottom: 0px;
		max-width:466px;
}
.dsFinnleoProductGrid {
	display: grid;
  grid-template-columns: auto auto auto;
  padding: 10px;
}
@media screen and (max-width:981px) {
	.dsFinnleoProductGrid {
		grid-template-columns: auto auto;
	}
}

@media screen and (max-width:650px) {
	.dsFinnleoProductGrid {
		grid-template-columns: auto;
	}
}
</style>
</div>