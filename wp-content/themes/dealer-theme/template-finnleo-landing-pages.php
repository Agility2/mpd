<?php
/**
 * Template Name: Finnleo Landing Pages
 */

$this_id        = get_the_ID();
$all_categories = []; // used to show only items from categories that exist in filter
$termID = get_field('main_category');
?>

<?php $hero_back = get_field( 'product_hero_image' ); ?>
<div class="archive-product-hero" style="background-image: url('<?php echo $hero_back; ?>')"></div>


<?php $page_navigation_bar = get_field('page_navigation_bar'); ?>
<div class="archive-product-links">
	<div class="row d-row">
	<?php $page_navigation_bar = array_chunk( $page_navigation_bar, ceil( count( $page_navigation_bar ) / 2 ) ); ?>
		<div class="columns large-4 small-12 small-order-2">
			<?php if ( ! empty( $page_navigation_bar[0] ) ) : ?>
				<div class="archive-product-links-wrapper">
					<?php foreach ( $page_navigation_bar[0] as $link ) : ?>
						<?php
						if($link):
							$link_url = $link['link_url'];
							$link_title  = $link['link_name'];
							$link_target = '_self';
							?>
							<a href="<?php echo esc_url( $link_url ); ?>"
							   target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="columns large-4 small-12 small-order-1 text-center archive-product-links-image">
			<?php if ( get_field( 'center_image' ) ): ?>
				<img src="<?php the_field( 'center_image' ); ?>"/>
			<?php endif; ?>
		</div>
		<div class="columns large-4 small-12 small-order-3">
			<?php if ( ! empty( $page_navigation_bar[1] ) ) : ?>
				<div class="archive-product-links-wrapper">
					<?php foreach ( $page_navigation_bar[1] as $link ) : ?>
						<?php
							if($link):
							$link_url = $link['link_url'];
							$link_title  = $link['link_name'];
							$link_target = '_self';
							?>
							<a href="<?php echo esc_url( $link_url ); ?>"
							   target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>			

<?php 
//* Brand Families
//? 3 columns with flex
?>
<div class="container">
	<div class="row">
<div class="dsBrandFamilies">
 <div class="dsBrandFamily">
	<a href="finnleo-traditional-saunas/">
	 <img src="https://csp-finnleo-saunas.s3.amazonaws.com/hero-blocks/images/finnleo-traditional-saunas.webp" />
	 <h3>Traditional Saunas</h3>
	 <p>The original Finnish sauna with heater & rocks</p>
	</a>
 </div>

 <div class="dsBrandFamily">
	<a href="infrared-saunas/">
	 <img src="https://csp-finnleo-saunas.s3.amazonaws.com/hero-blocks/images/finnleo-infrared-saunas.webp" />
	 <h3>Infrared Saunas</h3>
	 <p>Low EMR/EF IR sauna with patented technology</p>
	</a>
 </div>

 <div class="dsBrandFamily">
	<a href="infrasauna/">
	 <img src="https://csp-finnleo-saunas.s3.amazonaws.com/hero-blocks/images/finnleo-infrasauna.webp" />
	 <h3>InfraSaunas</h3>
	 <p>Hybrid 2-in-1 Traditional & Infrared Sauna</p>
	</a>
 </div>
</div>
</div>
</div>
<style>
	.archive-product-hero {
		height:300px !important;
	}
	.dsBrandFamilies {
		display: grid;
    grid-template-columns: 1fr 1fr 1fr;
		margin-bottom:30px;
		top: -70px;
    position: relative;
		
	}
	@media screen and (max-width:1024px) {
		.dsBrandFamilies {
			display:none;
		}	
	}
	.dsBrandFamilies div {
		padding: 15px;
	}
	.dsBrandFamilies div h3 {
		padding-top:10px;
	}
	.dsBrandFamilies div p {
		color:#000;
	}
</style>


<?php 
//* Product Filters
//? Find Finnleo Categories and get the categories we need for the filters
$mainCategory = get_term_by( 'slug', 'finnleo', 'product_cat' );

//? Filter by Category: Traditional Saunas, Infrared Saunas, Infra Saunas, Heaters & Controls
$filterByCategoryNames = array('Traditional Saunas', 'Infrared Saunas', 'Infra Saunas');
$filterByCategories = createFilterArray($filterByCategoryNames);

//? Filter by Location: Indoor Sauna Rooms, Outdoor Sauna Rooms
$filterByLocationNames = array('Indoor Sauna Rooms', 'Outdoor Sauna Rooms');
$filterByLocations = createFilterArray($filterByLocationNames);

//? Filter by Features: With Steam, Without Steam
$filterByFeaturesNames = array('With Steam', 'Without Steam');
$filterByFeatures = createFilterArray($filterByFeaturesNames);

//? Function to get the term object from woocommerce
function createFilterArray($categoryNames) {
	foreach($categoryNames as $categoryName) {
		$categoryTerm = get_term_by('slug', $categoryName, 'product_cat');
		$filterCategory[] = $categoryTerm;
	}
	return $filterCategory;
}

//? Function to create the filter html sidebar part
function createFilterHTML($filterArray, $filterTitle) {
	if($filterArray){ ?>
					<h4 class="article-title open"><?php echo $filterTitle; ?></h4> 
					<div class="accordion-content content-categories">
					<ul>
			<?php 
							foreach($filterArray as $filterByCategory) {
								if ( !in_array( $filterByCategory->term_id, $all_categories ) ) {
									$all_categories[] = $filterByCategory->term_id;
			
									$categoryName = str_replace('-', ' ', $filterByCategory->name);
									$categoryName = ucwords($categoryName);
								
									if($categoryName == 'Infra Saunas') {
										$categoryName = str_replace(' ', '', $categoryName);
									}

								} ?>
					<li class="">
					<input name="<?php echo $filterByCategory->slug; ?>"
								 type="checkbox"
								 class="dsCBs"
								 value="<?php echo $filterByCategory->term_id; ?>"
								 id="<?php echo $filterByCategory->slug; ?>" >
			
					<label for="<?php echo $filterByCategory->slug; ?>">
								 <?php echo $categoryName; ?>
					</label>
					</li>
	<?php } ?>
	 </ul>
	</div>
  <?php } } ?>

<div class="row">
	<div class="ds-filters-over"></div>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="columns small-12 large-3">
				<div id="ds-filters" class="ds-filters">
					<div class="ds-filters__top show-for-medium-down">
						<h4><?php _e( 'Filter', 'dealer-theme' ); ?></h4>
						<span id="js-toggle-filters"><i class="fa fa-times" aria-hidden="true"></i></span>
					</div>
					<form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="ds-filter">
						<div id="accordion" class="accordion-container">

							<!-- Filter 1 By Category --> 
							<?php createFilterHTML($filterByCategories, 'Category'); ?>

							<!-- Filter 2 By Location --> 
							<?php createFilterHTML($filterByLocations, 'Location'); ?>

							<!-- Filter 3 By Features --> 
							<?php createFilterHTML($filterByFeatures, 'Features'); ?>

							<!-- Can add more filters here in the future? -->

						</div><!--/#accordion-->
						<div class="ds-filters__bottom show-for-medium-down">
							<input id="ds-filters-reset" type='reset' value='Clear' name='reset'>
							<button id="ds-filters-apply" class="apply-button"><?php _e( 'Apply', 'dealer-theme' ); ?></button>
						</div>
					</form>
				</div>
		</div>

		
		<div class="columns small-12 large-9">
			<?php
			$arg = array(
				'post_type'      => 'product',
				'order'          => 'DESC',
				'orderby'        => 'menu_order',
				'posts_per_page' => 30,
				'tax_query'      => array(
					'relation' => 'AND',
        array(
					'taxonomy' => 'product_cat', // The taxonomy name
					'field'    => 'term_id', // Type of field ('term_id', 'slug', 'name' or 'term_taxonomy_id')
					'terms' => $termID,
        ),
        array(
					'taxonomy' => 'product_cat',
					'field' => 'slug',
					'operator' => 'NOT IN',
					'terms' => array('heaters-finnleo-saunas', 'classic-sauna-controls', 'deluxe-sauna-controls', 'heaters-controls'),
				)),
			);

			$the_query = new WP_Query( $arg );
			if ( $the_query->have_posts() ) : ?>
				<div class="row flex-row" id="response" data-counter="<?php echo $the_query->found_posts; ?>"
				     data-categories="<?php echo json_encode( $all_categories ); ?>">
					<div class="columns small-12 ds-filters-nav">
						<div class="ds-filters-nav-right">
							<div class="ds-filters-name hide-for-medium-down">
								<span class="ds-filters-name__value"><?php the_title(); ?></span>
							</div>
							<form id="ds-filters-search-wrap" class="" action="<?php echo esc_url( home_url( '/' ) ); ?>">
								<input type="search" name="ds-search" id="ds-filters-search" class="search__input"
								       placeholder="<?php _e( 'Search', 'dealer-theme' ); ?>"
								       value="<?php echo get_search_query(); ?>"/>
							</form>

							<button id="js-toggle-filters" class="ds-filters-toggle show-for-medium-down">
								<i class="fa fa-filter" aria-hidden="true"></i>
							</button>

							<!--							<select name="sort_by" id="ds-sort_by">-->
							<!--								<option value="" disabled selected>Sort by Price</option>-->
							<!--								<option value="price-asc" --><?php //echo $_POST['sort_by'] == 'price-asc' ? 'selected' : ''; ?><!-- >Low to High</option>-->
							<!--								<option value="price-desc" --><?php //echo $_POST['sort_by'] == 'price-desc' ? 'selected' : ''; ?><!-- >High to Low</option>-->
							<!--							</select>-->
						</div>

					</div>
					<?php while ( $the_query->have_posts() ) :
						$the_query->the_post(); ?>
						<?php $product = wc_get_product( get_the_ID() ); ?>
						<div class="columns small-12 medium-6 large-4">
							<div class="ds-product">
								<a href="<?php echo get_permalink() ?>">
									<?php if ( $product->is_on_sale() ): ?>
										<span class="ds-product__sale">Sale</span>
									<?php endif; ?>
									<span class="ds-product__image"
									      style="background-image: url(<?php echo get_field('single_product_image'); ?>)">
                                        </span>
									<span class="ds-product__title"><?php the_title(); ?></span>
								</a>
							</div>
						</div>
					<?php endwhile; ?>
					<div class="columns small-12 ds-filters-footer-nav">
						<div class="hide-for-medium-down">
							<?php $_POST['posts_per_page'] = '30'; ?>
							<ul id="ds-posts_per_page">
								<li class="<?php echo $_POST['posts_per_page'] == 30 ? 'selected' : ''; ?>">30</li>
								<li class="<?php echo $_POST['posts_per_page'] == 60 ? 'selected' : ''; ?>">60</li>
							</ul>
						</div>
						<div class="js-pagination">
							<?php foundation_pagination( $the_query ); ?>

						</div>
					</div>
				</div>
			<?php endif;
			wp_reset_query(); ?>
		</div>
	<?php endwhile; ?>

	<script>
			;
			(function ($) {
				var $body = $('body');

				$body.on('submit', '#ds-filter, #ds-filters-search-wrap', function () {
					dsFilter();
				});

				$body.on('change', '#ds-filter :checkbox, #ds-filter :radio, #ds-sort_by', function () {
					dsFilter();
				});

				$body.on('click', '#ds-posts_per_page li', function () {
					$('#ds-posts_per_page li').removeClass('selected');
					$(this).addClass('selected');
					dsFilter();
				});

				$body.on('click', '#ds-filters-reset, #ds-filters-apply', function () {
					setTimeout(function () {
						dsFilter();
						$('#ds-filters').fadeOut();
					}, 100);
				});

				$body.on('change', '#ds-filters-paged', function () {
					dsFilter($(this).val());
				});

				$body.on('click', '#js-toggle-filters', function () {
					$('#ds-filters').fadeToggle();
				});

				$body.on('click', '#toTop', function () {
					$("html, body").animate({scrollTop: 0}, 1000);
				});

				$('.article-title').on('click', function () {

					$(this).next().slideToggle(200);

					$(this).toggleClass('open');
				});

				$body.on('click', '.js-pagination a', function () {
					event.preventDefault();
					var urlThis = $(this).attr('href'),
						paged = '';
					if (urlThis.includes('admin-ajax.php')) {
						paged = urlThis.split('?paged=')[1];
					} else {
						var pagedStr = urlThis.split('/page/')[1];
						paged = pagedStr.split('/')[0];
					}
					$("html, body").animate({scrollTop: 800}, 1000);
					dsFilter(paged, '');
				});

				var dsFilter = function (paged) {
					if (event) {
						event.preventDefault();
					}

					var filter = $('#ds-filter');

					// gather categories
					var categoriesCheckboxValue = "";
					$(".content-categories :checkbox").each(function () {
						var ischecked = $(this).is(":checked");
						if (ischecked) {
							categoriesCheckboxValue += $(this).val() + ",";
							console.log(categoriesCheckboxValue += $(this).val());
						}
					});

					if (categoriesCheckboxValue === '') {
						categoriesCheckboxValue = $('#response').data('categories');
						categoriesCheckboxValue = categoriesCheckboxValue + '';
					}

					// gather brands
					var brandsCheckboxValue = "";
					$(".content-brands :checkbox").each(function () {
						var ischecked = $(this).is(":checked");
						if (ischecked) {
							brandsCheckboxValue += $(this).val() + ",";
						}
					});

					// gather product use
					var prUseCheckboxValue = "";
					$(".content-product-use :checkbox").each(function () {
						var ischecked = $(this).is(":checked");
						if (ischecked) {
							prUseCheckboxValue += $(this).val() + ",";
						}
					});

					var sale = $('#sale_sale').is(':checked'),
						orderItem = $('#ds-sort_by'),
						orderBy = 'date',
						order = 'desc';

					if (orderItem.length > 0 && orderItem.val() !== null) {
						var orderStr = orderItem.val();
						orderBy = orderStr.split('-')[0];
						order = orderStr.split('-')[1];
					}

					var data = {
						action: 'ds_filter',
						posts_per_page: $('#ds-posts_per_page li.selected').text(),
						orderby: orderBy,
						order: order,
						categories: categoriesCheckboxValue,
						brands: brandsCheckboxValue,
						product_use: prUseCheckboxValue,
						price_min: $('#price_min').val(),
						price_max: $('#price_max').val(),
						sale: sale,
						this_id: <?php echo $this_id ?>
					}

					if (paged) {
						data.paged = paged;
					}

					var dsFiltersSearch = $('#ds-filters-search');

					if (dsFiltersSearch.val() !== '') {
						data.search = dsFiltersSearch.val();
					}

					$.ajax({
						url: filter.attr('action'),
						data: data,
						type: filter.attr('method'), // POST
						beforeSend: function (xhr) {
							$('.ds-filters-over').addClass('show');
						},
						success: function (data) {
							$('.ds-filters-over').removeClass('show');
							$('#response').html(data); // insert data

							// update value for product counter in filter section
							if ($('.ds-filters-counter.hide-for-medium-down').length) {
								$('.ds-filters-counter.show-for-medium-down').html($('.ds-filters-counter.hide-for-medium-down').html());
							} else {
								$('.ds-filters-counter.show-for-medium-down').html('No products found')
							}
						}
					});
					return false;
				}

				$(".dsCBs").change(function () {
    				$(".dsCBs").not(this).prop('checked', false);
				});

				$(document).on('click', '.single_add_to_cart_button', function (e) {
					e.preventDefault();

					var $thisbutton = $(this),
						id = $thisbutton.val(),
						variation_id = $thisbutton.data('variation_id') || 0;

					var data = {
						action: 'woocommerce_ajax_add_to_cart',
						product_id: id,
						product_sku: '',
						quantity: 1,
						variation_id: variation_id,
					};

					$(document.body).trigger('adding_to_cart', [$thisbutton, data]);
					console.log(data);
					$.ajax({
						type: 'post',
						url: wc_add_to_cart_params.ajax_url,
						data: data,
						beforeSend: function (response) {
							$thisbutton.removeClass('added').addClass('loading');
						},
						complete: function (response) {
							$thisbutton.addClass('added').removeClass('loading');
						},
						success: function (response) {

							if (response.error & response.product_url) {
								window.location = response.product_url;
								return;
							} else {
								$(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);
							}
						},
					});

					return false;
				});

				$(document).on('ready', function () {
					$('.ds-filters .ds-filters-counter__value').html($('#response').data('counter'));
				});

			}(jQuery));
	</script>
</div>
