<?php
/*
 *  Single Reviews
 */
add_filter('show_admin_bar', '__return_false');
?>
<style>
    html { margin-top: 0 !important; }
    .main { padding: 0 !important; }
</style>
<?php
//get image variables
$left_quote = 'https://s3.amazonaws.com/wp-agilitysquared/myproductdata/wp-content/uploads/2015/06/left-quote.png';
$right_quote = 'https://s3.amazonaws.com/wp-agilitysquared/myproductdata/wp-content/uploads/2015/06/right-quote.png';
$quote = get_field('review-quote');
$owner = get_field('review-owner');
$location = get_field('review-location');
$score = get_field('review-score');
$raw_score = get_field('review-score');
$score = round($raw_score);
$score_img = 'https://s3.amazonaws.com/wp-agilitysquared/myproductdata/wp-content/uploads/2015/07/'.$score.'_star_rating.png';
$review_title = get_field('review-title');
?>
    <div class="row">
        <div class="single-review-wrapper">
           <div class="review-header">
               <h1><? echo $review_title; ?></h1>
           </div>
            <div class="left-quote large-2 medium-2 small-1 columns"><img src="<?php echo $left_quote; ?>" style="float:right;"/></div>
            <div class="large-8 medium-8 small-12 columns"><h1 class="entry-title"><?php the_title(); ?></h1></div>
            <div class="right-quote large-2 medium-2 small-1 columns"><img src="<?php echo $right_quote; ?>" style="float:left;" /></div>
            <div class="clearfix"></div>

                <div class="large-12 columns mobile-quotes"><img src="https://s3.amazonaws.com/wp-agilitysquared/myproductdata/wp-content/uploads/2015/05/commas-174x54.png" /></div>

                <div class="large-12 medium-12 small-12 columns">
                    <div class="slider">
                                <div>
                                    <?php
                                    if (!empty($raw_score)){
                                    echo '<img src="'.$score_img.'" style="display:block; margin: 0 auto;" />';
                                    }
                                    echo '<blockquote><p>'.$quote.'</p></blockquote>';
                                    echo '<p class="quote-author">'. $owner .' - '.$location.'</p>';

                                     ?>
                               </div>
                    </div>
              </div>

            <div class="clearfix"></div>
        </div>
    </div>
