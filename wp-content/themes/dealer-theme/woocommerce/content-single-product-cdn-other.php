<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     4.0.0
 *
 * MPD
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
<style>
.single-product .product {
	padding-top:0.2em;
    margin-top: 2em;
}
.single-product .product-title-line {
    background-color: #f4f4f4;
    padding: 30px 0;
}
.single-product .product-title-line .title {
    text-align:center;
}
.single-product .attachment-shop_single.size-shop_single.wp-post-image {
/*        max-width: 430px;*/
    margin: 0 auto;
    outline: solid 1px #eaeaea;
}
.single-product .inner-wrap .product-heading, .product-heading.single-product-heading {
    padding: 0 1em 1em !important;
    margin-bottom: 1em;
}
.single-product header {
    box-shadow: 0px 10px 5px 0px rgba(209,209,209,1);
    z-index: 1;
}
.single-product .woocommerce-variation.single_variation {
    float: left;
    margin-left: 1em;
    margin-right: 1em;
}
.woocommerce div.product span.price, .woocommerce div.product p.price {
    color: black;
    font-size: 2em;
    font-weight: 700;
    font-family: sans-serif;
    line-height: 36px;
}
.woocommerce div.product p.price {
    margin-left: 13px;
}
.woocommerce div.product form.cart .button {
    /*font-family: prelolight;*/
    max-width: 223px;
}
div.woocommerce-variation-price span.price::before {
/*    content: 'Price: ';*/
    display: none;
}
.woocommerce div.product form.cart .variations {
    margin-left: 1em;
}
.woocommerce div.product p.stock, .woocommerce-variation-availability {
    display: none;
}
div.woocommerce-variation-add-to-cart button.single_add_to_cart_button, form.cart button.single_add_to_cart_button {
    width: calc(48% - 1em);
}
.single-product  div.product {
    margin-bottom: 0;
    position: relative;
    max-width: 1400px;
    margin: 1em auto 0;
}
.woocommerce div.product form.cart .variations select {
    max-width: 100%;
    min-width: 73%;
    display: inline-block;
    margin-right: 0;
    max-width: 484px;
}
.woocommerce div.quantity input.qty.text {
    width: 85%;
    margin: 0 1em;
}

    /* Sale Price Color */
 .woocommerce div.product p.price ins, .woocommerce div.product span.price ins {
    color: green !important;
}
 form.cart, form.variations_form.cart {
    padding: 8px 0px;
    margin-bottom: 1em !important;
     max-width: 535px !important;
}

@media only screen and (min-width:525px){
     form.cart, form.variations_form.cart {
         max-width: none !important;
    }
}

div[itemprop="description"] ul {
    margin-left: 0;
}
div[itemprop="description"] ul li {
    list-style: none;
    background-image: url(//watkinsdealer.s3.amazonaws.com/images/sitewide/black-checkbox.png);
    background-repeat: no-repeat;
    background-size: 20px 20px;
    padding-left: 30px;
    padding-bottom: 15px;
    font-size: 20px;
    line-height: 23px;
}
    .woocommerce div.product form.cart .variations label {
    font-weight: 700;
    margin-bottom: 2px;
    text-transform: capitalize;
}

.woocommerce .woocommerce-error .button, .woocommerce .woocommerce-info .button, .woocommerce .woocommerce-message .button {
    float: right;
    }

/*  NEW ERROR MSG STYLES  */
.woocommerce .woocommerce-message::after, .woocommerce .woocommerce-message::before {
    content: none;
    display: table;
}
.woocommerce a.button, .woocommerce button.button, .woocommerce input.button {
    /*background-color: #1e73be;*/
    /*font-family: prelolight;*/
}
.woocommerce .woocommerce-message {
    padding: 1em 2em 1em 3.5em !important;
    margin: 0 0 0em !important;
    position: absolute;
    background-color: rgba(0,0,0,0.88);
    color: white;
    border-top-color: #8fae1b;
    list-style: none !important;
    width: auto;
    word-wrap: break-word;
    z-index: 1;
    height: 105px;
    font-size: 1.5em;
    top: 179px;
    left: 0;
    right: 0;
    line-height: 52px;
}
 .woocommerce .woocommerce-message .button {
    float: right;
     font-size: 16px !important;
}
.close-woocommerce-box {
    height: 15px;
    width: 15px;
    display: inline-block;
    position: absolute;
    top: 0;
    right: 0;
    background-image: url('//watkinsdealer.s3.amazonaws.com/images/sitewide/white-close-window-256.png');
    background-size: cover;
    margin: 5px;
}

.single-product .inner-wrap .product-heading, .product-heading.single-product-heading {
    padding: 0 !important;
}
 form.cart, form.variations_form.cart {
    max-width: none;
}
.summary.entry-summary div[itemprop="description"] {
    max-width: 551px;
    padding: 0 1em;
}
.woocommerce div.aspot-content {
    z-index: 0;
}

/*.prod.btns {
    padding: 0 1em;
}*/
</style>

</div> <!--- close container -->
</div> <!--- close content -->

<!-- ASPOT/HEADER BEGIN -->
<style>
 .single-product header {
        box-shadow: none !important;
    }
</style>
    
    
    <?php //mainAspot(); ?>

    
    <!-- HERO -->
    <?php 
        $productHero = get_field('product_hero');
        $heroBackground = $productHero['background'];
        $heroBGImg = $productHero['background']['image'];
        // $heroBGOverlayColor = $productHero['background']['overlay_color'];
        // $heroBGOverlayOpacity = $productHero['background']['overlay_opacity'];

        $heroTitle = $productHero['title'];
        
        $heroCTA = $productHero['cta'];

    ?>
    <style>
        .dsHeroCDN {
            background:url(<?php echo $heroBGImg; ?>);
            width:100%;
            height:552px;
            background-size:cover;
            position:relative;
        }
        .dsHeroCDN:before {
            content: ' ';
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            background-color: rgba(14, 55, 108, 0.63);
        }
        .dsHeroCDNContent {
            position:absolute;
            left: 50%;
            top: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            text-align:center;
            width: 66% !important;
        }
        .dsHeroCDNContent h3 {
            color:white;
        }
    </style>


<?php  if($heroBGImg ) {?>
    <div class="dsHeroCDN">
            <div class="dsHeroCDNContent">
                <h3 class="aspot-subheading aspot-h3-medium" style="letter-spacing: 10px;"><?php echo $heroTitle; ?></h3>
            </div>
    </div>
<?php } ?>

<!-- ASPOT/HEADER END -->


        <div class="product-heading single-product-heading test-11">
        <div style="clear:both;"></div>

        <div id="container"> <!--- open container -->
        <div id="content" role="main"> <!--- open content -->
        <div class="breadcrumbs" style="text-align:center">
            <div class="row">
            <?php if(function_exists('bcn_display'))
            {
            bcn_display(); ?>
						<br>
		<hr>
						<?php
            }?>
            </div>
        </div>


<div id="product-<?php the_ID(); ?>" <?php post_class('cf'); ?>>

	<?php
		/**
		 * woocommerce_before_single_product_summary hook.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		//do_action( 'woocommerce_before_single_product_summary' );
        $mainImage = get_field('main_product_image');
       
    ?>
    <div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images">
        <img src="<?php echo $mainImage; ?>" />
    </div>


	<div class="summary entry-summary">

       <?php

            $show_pricing = get_field('show_pricing_values');
        ?>

        <?php


       global $product;


        //TOP TITLE AND PRICE
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

        //ADD TO CART
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

        //META
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

        //RE ADD ACTION


         if(empty($showAspot)){
                add_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
            }


        if( $product->is_type( 'simple' ) ){
            ////////////show single price
            add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
            add_action('woocommerce_single_product_summary', 'woocommerce_simple_add_to_cart', 15);

        } elseif( $product->is_type( 'variable' ) ){
            ///////////////show variable box

						if(!empty($show_pricing)){
							add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
	            add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 10 );
						}

                //add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10 );

        }


        ///////////////////////}

        do_action( 'woocommerce_single_product_summary', 20);


            echo '<div class="prod btns test3">';

            $show_question = get_field('show_question');
            $question_text = get_field('question_text');
            $product_title = get_the_title();
            $product_meta = str_ireplace('<r></r>', '' , $product_title);
            $home_url = esc_url( home_url( '/' ) );

            $downloadb = get_field('show_download_brochure_btn');
            $db_text = get_field('download_brochure_text');
            $db_link = get_field('download_brochure_link');
            $brochure_btn_class = get_field('download_brochure_btn_class');
            $inquiry_btn_class = get_field('inquiry_btn_class');

            $inquiryLinkOverride = get_field('inquiry_button_link_override');

            if(empty($db_text)){
                $db_text = 'Download Brochure';
            }

            if (!empty($show_question)) {
                if( empty($question_text) ){
                    $question_text = 'Request More Info';
                }
                if(!empty($show_pricing)){
                    echo  '<div class="orSeparator cf"><span class="orLine"></span><p>or</p><span class="orLine"></span></div>';
                };

                if(empty($inquiryLinkOverride)){
                    echo '<a class="button ask-question test '.$inquiry_btn_class.'" href="'.$home_url.'product-inquiry/?product_name_field='.$product_meta.'">'.$question_text.'</a>';
                }else{
                    echo '<a class="button ask-question test2 '.$inquiry_btn_class.'"  href="'.$inquiryLinkOverride.'">'.$question_text.'</a>';
                }

                if(!empty($downloadb)){
                    echo  '<div class="orSeparator cf"><span class="orLine"></span><p>or</p><span class="orLine"></span></div>';
                    echo '<a class="dwlbrochure button '.$brochure_btn_class.'"  href="'.$db_link.'">'.$db_text.'</a>';
                }
            }


            echo '</div>';


        ?>

	</div><!-- .summary -->


</div>

<style>
div.tab {
    margin:0 !important;
}
.tabs-wrapper p {
    margin: 20px 0px;
}
.dsGridProduct {
 display:grid;
 grid-template-columns:50% 50%;
 grid-gap: 3em;
 padding: 2em;
 max-width: 1400px;
 margin: 0 auto;
}
.imgp_left .dsGridAcc {
    order:2
}
.imgp_left .dsGridAccImg{
    order: 1
}
.imgp_right .dsGridAcc {
    order: 1
}
.imgp_right .dsGridAccImg{
    order: 2
}

@media (max-width:800px){
    .dsGridProduct {
        grid-template-columns:100%;
    }
}

</style>
<ul class="tab-links cf">

<li><a href="javascript:;" class="tabSpecs">SPECIFICATIONS</a></li>
<li><a href="javascript:;" class="tabFeatures">FEATURES</a></li>
<li><a href="javascript:;" class="tabGallery">GALLERY</a></li>
<li><a href="javascript:;" class="tabAccessories">ACCESSORIES</a></li>
        
</ul>
<div class="tabs-wrapper cf" style="margin-bottom:1.5em;">

<div class="tabSpecs tab">
    <div class="container cf" style="padding:1.75em;">
        <?php the_field('spec_table'); ?>
    </div>
</div>


<div class="tabFeatures tab">
    <div class="container cf" style="padding:1.75em;">
        <?php 
            $features = get_field('features_group');
        ?>
            <div class="dsGridProduct">
                <div class="dsGridFeat">
                    <?php echo $features['feat_left']; ?>
                </div>
                <div class="dsGridFeat">
                    <?php echo $features['feat_right']; ?>
                </div>
            </div>

    </div>
</div>



<div class="tabGallery tab">
    <div class="container cf" style="padding:1.75em;">
        <div class="gallery-page-product">
        <?php 
            if( have_rows('gallery_group') ){
                while( have_rows('gallery_group') ){ the_row(); 
                if( have_rows('images') ){
                while( have_rows('images') ){ the_row();   ?>
            
            <div>
                <a href="<?php the_sub_field('image_url'); ?>" target="_blank"><img src="<?php the_sub_field('image_url'); ?>" alt="<?php the_sub_field('image_alt') ?>"></a>
            </div>
               
            <?php } } 
                  } } ?>
        </div>
    </div>
</div>


<div class="tabAccessories tab">
    <div class="container cf" style="padding:1.75em;">
    
<?php 
if( get_field('accessories') ){
 while( the_repeater_field('accessories') ){  ?>
        <div class="dsGridProduct imgp_<?php echo get_sub_field('img_position') ?>">
         
        <div class="dsGridAccImg">
            <img src="<?php the_sub_field('image'); ?>" />
         </div>
        
         <div class="dsGridAcc">
            <?php the_sub_field('content'); ?>
         </div>

         </div>

<?php 
            }  
        }
        
        ?>

       

    </div>

</div>
</div>
        
</div>


        </div>
    </div>


		<!-- Structure Data -->
<?php
global $post;
$terms = get_the_terms( $post->ID, 'product_cat' );
foreach ($terms as $term) {
		$product_cat = $term->name;
		break;
}
		$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
		$sdBrand = $product_cat;
		$sdProductName = get_the_title();
		$sdProductImage = esc_url( $large_image_url[0] );
		$sdProductDescription = strip_tags(get_the_excerpt());
?>
  <script type='application/ld+json'>
    {
        "@context": "https://schema.org/",
        "@type": "Product",
        "name": "<?php echo $sdProductName; ?>",
        "image": [
          "<?php echo $sdProductImage; ?>"
         ],
        "description": "<?php echo $sdProductDescription; ?>",
        "sku": " ",
        "mpn": " ",
        "brand": {
          "@type": "Brand",
          "name": "<?php echo $sdBrand; ?>"
        },
        "review": {
          "@type": "Review",
          "reviewRating": {
            "@type": "Rating",
            "ratingValue": "5",
            "bestRating": "5"
          },
          "author": {
            "@type": "Organization",
            "name": "<?php echo $sdBrand; ?>"
          }
        },
        "aggregateRating": {
          "@type": "AggregateRating",
          "ratingValue": "5",
          "reviewCount": "100"
        },
        "offers": {
          "@type": "AggregateOffer",
          "offerCount": "0",
          "lowPrice": "0",
          "highPrice": "0",
          "priceCurrency": "USD"
        }
      }
     </script>