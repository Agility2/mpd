<?php
/**
 * The template for displaying product content in the single-product-endless.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see       http://docs.woothemes.com/document/template-structure/
 * @author    WooThemes
 * @package   WooCommerce/Templates
 * @version     4.0.0
 *
 * MPD
 */


if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

?>

<style type="text/css">
.endless-container {
  max-width: 1400px;
  margin: 0 auto;
  padding: 0 20px;
}

.endless-industry {
  padding: 0 20px;
  margin-bottom: 100px;
}

.endless-industry h2 {
  color: #ffffff;
  padding: 0 100px 50px 100px;
}

.industry-img {
  height: 490px;
  display: flex;
  align-items: flex-end;
}


@media (max-width:1024px) {
  .endless-industry h2 {
    padding: 0 10px 50px 10px;
  }

}

/* =========================================================================
    
Tabs
    
========================================================================== */
.single-product .prod-description .ui-tabs-nav {
  display: flex;
  max-width: 1400px;
  margin: 0 auto;
}

.prod-description--endless .tab-nav {
  display: flex;
}

.prod-description--endless {
  background-color: #dbdbdb;
}

.prod-description--endless .tab-content {
  color: #000000;
}

.prod-description--endless .tab-content h4.title {
  color: #000000;
}

.single-product .prod-description--endless h2.title {
  color: #000000;
}

.single-product .prod-description--endless .ui-tabs-nav li a {
  border: 4px solid #000000 !important;
  padding: 20px 0;
  color: #000000 !important;
}

@media (max-width:1024px) {
  .single-product .prod-description .ui-tabs-nav {
    flex-wrap: wrap;
  }

}

/* =========================================================================
    
Specs
    
========================================================================== */
ul.prod-specs-list--endless {
  max-width: 680px;
  margin: 0 auto;
}

ul.prod-specs-list--endless li {
  background-color: #006086;
  margin-bottom: 0 !important;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
}

ul.prod-specs-list--endless li a {
  width: 100% !important;
  text-align: center;
}

ul.prod-specs-list--endless li a:before {
  position: absolute;
  left: 20px;
}

ul.prod-specs-list--endless li a {
  color: #ffffff !important;
}

.prod-specs-list--endless td {
  margin: 0;
  margin-left: 20px;
  padding: 15px 30px;
  font-size: 16px;
}

.prod-specs-list--endless .table-key {
  font-weight: bold;
  text-align: right;
  margin-right: 20px;
}

ul.prod-specs-list--endless li a:before {
  color: #ffffff;
}

/* =========================================================================
    
Gallery
    
========================================================================== */
.endless-gallery {
  padding: 100px 0;
}

.endless-gallery h2 {
  text-align: center;
}

.endless-gallery__gallery .slick-next:before {
  content: ">";
  color: #ffffff;
  font-weight: bold;
  font-size: 30px;
  display: inline-block;
  position: absolute;
  left: 0;
  text-indent: 0;
  border-radius: 50%;
  padding: 0.25em 0.7em;
}

.endless-gallery__gallery .slick-slide {
  padding: 0 10px 0 10px;
}

.endless-gallery__gallery button {
  position: absolute;
  height: calc(100% - 8px);
  z-index: 12;
  top: 0;
  margin: 0 !important;
  text-indent: -9999em;
  padding: 0 65px;
}

.endless-gallery__gallery .slick-next {
  right: 0;
  background: linear-gradient(to left, rgba(0, 0, 0, .64) 70%, rgba(0, 0, 1, .1) 100%);
}

.endless-gallery__gallery .slick-prev {
  left: 0;
  background: linear-gradient(to right, rgba(0, 0, 0, .64) 70%, rgba(0, 0, 1, .1) 100%);
}

.endless-gallery__gallery .slick-prev:after {
  content: "<";
  color: #ffffff;
  font-weight: bold;
  font-size: 30px;
  display: inline-block;
  position: absolute;
  right: 0;
  text-indent: 0;
  border-radius: 50%;
  padding: 0.25em 0.7em;
}

.slide-inner {
  height: 100%;
}

.top {
  height: 50%;
}

.lower {
  display: flex;
  height: 50%;
}

</style>


<?php
// Variables
$home_url      = esc_url( home_url( '/' ) );
$product_title = get_the_title();
$product_meta  = str_ireplace('<r></r>', '' , $product_title);
$brands        = array('endless-pools-fitness-systems', 'swimcross-exercise-systems', 'recsport-recreation-systems' );
$current_url   = get_permalink();

// For btns
foreach($brands as $brand){
    if( stripos( $current_url, $brand) !== false ){
         $product_brand = $brand;
    }
}

switch ($product_brand) {
  case 'endless-pools-fitness-systems':
    $btns_link = 'Endless Pools Fitness Systems';
    break;

  case 'swimcross-exercise-systems':
    $btns_link = 'SwimCross Exercise Systems';
    break;

  case 'recsport-recreation-systems':
    $btns_link = 'RecSport Recreation Systems';
    break;
  default:
    # code...
    break;
}
?>

<div class="product-heading"><!-- begin heading -->
    <div class="row">
        <div class="large-11 large-offset-1 medium-12"><!-- title of top section -->
            <div class="product-title-line">
                <h1 class="title hot-tub-title">
                    <?php the_title(); ?>
                </h1>
                <hr class="product-title-line">
            </div>
        </div>
    </div>
</div> <!-- end product heading -->
<div class="the-product cf"><!-- product container -->
     <div class="custom-product-view large-6 medium-12 small-12 xsmall-12  xsmall-12 columns"> <!-- product image -->
      <img class="feat-img product-img" src="<?php echo the_field('product_main_image_endless'); ?>">
    </div> <!-- end product image -->
    <div class="product-description large-6 medium-12 small-12 xsmall-12  xsmall-12 columns">
        <?php the_content(); ?>
        <a href="<?php echo '/download-a-brochure/endless-pools-fitness-systems-brochure/?swimspa=' . $btns_link; ?>">
            <div class="product-price-button">Download Brochure</div>
        </a>
        <a href="<?php echo '/request-hot-tub-pricing/request-price-quote-endless-pools-fitness-systems/?swimspa=' . $btns_link; ?>">
            <div class="product-price-button">Get Pricing</div>
        </a>
    </div>
</div> <!-- end product container -->

<section class="endless-industry endless-container"> <!-- begin endless-industry -->
  <div class="row">
    <div class="small-12">
      <div class="industry-img" style="background: url(<?php the_field('industry_image'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center center;">
      <h2><?php the_field('industry_title'); ?></h2>
    </div>
  </div>
</section> <!-- end endless-industry -->

<section class="features">
  <a id="explore" class="cat-anchor"></a> <!-- ANCHOR LINK -->
  <div class="prod-description prod-description--endless">
      <div class="row">
          <!-- SECTION TITLES -->
          <h2 class="title"><?php the_field("explore-header"); ?></h2>
      </div>
      <div class="tabs">
          <div class="row tab-nav">
              <ul class='product-desc-list large-12 medium-12 small-12 xsmall-12  xsmall-12  columns large-block-grid-<?php echo $large_grid_size;?> medium-block-grid-<?php echo $medium_grid_size;?> small-block-grid-1'>
                  <?php
  $count = 0;
  $posts = get_field('features');
  foreach ($posts as $post){
      setup_postdata($post);
      //Extract Feature Post Variables
      $name = get_field('feature-name');
      $link_name = str_ireplace(' ', '-', $name);
      $link_name = str_ireplace('<r></r>', '', $link_name);
      //$name = str_ireplace('<r></r>', '', $name);
      $tab_index = '';
      //print
      echo '<li tabindex="'.$count.'"><a href="#'.$link_name.'" tabindex="'.$count.'" class="tab-nav-link-'.$count.'  '.$more_than_5.'" >'.$name.'</a></li>';
      $count++;
  }

  wp_reset_postdata();

  echo '</ul>';
  echo '</div><!-- end row tab nav -->';

  $posts = get_field('features');
  foreach ($posts as $post){
      setup_postdata($post);
      //Extract Feature Post Variables
      $name = get_field('feature-name');
      $block_name = str_ireplace(' ', '-', $name);
      $block_name = str_ireplace('<r></r>', '', $block_name);
      //$name = str_ireplace('<r></r>', '', $name);
      $header = get_field('feature-header');
      $body = get_field('feature-body');
      $video = get_field('feature-video');
      $image = get_field('feature-single-image');
      $format = get_field('feature-format');
      $feature_title = get_the_title();
      $feature = get_sub_field('feature');

      $video1 = get_field('feature_video_1');
      $video2 = get_field('feature_video_2');

      /*$header2 = get_field('feature-2-header');
                              $body2 = get_field('feature-2-body');
                              $video7 = get_field('feature-2-video');
                              $image2 = get_field('feature-2-single-image');
                              $format2 = get_field('feature-2-format');*/

      //Display Tab Content
      echo '<div id="'.$block_name.'" class="tab-content">';
      echo '<div class="row">';
      echo '<div class="large-6 medium-12 small-12 xsmall-12  xsmall-12  columns image">';
      //IMAGE ONLY
      if ($format == 'Image') :
      echo '<div class="large-12 xsmall-12  feature-media columns"><p><img src="'.$image.'" /></p></div>';
      //VIDEO ONLY
      elseif($format == 'Video' ) :
      echo '<div class="large-12 xsmall-12  feature-media columns">'.$video.'</div>';
      //IMAGE SLIDER ONLY
      elseif($format == 'Image-Slider') :
      echo '<div class="large-12 xsmall-12  feature-media columns">';
      echo '<div class="slider">';



      // check if the repeater field has rows of data
      if( have_rows('image_slider_cdn') ):
      // loop through the rows of data
      while ( have_rows('image_slider_cdn') ) : the_row();
      $image_slide = get_sub_field('image_url');
      if (!empty($image_slide)) :
      echo '<div class="gallery-side"><img src="'.$image_slide.'" /></div>';
      else :
      endif;
      endwhile;
      
      
      else :
      endif;
      echo '</div><!-- end slider-->';
      echo '</div>';
      //IMAGE AND IMAGE SLIDER
      elseif($format == 'Image & Image-Slider') :
      echo '<div class="large-12 xsmall-12  feature-media columns"><p><img src="'.$image.'" /></p></div>';
      echo '<div class="large-12 xsmall-12  feature-media columns">';
      // check if the repeater field has rows of data
      if( have_rows('image-slider') ):
      echo '<div class="slider2">';
      // loop through the rows of data
      while ( have_rows('image-slider') ) : the_row();
      $image_slide = get_sub_field('image');
      if (!empty($image_slide)) :
      echo '<div class="gallery-side"><img src="'.$image_slide.'" /></div>';
      else :
      endif;
      endwhile;
      echo '</div><!-- end slider-->';
      else :


      endif;
      echo '</div>';
      //IMAGE AND VIDEO
      elseif($format == 'Image & Video') :
      echo '<div class="large-12 xsmall-12  feature-media columns">'.$video.'</div>';
      echo '<div class="large-12 xsmall-12  feature-media columns"><p><img src="'.$image.'" /></p></div>';
      //IMAGE SLIDER AND VIDEO
      elseif($format == 'Image-Slider & Video') :
      echo '<div class="large-12 xsmall-12  feature-media columns">'.$video.'</div>';
      echo '<div class="large-12 xsmall-12  feature-media columns">';
      echo '<div class="slider" id="vidSlide">';
      // check if the repeater field has rows of data
      if( have_rows('image-slider') ):
      // loop through the rows of data
      while ( have_rows('image-slider') ) : the_row();
      $image_slide = get_sub_field('image');
      if (!empty($image_slide)) :
      echo '<div class="gallery-side"><img src="'.$image_slide.'" /></div>';
      else :
      endif;
      endwhile;
      else :
      endif;

       // check if the repeater field has rows of data
       if( have_rows('image_slider_cdn') ):
        // loop through the rows of data
        while ( have_rows('image_slider_cdn') ) : the_row();
        $image_slide = get_sub_field('image_url');
        if (!empty($image_slide)) :
        echo '<div class="gallery-side"><img src="'.$image_slide.'" /></div>';
        else :
        endif;
        endwhile;
        else :
        endif;

      echo '</div><!-- end slider-->';




      echo '</div>';
      //DOUBLE VIDEO
      elseif($format == 'Double Video') :
      echo '<div class="large-12 xsmall-12  feature-media columns">'.$video1.'</div>';
      echo '<div class="large-12 xsmall-12  feature-media columns"><p>'.$video2.'</p></div>';
      endif;
      echo '</div>';
      echo '<div class="large-6 xsmall-12  feature-media columns content">';
      /*echo '<h4 class="title">'.$header.'</h4>';*/
      echo $body;
      echo '</div>';
      echo '<div class="clearfix"></div>';
      echo '</div>';
      echo '</div><!-- /.tab-content-->';



  }

  wp_reset_postdata();

                  ?>
    </div><!-- end tabs -->
  </div><!-- /.prod-description -->
</section> <!-- end features -->


<?php if( have_rows('endless_spec_table') ): ?>

  <?php $count = count(get_field('endless_spec_table')); 

  switch ($count) {
      case '1':
        $columns = "large-12 medium-12 small-12 xsmall-12";
      break;

      case '2':
        $columns = "large-6 medium-12 small-12 xsmall-12";
      break;

      case '3':
        $columns = "large-4 medium-12 small-12 xsmall-12";
      break;
    
    default:
      # code...
      break;
  }
  ?>


    
        <?php if( have_rows('endless_images_gallery') ): ?>

           <div class="endless-gallery"> <!-- begin endless-gallery -->
      <div class="row"> <!-- begin row -->
        <div class="large-12 medium-12 small-12 xsmall-12 columns">
          <h2 class="title">GALLERY</h2>
        </div>
      </div><!--  end row -->

            <div class="endless-gallery__gallery">

            <?php while( have_rows('endless_images_gallery') ): the_row(); 
          
              ?>

               <div class="slide-item">

                  <img src="<?php the_sub_field('image_endless'); ?>">
                
              </div>

            <?php endwhile; ?>

            </div>

            </div> <!-- end endless-gallery --> 

          <?php endif; ?>

  <div class="prod-specs"> <!-- begin prod-specs -->

  <div class="row">

    <h2 class="title large-12 columns"><?php the_title(); ?> Specs</h2>

    <?php while( have_rows('endless_spec_table') ): the_row(); ?> 

      <div class=" spec-details <?php echo $columns; ?> columns">

        <div class="specs-primary tabs"> <!-- begin primary specs -->
        
          <ul class="prod-specs-list prod-specs-list--endless">

            <li><a href="#tabs-1"><?php the_sub_field('endless_table_title'); ?></a></li>

            <div id="tabs-1" class="tab-content main-specs">

              <table class="prod-specs-table">

              <?php while( have_rows('endless_primary_specs') ): the_row(); ?>

                <tr> 
                  <td class="table-key"><?php the_sub_field('endless_primary_name'); ?></td>
                  <td><?php the_sub_field('endless_primary_value'); ?></td>
                </tr>

              <?php endwhile; ?>

            </table>

        </div>

      </ul>

  </div> <!-- end primary specs -->

  <?php if( have_rows('endless_additional_specs') ): ?>

  <div class="specs-additional tabs"> <!-- begin additional specs -->

      <ul class="prod-specs-list prod-specs-list--endless"> 

        <li><a href="#tabs-1">Additional Specs</a></li>

          <div id="tabs-1" class="tab-content main-specs">

             <table class="prod-specs-table">

                <?php while( have_rows('endless_additional_specs') ): the_row(); ?>

                    <tr> 
                      <td class="table-key"><?php the_sub_field('endless_additional_name'); ?></td>
                      <td><?php the_sub_field('endless_additional_value'); ?></td>
                    </tr>

                  <?php endwhile; ?>

            </table>

        </div>

      </ul>
    
  </div> <!-- end additional specs -->

  <?php endif; ?>

</div> <!-- end spec-details -->

  <?php endwhile; ?>

</div> <!-- end row -->

</div> <!-- end prod-specs -->

<?php endif; ?>



<?php
$show_accessory_block = get_field('show-accessories');
$accessory_type       = get_field('accessory_type');
if (!empty($show_accessory_block)) :
echo '<a id="accessories"></a>';
        ?>
        <div class="accessories exclusive-features">
            <div class="row">
                <!-- SECTION TITLES -->
                <h2 class="title"><?php the_field("accessories-header"); ?></h2>
                <?php
/* FEATURE BLOCKS */
    $accessory_brand_slug = $product_brand . '-accessories';
    $prod = get_term_by('slug', $accessory_brand_slug, 'product_cat');
    $brand_accessories_cat_id = $prod->term_id;
//Show Accessory Categories
if($accessory_type == 'cat'){
    $taxonomy     = 'product_cat';
    $parent       = $brand_accessories_cat_id;
    $orderby      = 'name';
    $show_count   = 0;      // 1 for yes, 0 for no
    $pad_counts   = 0;      // 1 for yes, 0 for no
    $hierarchical = 1;      // 1 for yes, 0 for no
    $title        = '';
    $empty        = 0;
    $order        = 'DESC';
    $args = array(
        'taxonomy'     => $taxonomy,
        'parent'       => $parent,
        'orderby'      => $orderby,
        'show_count'   => $show_count,
        'pad_counts'   => $pad_counts,
        'hierarchical' => $hierarchical,
        'title_li'     => $title,
        'hide_empty'   => $empty,
        'order'        => $order

    );

    $sub_accessory_categories = get_categories( $args );
    $num_accessories_cats = count($sub_accessory_categories);
    $accessories_class = '';
    switch($num_accessories_cats){
        case 4:$accessories_class = 'large-3 medium-6 small-12 xsmall-12';
            break;
        case 5:$accessories_class = 'large-4 medium-6 small-12 xsmall-12';
            break;
        case 6:$accessories_class = 'large-4 medium-6 small-12 xsmall-12';
            break;
        case 7:$accessories_class = 'large-3 medium-6 small-12 xsmall-12';
            break;
        case 8:$accessories_class = 'large-3 medium-6 small-12 xsmall-12';
            break;
        default:$accessories_class = 'large-3 medium-6 small-12 xsmall-12';
    }
    foreach ($sub_accessory_categories as $cat) {
        //print_r($cat);
        $brand_accessories_cat_id = '';
        /*switch($product_brand){
            case 'hot-spring':
            $brand_accessories_cat_id = 131;
            break;
            case 'caldera-spas':
            $brand_accessories_cat_id = 130;
            break;
            case 'freeflow-spas':
            $brand_accessories_cat_id = 133;
            break;
            case 'fantasy-spas':
            $brand_accessories_cat_id = 132;
            break;
            default:
        }*/

        /*if($cat->category_parent == $brand_accessories_cat_id) {*/
            //$category_id = $cat->term_id;
            $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
            $image = wp_get_attachment_url( $thumbnail_id );
            $name = strtolower($cat->name);
            $formatted_name = preg_replace('/[ ,]+/', '-', trim($name));
            $formatted_name = str_replace('&', '-', $formatted_name);
            $formatted_name = str_replace('amp;', '-', $formatted_name);
            $formatted_name = str_replace('----', '-', $formatted_name);
            $formatted_product_brand = preg_replace('/[ ,]+/', '-', trim(strtolower($product_brand)));

            if ($formatted_name == 'hot-tub-water-care') {
              if ($formatted_product_brand == 'hot-spring') {
                $formatted_product_brand = "hot-spring-spas";
              }
              $link = '/water-care/'.$formatted_product_brand.'-water-care/';
            } else {
              $link = '/spa-accessories/'.$formatted_product_brand.'-accessories/'.$formatted_product_brand.'-'.$formatted_name;
            }


            echo '<div class="feature-block '.$accessories_class.' columns">';
            // echo '<a href="'. $link .'"><div class="feature-name">'. $cat->name .'</div>';
            if ( $image ) {
                echo '<a href="'. $link .'"><div class="feature-name">'. $cat->name .'</div>';
                echo '<img class="feature-block-img" src="' . $image . '" alt="" /></a>';
            }
            echo '</a>';
            /*echo '<div class="feature-desc">'.$cat->category_description.'</div>';*/
            // echo '<a href="'. $link .'" class="learn-more-link">LEARN MORE</a>';
            echo '</div>';
        /*}*/
    }
}// End Accessory Categories

if($accessory_type == 'product'){
    $acc_products = get_field('acc_products');
    if($acc_products){
        foreach( $acc_products as $post) {
            //Take each product and extract variables
            setup_postdata($post);
            $product_name = get_the_title();
            $product_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');
            $product_link = get_permalink();
            //Print accessory product block
            echo '<div class="feature-block large-3 medium-6 small-12 xsmall-12  columns">';
            echo '<a href="'. $product_link .'"><div class="feature-name">'. $product_name .'</div></a>';
            if ( $product_img[0] ) {
                echo '<div class="block-img-wrap"><a href="'. $product_link .'" class="feature-block-link"><img class="feature-block-img" src="' . $product_img[0] . '" alt="" /></a></div>';
            }
            /*echo '<div class="feature-desc">'.$cat->category_description.'</div>';*/
            echo '<a href="'. $product_link .'" class="learn-more-link">LEARN MORE</a>';
            echo '</div>';

        }
        wp_reset_postdata();
    }
}//End Accessory Products
                ?>
            </div><!-- /. row -->
        </div> <!-- /. exclusive feats -->
        <?php
else :
endif;
?>

<script src="https://masonry.desandro.com/masonry.pkgd.js"></script>

<script type="text/javascript">
  

(function($) {
  $('.slider').resize();
  $('.grid').masonry({
  // options
  itemSelector: 'img',
  columnWidth: 200
});

})(jQuery);

</script>