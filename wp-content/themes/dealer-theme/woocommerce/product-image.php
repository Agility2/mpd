<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.2
 */

defined('ABSPATH') || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if (!function_exists('wc_get_gallery_image_html')) {
    return;
}

global $product;

$post_thumbnail_id = $product->get_image_id();
$custom_image_url = get_field('single_product_image',get_the_ID());
// $image = wp_get_attachment_image($post_thumbnail_id, 'shop_single', true,array( "class" => "attachment-shop_single size-shop_single wp-post-image" ));
$image = '<img width="600" height="587" src="'.$custom_image_url.'" class="attachment-shop_single size-shop_single wp-post-image custom-wp-post-image" alt="">';


$wrapper_classes = apply_filters('woocommerce_single_product_image_gallery_classes', array(
    'wpgs',
    'wpgs--' . (has_post_thumbnail() ? 'with-images' : 'without-images'),
    'images',

));

?>

<div class="<?php echo esc_attr(implode(' ', array_map('sanitize_html_class', $wrapper_classes))); ?>">

		<?php
      
        if (!empty($custom_image_url)) {
        echo '<div class="wpgs-for">';
        
            echo '<div class="woocommerce-product-gallery__image single-product-main-image"><a class="venobox"  title="'.get_the_title().'" data-gall="wpgs-lightbox" href="'.$custom_image_url.'" >' . $image . '</a></div> ';
            
            $gallery_product_images = get_field('gallery_product_images',get_the_ID());
            if ($gallery_product_images) {
                foreach ($gallery_product_images as $image) {
                    echo '<a class="venobox" data-gall="wpgs-lightbox" title="'.get_the_title().'" href="'.$image['image_link'].'" ><img width="600" height="587" src="'.$image['image_link'].'" class="attachment-shop_single size-shop_single wp-post-image custom-wp-post-image" alt=""></a>';

                }
            }
            echo "</div>";
        } else {
            $html = '<div class="woocommerce-product-gallery__image--placeholder">';
            $html .= sprintf('<img src="%s" alt="%s" class="wp-post-image" />', esc_url(wc_placeholder_img_src()), esc_html__('Awaiting product image', 'woocommerce'));
            $html .= '</div>';
        }

        echo apply_filters('woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id);

        do_action( 'woocommerce_product_thumbnails' );
?>
<style>
.custom-attachment-shop_thumbnail{
    width:100px !important;
    height:100px !important;
    object-fit:cover !important;
}
.custom-wp-post-image{
    width: 100% !important;
    height: 600px !important;
    object-fit: cover !important;
}
</style>
</div>

			