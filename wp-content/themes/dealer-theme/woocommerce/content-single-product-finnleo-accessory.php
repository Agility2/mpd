<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see         http://docs.woothemes.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.4.5
 *
 * MPD
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>

<?php
/**
 * woocommerce_before_single_product hook.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
    echo get_the_password_form();
    return;
}
?>
<style>
    .single-product .product-title-line {
        background-color: #f4f4f4;
        padding: 30px 0;
    }
    .single-product .product-title-line .title {
        text-align: center;
    }
    .single-product .attachment-shop_single.size-shop_single.wp-post-image {
        margin: 0 auto;
        outline: solid 1px #eaeaea;
    }
    .single-product .inner-wrap .product-heading, .product-heading.single-product-heading {
        padding: 67px 0 100px !important;
    }
    .single-product-heading #container {
        width: 100%;
        max-width: 1440px;
        margin: 0 auto;
    }
    .single-product header {
        box-shadow: 0px 10px 5px 0px rgba(209,209,209,1);
        z-index: 1;
    }
    .single-product .woocommerce-variation.single_variation {
        float: left;
        margin-left: 1em;
        margin-right: 1em;
    }
    .woocommerce div.product form.cart .button {
        /*font-family: prelolight;*/
        max-width: 223px;
    }
    div.woocommerce-variation-price span.price::before {
        /*    content: 'Price: ';*/
        display: none;
    }
    .woocommerce div.product form.cart .variations {
        margin-left: 1em;
    }
    .woocommerce div.product p.stock, .woocommerce-variation-availability {
        display: none;
    }
    div.woocommerce-variation-add-to-cart button.single_add_to_cart_button, form.cart button.single_add_to_cart_button {
        width: calc(48% - 1em);
    }
    .single-product div.product {
        margin: 0;
        padding: 0;
        position: relative;
        max-width: 1440px;
    }
    .woocommerce div.product form.cart .variations select {
        max-width: 100%;
        min-width: 73%;
        display: inline-block;
        margin-right: 0;
        max-width: 484px;
    }
    .woocommerce div.quantity input.qty.text {
        width: 85%;
        margin: 0 1em;
    }

    /* Sale Price Color */
    .woocommerce div.product p.price ins, .woocommerce div.product span.price ins {
        color: green !important;
    }
    form.cart, form.variations_form.cart {
        padding: 8px 0px;
        margin-bottom: 1em !important;
        max-width: 535px !important;
    }

    @media only screen and (min-width:525px){
        form.cart, form.variations_form.cart {
            max-width: none !important;
        }
    }

    div[itemprop="description"] ul {
        margin-left: 0;
    }
    div[itemprop="description"] ul li {
        list-style: none;
        background-image: url(//watkinsdealer.s3.amazonaws.com/images/sitewide/black-checkbox.png);
        background-repeat: no-repeat;
        background-size: 20px 20px;
        padding-left: 30px;
        padding-bottom: 15px;
        font-size: 20px;
        line-height: 23px;
    }
    .woocommerce div.product form.cart .variations label {
        font-weight: 700;
        margin-bottom: 2px;
        text-transform: capitalize;
    }

    .woocommerce .woocommerce-error .button, .woocommerce .woocommerce-info .button, .woocommerce .woocommerce-message .button {
        float: right;
        width: 50%;
    }

    /*  NEW ERROR MSG STYLES  */
    .woocommerce .woocommerce-message::after, .woocommerce .woocommerce-message::before {
        content: none;
        display: table;
    }
    .woocommerce a.button, .woocommerce button.button, .woocommerce input.button {
        /*background-color: #1e73be;*/
        /*font-family: prelolight;*/
    }
    .woocommerce .woocommerce-message {
        padding: 1em 2em 1em 3.5em !important;
        margin: 0 0 0em !important;
        position: absolute;
        background-color: rgba(0,0,0,0.88);
        color: white;
        border-top-color: #8fae1b;
        list-style: none !important;
        width: auto;
        word-wrap: break-word;
        z-index: 1;
        height: 105px;
        font-size: 1.5em;
        top: 179px;
        left: 0;
        right: 0;
        line-height: 52px;
    }
    .woocommerce .woocommerce-message .button {
        float: right;
        font-size: 16px !important;
    }
    .close-woocommerce-box {
        height: 15px;
        width: 15px;
        display: inline-block;
        position: absolute;
        top: 0;
        right: 0;
        background-image: url('//watkinsdealer.s3.amazonaws.com/images/sitewide/white-close-window-256.png');
        background-size: cover;
        margin: 5px;
    }

    form.cart, form.variations_form.cart {
        max-width: none;
    }
    .summary.entry-summary div[itemprop="description"] {
        max-width: 551px;
        padding: 0 1em;
    }
    .woocommerce div.aspot-content {
        z-index: 0;
    }
    .woocommerce div.product .product_title {
        color: #000000;
        font-size: 40px;
        position: relative;
        padding-bottom: 68px;
    }
    .woocommerce div.product .product_title:before {
        content: '';
        position: absolute;
        height: 3px;
        width: 50px;
        bottom: 25px;
        left: 0;
        background-color: #000000;
        margin-bottom: 0;
    }
    .woocommerce #content div.product div.images, .woocommerce div.product div.images, .woocommerce-page #content div
    .product div.images, .woocommerce-page div.product div.images {
        float: right;
    }
    @media only screen and (max-width: 1024px) {
        .woocommerce #content div.product div.images, .woocommerce div.product div.images, .woocommerce-page #content div
        .product div.images, .woocommerce-page div.product div.images {
            width: 100%;
        }
    }
    .woocommerce #content div.product div.summary, .woocommerce div.product div.summary, .woocommerce-page #content
    div.product div.summary, .woocommerce-page div.product div.summary {
        float: left;
    }
    .wpgs-for .slick-arrow {
        transform: translateY(-50%);
        margin-top: -30px;
    }
    .flaticon-back {
        left: -45px;
    }
    @media only screen and (max-width: 1300px) {
        .flaticon-back {
            left: -25px;
        }
    }
    .flaticon-back:before {
        content: "\276E";
    }
    .flaticon-right-arrow {
        right: -45px;
    }
    @media only screen and (max-width: 1300px) {
        .flaticon-right-arrow {
            right: -25px;
        }
    }
    .flaticon-right-arrow:before {
        content: '\276F';
    }
    [class^="flaticon-"]:before, [class*=" flaticon-"]:before, [class^="flaticon-"]:after, [class*=" flaticon-"]:after {
        font-size: 40px;
    }
    .woocommerce-product-details__short-description h4 {
        font-size: 24px;
        color: #000000;
    }
    .woocommerce-product-details__short-description ul {
        margin-left: 2rem;
        width: 100%;
        max-width: 586px;
    }
    .woocommerce-product-details__short-description ul li {
        color: #4B4B4B;
        font-size: 20px;
    }
    .gen-content-tab .uniq-wrap-content {
        width: 100%;
        max-width: 280px;
        margin-left: auto;
    }
    @media only screen and (max-width: 768px) {
        .gen-content-tab .uniq-wrap-content {
            margin-left: 0;
        }
    }
    .gen-content-tab .uniq-wrap-content h4 {
        font-size: 40px;
        padding-bottom: 45px;
        position: relative;
    }
    .gen-content-tab .uniq-wrap-content h4:before {
        content: '';
        position: absolute;
        height: 3px;
        width: 50px;
        top: 76px;
        left: 0;
        background-color: #000000;
        margin-bottom: 0;
    }
    .gen-content-tab .uniq-wrap-content p {
        font-size: 24px;
        margin-bottom: 20px;
    }
    .gen-content-tab .uniq-wrap-content ul {
        margin-left: 35px;
    }
    @media only screen and (max-width: 1024px) {
        .gen-content-tab .uniq-wrap-content ul {
            margin-left: 0;
        }
    }
    .gen-content-tab .uniq-wrap-content ul li {
        font-size: 20px;
        margin-bottom: 15px;
    }
    .gen-content-tab .uniq-wrap-content .specs-uniq-btn {
        background-color: transparent;
        color: #406418 !important;
        text-align: center;
        display: inline-block !important;
        width: 100%;
        max-width: 220px;
        padding: 12px;
        border-radius: 5px !important;
        text-transform: none;
        border: 2px solid #406418;
        transition: all 0.3s;
    }
    .gen-content-tab .uniq-wrap-content .specs-uniq-btn:hover {
        background-color: #406418;
        color: #fff !important;
    }
    @media only screen and (min-width: 1024px) {
        .single-product #content {
            padding: 0 50px 0 15px;
        }
    }
    @media only screen and (max-width: 1024px) {
        .single-product #content {
            padding: 0 30px;
        }
    }
</style>

</div> <!--- close container -->
</div> <!--- close content -->

<!-- ASPOT/HEADER BEGIN -->
<style>
    .single-product header {
        box-shadow: none !important;
    }
</style>
<?php mainAspot(); ?>
<!-- ASPOT/HEADER END -->


<div class="product-heading single-product-heading test-11">
    <div style="clear:both;"></div>

    <div id="container"> <!--- open container -->
        <div id="content" role="main"> <!--- open content -->
            <div class="breadcrumbs">
                <div class="row">
                    <div class="breadcrumbs__wrapper">
                        <?php if(function_exists('bcn_display')) { bcn_display(); } ?>
                    </div>
                    <style>
                        @media only screen and (min-width: 1024px) {
                            .breadcrumbs__wrapper {
                                margin-left: 52%;
                            }
                        }
                        .breadcrumbs__wrapper span {
                            font-size: 14px;
                            text-transform: uppercase;
                            color: #4B4B4B;
                        }
                        .breadcrumbs__wrapper span:hover {
                            color: #406418;
                        }
                    </style>
                </div>
            </div>


            <div id="product-<?php the_ID(); ?>" <?php post_class('cf'); ?>>

                <div class="summary entry-summary test">

                    <?php

                    $show_pricing = get_field('show_pricing_values');
                    ?>

                    <?php


                    global $product;


                    //TOP TITLE AND PRICE
                    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
                    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

                    //ADD TO CART
                    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

                    //META
                    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

                    //RE ADD ACTION


                    if(empty($showAspot)){
                        add_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
                    }
                 

                    if( $product->is_type( 'simple' ) ){
                        ////////////show single price
                        add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 20);
                        add_action('woocommerce_single_product_summary', 'woocommerce_simple_add_to_cart', 20);

                    } elseif( $product->is_type( 'variable' ) ){
                        ///////////////show variable box

                        if(!empty($show_pricing)){
                            add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 20);
                            add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 20 );
                        }

                        //add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10 );

                    }
                    do_action( 'woocommerce_single_product_summary', 10);

                    echo '<div class="prod btns test3">';

                    $show_question = get_field('show_question');
                    $question_text = get_field('question_text');
                    $product_title = get_the_title();
                    $product_meta = str_ireplace('<r></r>', '' , $product_title);
                    $home_url = esc_url( home_url( '/' ) );

                    $downloadb = get_field('show_download_brochure_btn');
                    $db_text = get_field('download_brochure_text');
                    $db_link = get_field('download_brochure_link');
                    $brochure_btn_class = get_field('download_brochure_btn_class');
                    $inquiry_btn_class = get_field('inquiry_btn_class');

                    $inquiryLinkOverride = get_field('inquiry_button_link_override');

                    if(empty($db_text)){
                        $db_text = 'Download Brochure';
                    }

                    if (!empty($show_question)) {
                        if( empty($question_text) ){
                            $question_text = 'Request More Info';
                        }

                        if(empty($inquiryLinkOverride)){
                            echo '<a target="_blank" class="button ask-question test '.$inquiry_btn_class.'" href="'.$home_url.'product-inquiry/?product_name_field='.$product_meta.'">'.$question_text.'</a>';
                        }else{
                            echo '<a target="_blank" class="button ask-question test2 '.$inquiry_btn_class.'"  href="'.$inquiryLinkOverride.'">'.$question_text.'</a>';
                        }

                        if(!empty($downloadb)){
                            echo '<a target="_blank" class="button dwlbrochure'.$brochure_btn_class.'"  href="'.$db_link.'">'.$db_text.'</a>';
                        }
                    }


                    echo '</div>';


                    ?>

                    <style>
                        .prod.btns {
                            padding: 0;
                            display: flex;
                            margin-top: 40px;
                        }
                        .prod.btns .button.ask-question {
                            background-color: #406418;
                            color: #fff !important;
                            text-align: center;
                            display: inline-block !important;
                            margin-right: 24px !important;
                            width: 100%;
                            max-width: 220px;
                            padding: 12px;
                            border-radius: 5px !important;
                            text-transform: none;
                            border: 2px solid #406418;
                        }
                        .prod.btns .button.ask-question:hover {
                            background-color: transparent !important;
                            color: #406418 !important;
                        }
                        .prod.btns .button.dwlbrochure {
                            background-color: transparent;
                            color: #406418 !important;
                            text-align: center;
                            display: inline-block !important;
                            width: 100%;
                            max-width: 220px;
                            padding: 12px;
                            border-radius: 5px !important;
                            text-transform: none;
                            border: 2px solid #406418;
                            margin-bottom: 0 !important;
                        }
                        @media only screen and (max-width: 640px) {
                            .prod.btns .button.dwlbrochure {
                                padding: 10px;
                            }
                            .prod.btns .button.ask-question {
                                padding: 10px;
                            }
                        }
                        .prod.btns .button.dwlbrochure:hover {
                            background-color: #406418;
                            color: #fff !important;
                        }

                    </style>

                </div><!-- .summary -->

	            <?php
	            /**
	             * woocommerce_before_single_product_summary hook.
	             *
	             * @hooked woocommerce_show_product_sale_flash - 10
	             * @hooked woocommerce_show_product_images - 20
	             */
	            //do_action( 'woocommerce_before_single_product_summary' );
	            ?>
                <div class="images">
                        <!-- IMAGE OR GALLERY HERE -->
                        <?php 
                           $cdnIMG = get_field('single_product_image');
                           $sliderImgs = get_field('gallery_product_images');
                         if(!$sliderImgs) {
                            
                            if($cdnIMG) { //check if url image exists ?>
                            <img src="<?php echo $cdnIMG; ?>" alt="Product Name"/>
                        <?php
                            } else { //display default img

                            }                            
                        } else { // display slider gallery ?>
                        <div class="dsproduct-Gallery">
                        <div class="slider-for">
                        <div class="dsproduct-Img">
                        <img src="<?php echo $cdnIMG; ?>" alt="Product Name"/>
                        </div>
                        <?php foreach($sliderImgs as $slideImg) { ?>
                            <div class="dsproduct-Img"> 
                            <img src="<?php echo $slideImg['image_link']; ?>" alt="Product Name"/>
                            </div>
                        <?php } ?>
                        </div>


                        <div class="slider-nav">
                        <div>
                        <img src="<?php echo $cdnIMG; ?>" alt="Product Name"/>
                        </div>    
                        <?php foreach($sliderImgs as $slideImg) { ?>
                            <div> 
                            <img src="<?php echo $slideImg['image_link']; ?>" alt="Product Name"/>
                            </div>
                        <?php } ?>
                        </div>

                        <i class="fas fa-chevron-left" aria-disabled="false"></i>
                        <i class="fas fa-chevron-right" aria-disabled="false"></i>
                        </div>
                        <style>

                            .dsproduct-Img img {
                                padding:0 50px;
                                text-align:center;
                                width: 100% !important;
                                height: 600px !important;
                                object-fit: cover !important;
                            }
                       
                            .dsproduct-Gallery {
                                position:relative;
                            }
                            .fa-chevron-left {
                                position: absolute;
                                top: 30%;
                                font-size: 55px;
                                left:0px;
                                color:#406418;
                                cursor:pointer;
                            }
                            .fa-chevron-right {
                                position: absolute;
                                top: 30%;
                                font-size: 55px;
                                right:0px;
                                color:#406418;
                                cursor:pointer;
                            }
                            .slider-nav div img {
                               padding:5px; 
                            }

                            @media (max-width:800px) {
                                .dsproduct-Img img {
                                    object-fit:initial !important;
                                    height:100% !important;
                                }
                                .fa-chevron-right, .fa-chevron-left {
                                    font-size:35px;
                                } 
                            }
                        </style>
                        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
                        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css" crossorigin="anonymous" /> <script>
                         jQuery('.slider-for').slick({
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: true,
                            fade: true,
                            adaptiveHeight: true,
                            prevArrow: jQuery('.fa-chevron-right'),
                            nextArrow: jQuery('.fa-chevron-left'),
                            asNavFor: '.slider-nav'
                         });
                         jQuery('.slider-nav').slick({
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            asNavFor: '.slider-for',
                            arrows: false,
                            dots: false,
                            centerMode: true,
                            focusOnSelect: true
                         });
                        </script>
                        <?php } ?>
                </div>

            </div>


            <?php
            $showspecs = get_field('general_show_product_specs_tab');
            $specstabtitle = get_field('product_specs_tab_title');
            if( empty($specstabtitle) ){
                $specstabtitle = 'Product Specs';
            }
            $showswatches = get_field('general_show_swatches_tab');
            $swatchestabtitle = get_field('swatches_tab_title');
            if( empty($swatchestabtitle) ){
                $swatchestabtitle = 'Product Swatches';
            }
            $showcustom = get_field('show_gener_content_tab');
            $customtabtitle = get_field('gen_content_tab_title');
            if( empty($customtabtitle) ){
                $customtabtitle = 'More';
            }
            $showmedia = get_field('show_media_library');
            $mediatabtitle = get_field('media_tab_title');
            if( empty($mediatabtitle) ){
                $mediatabtitle = 'Media Gallery';
            }
            $showvl = get_field('show_visual_list_tab');
            $vltabtitle = get_field('visual_list_tab_title');
            if( empty($vltabtitle) ){
                $vltabtitle = 'More Details';
            }
            $showgl = get_field('show_grid_tab');
            $gltabtitle = get_field('grid_section_title');
            if( empty($gltabtitle) ){
                $gltabtitle = 'More Details';
            }
            $theDesc = get_the_content();
            $showProdDesc = get_field('general_show_description_tab');

            $showRelatedProducts = get_field('general_show_related_products_tab');
            $showCrossSellProducts = get_field('general_show_crosssell_products_tab');

            $descriptionTabTitle = get_field('description_tab_title');
            $descriptionTabContentHeader = get_field('description_tab_content_header');

            $showMoreLikeThis = get_field('general_show_more_like_this_products_tab');

            ?>

            <?php if(!empty($showProdDesc) || !empty($showspecs) || !empty($showswatches) || !empty($showcustom) || empty($showmedia) || !empty($showvl) || !empty($showgl) || !empty($showMoreLikeThis)) { ?>
                <ul class="tab-links cf">
                    <?php if(!empty($showProdDesc)){ ?>
                        <?php if(!empty($theDesc)){ ?>
                            <li><a href="javascript:;" class="theDescription"><? echo $descriptionTabTitle; ?></a></li>
                        <?php } ?>
                    <?php } ?>
                    <?php if(!empty($showspecs)){ ?>
                        <li><a class="<?php echo str_replace(' ', '_', $specstabtitle); ?>" href="javascript:;"><?php echo $specstabtitle ?></a></li>
                    <?php } ?>
                    <?php if(!empty($showswatches)){ ?>
                        <li><a class="<?php echo str_replace(' ', '_', $swatchestabtitle); ?>" href="javascript:;"><?php echo $swatchestabtitle ?></a></li>
                    <?php } ?>
                    <?php if(!empty($showcustom)){ ?>
                        <li><a class="<?php echo str_replace(' ', '_', $customtabtitle); ?>" href="javascript:;"><?php echo $customtabtitle ?></a></li>
                    <?php } ?>
                    <?php if(!empty($showmedia)){ ?>
                        <li><a class="<?php echo str_replace(' ', '_', $mediatabtitle); ?>" href="javascript:;"><?php echo $mediatabtitle ?></a></li>
                    <?php } ?>
                    <?php if(!empty($showvl)){ ?>
                        <li><a class="<?php echo str_replace(' ', '_', $vltabtitle); ?>" href="javascript:;"><?php echo $vltabtitle ?></a></li>
                    <?php } ?>
                    <?php if(!empty($showgl)){ ?>
                        <li><a class="<?php echo str_replace(' ', '_', $gltabtitle); ?>" href="javascript:;"><?php echo $gltabtitle ?></a></li>
                    <?php } ?>
                    <?php if(!empty($showRelatedProducts)){ ?>
                        <li><a href="javascript:;" class="RelatedProducts">Related Products</a></li>
                    <?php } ?>
                    <?php if(!empty($showCrossSellProducts)){ ?>
                        <li><a href="javascript:;" class="CrossSellProducts"><?php echo get_field('cross_sell_tab_name'); ?></a></li>
                    <?php } ?>

                    <?php if(!empty($showMoreLikeThis)){ ?>
                        <li><a href="javascript:;" class="ShowMoreLikeThis"><?php echo get_field('more_like_this_tab_name'); ?></a></li>
                    <?php } ?>
                </ul>

            <?php } ?>


            <?php if(!empty($showspecs) || !empty($showswatches) || !empty($showcustom) || !empty($showmedia) || !empty($showvl) || !empty($showgl) || !empty($showMoreLikeThis)){ ?>
                <div class="tabs-wrapper cf" style="margin-bottom:1.5em;">

                    <?php if(!empty($showProdDesc)){ ?>
                        <?php if(!empty($theDesc)){ ?>
                            <div class="theDescription tab">
                                <section class="productDescription">
                                    <div class="container cf" style="padding-top:2.75em;">
                                        <h3><?php
                                            if(empty($descriptionTabContentHeader)) {
                                                echo the_title();
                                            } else {
                                                echo $descriptionTabContentHeader;
                                            } ?></h3>
                                        <?php echo $theDesc ?>
                                    </div>
                                </section>
                            </div>
                        <?php } ?>
                    <?php } ?>



                    <?php if(!empty($showCrossSellProducts)){ ?>
                        <div class="CrossSellProducts tab">
                            <section class="CrossSellProducts">
                                <div class="container cf">
                                    <?php
                                    // Remove standard Related Products section
                                    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
                                    // Remove the WooCommerce Upsell hook
                                    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
                                    remove_action( 'woocommerce_after_single_product_summary', 'woo_wc_upsell_display', 15 );
                                    // Add a custom action to display Upsells
                                    add_action( 'mpd_after_single_product_summary', 'woocommerce_upsell_display', 15 );
                                    do_action('mpd_after_single_product_summary');
                                    ?>

                                </div>
                            </section>
                        </div>
                    <?php } ?>

                    <?php if(!empty($showMoreLikeThis)){ ?>
                        <div class="ShowMoreLikeThis tab">
                            <section class="ShowMoreLikeThis">
                                <div class="container cf">
                                    <?php $terms = wp_get_post_terms( $post->ID, 'product_cat' ); ?>
                                    <?php foreach ( $terms as $term ) {
                                        $parent_array[] = $term->parent;
                                        $cats_array[] = $term->term_id;
                                    } ?>
                                    <?php $filtered_cats_array = array_diff($cats_array, $parent_array); ?>
                                    <?php $query_args = array( 'post__not_in' => array( $post->ID ), 'posts_per_page' => -1, 'post_type' => 'product', 'tax_query' => array(
                                        array(
                                            'taxonomy' => 'product_cat',
                                            'field' => 'id',
                                            'terms' => $filtered_cats_array
                                        ))); ?>

                                    <?php $myQuery = new WP_Query( $query_args ); ?>

                                    <?php if ( $myQuery->have_posts() ) { ?>
                                        <?php while ( $myQuery->have_posts() ) { ?>
                                            <?php $myQuery->the_post(); ?>
                                            <div class="col-4">
                                                <a class="ShowMoreLikeThis__link" href="<?php echo get_the_permalink(); ?>">
                                                    <img class="ShowMoreLikeThis__image" src="<?php echo get_field('single_product_image'); ?>">
                                                    <p class="ShowMoreLikeThis__title"><?php echo get_the_title();?></p>
                                                </a>
                                            </div>
                                            <style>
                                                .ShowMoreLikeThis a {
                                                    transition: all 0.3s;
                                                }

                                                .ShowMoreLikeThis a:hover p {
                                                    color: #406418;
                                                }

                                                .ShowMoreLikeThis p {
                                                    font-size: 20px;
                                                    color: #333;
                                                    font-weight: 600;
                                                    text-align: center;
                                                    margin-top: 10px;
                                                }
                                            </style>
                                        <?php }
                                    }
                                    wp_reset_postdata(); ?>
                                </div>
                            </section>
                        </div>
                    <?php } ?>

                    <?php if(!empty($showRelatedProducts)){ ?>
                        <div class="RelatedProducts tab">
                            <section class="RelatedProducts">
                                <div class="container cf">
                                    <?php
                                    // Remove standard Related Products section
                                    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
                                    // Remove the WooCommerce Upsell hook
                                    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
                                    remove_action( 'woocommerce_after_single_product_summary', 'woo_wc_upsell_display', 15 );
                                    // Add a custom action to display Upsells
                                    add_action( 'mpd1_after_single_product_summary', 'woocommerce_output_related_products', 20 );
                                    do_action('mpd1_after_single_product_summary');
                                    ?>
                                </div>
                            </section>
                        </div>
                    <?php } ?>

                    <?php
                    $specstitle = get_field('general_product_specs_section_title');
                    if( empty($specstitle) ){
                        $specstitle = 'Specifications';
                    }
                    $heightspec = get_field('general_product_height');
                    $widthspec = get_field('general_product_spec_width');
                    $lengthspec = get_field('general_product_spec_length');
                    $specimg = get_field('general_product_specs_image');
                    ?>

                    <?php if(!empty($showspecs)){ ?>

                        <div class="<?php echo str_replace(' ', '_', $specstabtitle); ?> tab">
                            <section class="gen specs" style="margin-bottom: 50px;">
                                <div class="container cf">
                                    <h3 style="margin-bottom:1em;"><?php echo $specstitle ?></h3>

                                    <div class="row"> <!-- open row -->

                                        <?php if( !empty($specimg) ){ ?>
                                        <div class="large-6 medium-6 small-12 xsmall-12 columns">
                                            <table class="thespecslist">
                                                <?php } else{ ?>
                                                <div class="large-6 medium-6 small-12 xsmall-12 columns">
                                                    <table class="thespecslist" style="float: none; margin: 0 auto;">
                                                        <?php } ?>

                                                        <tr>
                                                            <?php if(!empty($lengthspec)){ ?>
                                                        <tr>
                                                            <td>Length:</td>
                                                            <td><?php echo $lengthspec ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                        <?php if(!empty($widthspec)){ ?>
                                                            <tr>
                                                                <td>Width:</td>
                                                                <td><?php echo $widthspec ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                        <?php if(!empty($heightspec)){ ?>
                                                            <tr>
                                                                <td>Height:</td>
                                                                <td><?php echo $heightspec ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                        <?php
                                                        if( get_field('general_primary_product_spec_fields') ){

                                                            ?>
                                                            <tr>
                                                            <tr style="border-bottom: solid 1px #ccc;">

                                                            <th colspan="2" style="text-align:center;padding: 6px;
                                font-size: 1.2em;width: 100%;" class="specslist-heading">Primary Specs</th>

                                                            <?php


                                                            while( the_repeater_field('general_primary_product_spec_fields') ){
                                                                ?>


                                                                </tr>
                                                                <tr>
                                                                    <td><?php echo get_sub_field('label') ?></td>
                                                                    <td><?php echo get_sub_field('value') ?></td>
                                                                </tr>
                                                                </tr>
                                                                <?php
                                                            }//end while general_primary_product_spec_fields
                                                        }//end if general_primary_product_spec_fields
                                                        ?>
                                                        </tr>


                                                        <tr>
                                                            <?php
                                                            if( get_field('additional_product_spec_fields') ){ ?>
                                                        <tr class="cf">
                                                        <tr style="border-bottom: solid 1px #ccc;">

                                                            <th colspan="2" style="text-align:center;" class="specslist-heading" style="width: 100%;">Additional Specs</th></tr>
                                                    <?php
                                                    while( the_repeater_field('additional_product_spec_fields') ){
                                                        ?>

                                                        <td><?php echo get_sub_field('label') ?></td>
                                                        <td><?php echo get_sub_field('value') ?></td>
                                                        </tr>
                                                        <?php
                                                    }//end while general_primary_product_spec_fields
                                                    }//end if general_primary_product_spec_fields
                                                    ?>
                                                        </tr>
                                                </div>
                                            </table>
                                        </div>



                                        <?php
                                        if( !empty($specimg) ){
                                            ?>

                                            <div class="large-6 medium-6 small-12 xsmall-12 columns">
                                                <div class="img-container">
                                                    <img src="<?php echo $specimg; ?>" alt="">
                                                    <?php if( !empty($heightspec) && !empty($widthspec)) { ?>
                                                        <div class="height"><p><?php echo $heightspec; ?></p></div>
                                                        <div class="width"><p><?php echo $widthspec; ?></p></div>
                                                    <?php }; ?>
                                                </div>
                                            </div>



                                        <?php }; ?>

                                    </div> <!-- close row -->

                                </div> <!-- close container -->

                            </section>
                        </div> <!-- close tab title -->





                    <?php }//end if show specs not empty ?>



                    <?php
                    $swatch_section_title = get_field('swatches_section_title');
                    if( empty($swatch_section_title) ){
                        $swatch_section_title = 'Swatches';
                    }
                    ?>
                    <?php if(!empty($showswatches)){ ?>

                        <div class="<?php echo str_replace(' ', '_', $swatchestabtitle); ?> tab">
                            <section class="swatches cf">
                                <h3><?php echo $swatch_section_title ?></h3>
                                <div class="swatches-wrapper cf">
                                    <?php if( get_field('swatches') ){ ?>
                                        <?php while( the_repeater_field('swatches') ){ ?>

                                            <div class="swatch matchHeight2">

                                                <? if( get_sub_field('swatch_type') == 'color' ){ ?>
                                                    <div class="swatch-type" style="background-color:<?php the_sub_field('swatch_color') ?>"></div>
                                                <?php } ?>

                                                <? if( get_sub_field('swatch_type') == 'image' ){ ?>
                                                    <div class="swatch-type" style="background-image:url(<?php the_sub_field('swatch_image') ?>)"></div>
                                                <?php } ?>

                                                <div>
                                                    <p><?php echo the_sub_field('swatch_name') ?></p>
                                                </div>

                                            </div>
                                        <?php }//end while swatches ?>
                                    <?php }//end if swatches ?>
                                </div>
                            </section>
                        </div>

                    <?php }//end iof show swatches not empty ?>

                    <?php if(!empty($showcustom)){ ?>
                        <?php

                        $custom_section_title = get_field('ge_section_title');
//                        if( empty($custom_section_title) ){
//                            $custom_section_title = 'More Details';
//                        }

                        $numcol = get_field('general_content_type');
                        if( $numcol == 'onec' ){
                            $numcol = 1;
                            $column1 = get_field('column_content');
                        }
                        if( $numcol == 'twoc' ){
                            $numcol = 2;
                            $column1 = get_field('column_content');
                            $column2 = get_field('column_content_2');
                        }
                        if( $numcol == 'threec' ){
                            $numcol = 3;
                            $column1 = get_field('column_content');
                            $column2 = get_field('column_content_2');
                            $column3 = get_field('column_content_3');
                        }
                        if( $numcol == 'fourc' ){
                            $numcol = 4;
                            $column1 = get_field('column_content');
                            $column2 = get_field('column_content_2');
                            $column3 = get_field('column_content_3');
                            $column4 = get_field('column_content_4');
                        }
                        ?>
                        <div class="<?php echo str_replace(' ', '_', $customtabtitle); ?> tab gen-content-tab">
                            <section class="cf">
                                <?php if ( $custom_section_title = get_field( 'ge_section_title' ) ): ?>
                                    <h3><?php echo $custom_section_title ?></h3>
                                <?php endif; ?>
                                <?php if(!empty($column1)){ ?>
                                    <div class="col-<?php echo $numcol ?> matchHeight"><?php echo $column1 ?></div>
                                <?php } ?>
                                <?php if(!empty($column2)){ ?>
                                    <div class="col-<?php echo $numcol ?> matchHeight"><?php echo $column2 ?></div>
                                <?php } ?>
                                <?php if(!empty($column3)){ ?>
                                    <div class="col-<?php echo $numcol ?> matchHeight"><?php echo $column3 ?></div>
                                <?php } ?>
                                <?php if(!empty($column4)){ ?>
                                    <div class="col-<?php echo $numcol ?> matchHeight"><?php echo $column4 ?></div>
                                <?php } ?>
                            </section>
                        </div>
                    <?php }//end if show custom not empty ?>

                    <?php if(!empty($showmedia)){ ?>
                        <?php
                        $media_section_title = get_field('media_section_title');
                        if(empty($media_section_title)){
                            $media_section_title = 'Our Gallery';
                        }
                        $images = get_field('add_images');
                        ?>
                        <div class="<?php echo str_replace(' ', '_', $mediatabtitle); ?> tab">
                            <section class="cf">
                                <h3><?php echo $media_section_title ?></h3>

                                <?php if( $images ): ?>
                                    <div class="gallery-page-product">
                                        <?php foreach( $images as $image ): ?>
                                            <div>
                                                <img src="<?php echo $image['url']; ?>" alt="">
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>



                                <?php

                                if( get_field('add_video') ){ ?>


                                    <?php


                                    while( the_repeater_field('add_video') ){

                                        $videoLink = get_sub_field('video_embed_code');
                                        ?>

                                        <div class="media-library-tab-video">
                                            <?php echo $videoLink; ?>

                                        </div>


                                    <?php } //end while add video ?>

                                <?php }//end if add video ?>



                            </section>
                        </div>

                    <?php }//end if show media not empty ?>

                    <?php if(!empty($showvl)){ ?>

                        <div class="<?php echo str_replace(' ', '_', $vltabtitle); ?> tab">
                            <section class="cf">
                                <h3><?php echo $vltabtitle ?></h3>

                                <?php if( get_field('visual_list') ){ ?>
                                    <?php while( the_repeater_field('visual_list') ){ ?>

                                        <div class="vl-item cf imgp_<?php echo get_sub_field('vl_img_position') ?>">
                                            <div class="fifty-50 the-image">
                                                <img src="<?php echo get_sub_field('vl_image')  ?>" alt="">
                                            </div>
                                            <div class="fifty-50 the-content">
                                                <?php echo get_sub_field('vl_content') ?>
                                            </div>
                                        </div>

                                    <?php }//end while visual_list ?>
                                <?php }//end if visual_list?>

                            </section>
                        </div>

                    <?php }//end if show vl not empty ?>



                    <?php if(!empty($showgl)){ ?>

                        <div class="<?php echo str_replace(' ', '_', $gltabtitle); ?> tab">
                            <section class="cf">
                                <h3><?php echo $gltabtitle ?></h3>

                                <?php if( get_field('grid_list_items') ){ ?>
                                    <div class="grid-list">
                                        <?php while( the_repeater_field('grid_list_items') ){ ?>

                                            <div class="grid-item matchHeight3">
                                                <img src="<?php echo the_sub_field('grid_img') ?>" alt="">
                                                <h4><?php strip_tags( the_sub_field('grid_title') ) ?></h4>
                                                <p><?php strip_tags( the_sub_field('grid_subtitle') ) ?></p>
                                            </div>

                                        <?php }//end while drid list ?>
                                    </div>
                                <?php }//end if grid list?>

                            </section>
                        </div>

                    <?php }//end if show vl not empty ?>


                </div><!--tabs-wrapper end -->
                <style>
                    .tabs-wrapper {
                        border: 1px solid #DDDDDD;
                        background-color: #F9F9F9;
                    }
                    .tab-links li.current-tab a {
                        background-color: #F9F9F9;
                    }
                    .tab-links li {
                        height: 50px;
                        border-radius: 6px 6px 0 0;
                        margin-right: 25px;
                    }
                    .tab-links li a {
                        background-color: #EBEBEB;
                        text-transform: none;
                        font-size: 20px;
                        color: #4B4B4B !important;
                    }
                    .tab-links li.current-tab a {
                        color: #406418 !important;
                    }
                </style>
            <?php }//and if only show if one of them is not empty ?>

            <!-------------------------------------------------------------CUSTOM CODE END ------------------------->



            <?php wp_reset_postdata(); ?>
            <?php wp_reset_query(); ?>


        </div>
    </div>


    <!-- Structure Data -->
    <?php
    global $post;
    $terms = get_the_terms( $post->ID, 'product_cat' );
    foreach ($terms as $term) {
        $product_cat = $term->name;
        break;
    }
    $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
    $sdBrand = $product_cat;
    $sdProductName = get_the_title();
    $sdProductImage = esc_url( $large_image_url[0] );
    $sdProductDescription = strip_tags(get_the_excerpt());
    ?>
    <script type='application/ld+json'>
{
    "@context": "http://www.schema.org",
    "@type": "product",
    "brand": "<?php echo $sdBrand; ?>",
    "name": "<?php echo $sdProductName; ?>",
    "image": "<?php echo $sdProductImage; ?>",
    "description": "<?php echo $sdProductDescription; ?>"
}
 </script>

