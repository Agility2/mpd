<?php
/**
 * Template Name: Product Category
 */
while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'categories'); ?>
<?php endwhile; ?>