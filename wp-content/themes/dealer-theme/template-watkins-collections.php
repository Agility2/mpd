<?php
/**
 * Template Name: Watkins-Collections Page
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
 
  <?php get_template_part('templates/watkins-collections', 'page'); ?>
<?php endwhile; ?>
