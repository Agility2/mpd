<?php
/**
 * Template Name: Flexible Template
 */

?>

<div class="fl-wrap">
    <div class="row">
        <div class="columns small-12">
            <h2 class="text-center fl-title"><?php echo get_the_title(); ?></h2>
        </div>
        <?php
        if (have_rows('content')):

            while (have_rows('content')) :
                the_row();


                if (get_row_layout() == 'accordion'):
                    $items = get_sub_field('items');
                    $counter = 0;
                    ?>
                    <div class="columns small-12 large-8 large-offset-2">
                        <ul class="fl-accordion">
                            <?php foreach ($items as $item):
                                $title = $item['title'];
                                $content = $item['content'];
                                ?>
                                <?php if ($title && $content): ?>
                                <li class="<?php echo $counter == 0 ? 'show' : ''; ?>">
                                    <a href="#" class="fl-toggle"><?php echo $title; ?></a>
                                    <div class="fl-accordion-content"
                                         style="display: <?php echo $counter == 0 ? 'block' : ''; ?>">
                                        <p class="">
                                            <?php echo $content; ?>
                                        </p>
                                    </div>
                                </li>
                                <?php $counter++; ?>
                            <?php endif; ?>


                            <?php
                            endforeach; ?>
                        </ul>
                    </div>

                <?php
                elseif (get_row_layout() == 'default_content'):
                    $width = get_sub_field('width');
                    $content = get_sub_field('content');
                    if ($content): ?>
                        <div class="columns small-12 <?php echo $width == 'Thin' ? 'large-8 large-offset-2' : ''; ?>">
                            <?php echo $content; ?>
                        </div>
                    <?php
                    endif;

                elseif (get_row_layout() == 'cards'):
                    $title = get_sub_field('title');
                    $cards = get_sub_field('cards');
                    if ($cards): ?>
                        <div class="columns">
                            <div class="row cards">
                                <?php if ($title): ?>
                                    <div class="columns small-12">
                                        <h4 class="text-center"><?php echo $title; ?></h4>
                                    </div>
                                <?php endif; ?>
                                <?php foreach ($cards as $card):
                                    $image_link = $card['image_link'];
                                    $title = $card['title'];
                                    if ($title && $image_link):?>
                                        <div class="columns small-12 medium-4">
                                            <div class="card-item">
                                                <figure class="wp-caption">
                                                    <img src="<?php echo $image_link; ?>" alt="">

                                                    <figcaption
                                                            class="widget-image-caption wp-caption-text"><?php echo $title; ?></figcaption>
                                                </figure>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php
                    endif;
                elseif (get_row_layout() == 'content_with_video'):
                    $content = get_sub_field('content');
                    $video = get_sub_field('video');

                    if ($content && $video):?>
                        <div class="columns small-12 ">
                            <div class="content-video-wrap row">
                                <div class="columns small-12 medium-6">
                                    <?php echo $content; ?>
                                </div>

                                <div class="columns small-12 medium-6">
                                    <div class="responsiveWrapper">
                                        <?php echo $video; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif;
                endif;


            endwhile;


        else :

        endif;
        ?>
    </div>
</div>

<script>
    ;
    (function ($) {
        $('.fl-toggle').click(function (e) {
            e.preventDefault();

            var $this = $(this);

            if ($this.parent().hasClass('show')) {
                $this.parent().removeClass('show');
                $this.next().slideUp(350);
            } else {
                $this.parent().parent().find('li').removeClass('show');
                $this.parent().parent().find('li .fl-accordion-content').slideUp(350);
                $this.parent().toggleClass('show');
                $this.next().slideToggle(350);
            }
        });
    }(jQuery));

</script>