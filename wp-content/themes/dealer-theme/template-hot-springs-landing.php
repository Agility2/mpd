<?php
/**
 * Template Name: Hot Springs Landing Page
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
 
  <?php get_template_part('templates/hot-springs-landing', 'page'); ?>
<?php endwhile; ?>
