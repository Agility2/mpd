<div class="container">
<?php
    $header_img = get_field('blog_header_image', 'option');
    $header_subtitle = get_field('blog_sub_title', 'option');
    $header_overlay = get_field('blog_overlay_color', 'option');
    $overlay_opacity = get_field('blog_overlay_opacity', 'option');
?>
<?php
    if ( have_posts() ) {
?>

<?php if(!empty($header_img)){ ?>
    <div class="hero" style="background-image:url('<?php echo $header_img ?>');">
       <?php //if( !empty($header_overlay) ){ ?>
            <div class="overlay" style="background-color:<?php echo $header_overlay ?>; opacity:.<?php echo $overlay_opacity ?>;"></div>
        <?php //} ?>
        <img src="<?php echo $header_img ?>" alt="">
        <div class="the-content">

            <h2>Search Results For:</h2>

                <h3><?php echo esc_html( get_search_query( false ) ); ?></h3>

            <?//= custom_search_form( null, 'Search posts', 'post'); ?>
        </div>
    </div>
<?php } ?>

<?php
$blog_style = get_field('blog_style' , 'option');
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
?>
<div class="the-posts <?php echo $blog_style ?>-grid" style="max-width:1200px; margin:0 auto; padding: 1em;">

    <?php
        if( $blog_style == 'masonry' ){
    ?>
        <?php
            while (have_posts()) : the_post();
                $title = get_the_title();
                $featured_img = $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(450, 250) );
                $summary = get_field('blog_summary');
        ?>
        <article <?php post_class('masonry-item') ?>>
           <?php if($featured_img){ ?>
               <a href="<?php the_permalink() ?>">
                    <div class="head">
                        <img src="<?php echo $featured_img[0] ?>" alt="<?php echo $title ?>">
                    </div>
                </a>
            <?php } ?>
            <div class="body">
                <a href="<?php the_permalink() ?>"><h3><?php echo $title ?></h3></a>
                <?php if( !empty($summary) ){ ?>
                    <p><?php echo $summary ?></p>
                    <a href="<?php echo the_permalink() ?>">Read More</a>
                <?php }else{ ?>
                <p><?php the_excerpt() ?></p>
                <?php } ?>
            </div>
        </article>
        <?php endwhile;//end while ?>
        <?php wp_reset_postdata(); ?>
    <?php } //end if masonry style
        if( $blog_style == 'list' ){
    ?>
        <?php
            while (have_posts()) : the_post();
            $query->the_post();
            $title = get_the_title();
            $featured_img = $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(450, 250) );
        ?>

            <article <?php post_class(); ?>>
                <header>
                <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <?php get_template_part('templates/entry-meta'); ?>
                </header>
                <div class="entry-left-side large-4 small-12 xsmall-12">
                <?php
                if($featured_img){
                echo '<img src="'.$featured_img[0].'" alt="'.$title.'">';
                }
                ?>
                </div>
                <div class="entry-summary entry-right-side large-8 small-12 xsmall-12">
                <?php the_excerpt(); ?>
                </div>
                <div class="clear"></div>
            </article>

        <?php endwhile;//end while ?>
        <?php wp_reset_postdata(); ?>
    <?php }//end if list style ?>
</div>

<?php }//end if have psots ?>











<?php if (!have_posts()) : ?>
<?php if(!empty($header_img)){ ?>
    <div class="hero" style="background-image:url('<?php echo $header_img ?>');">
       <?php //if( !empty($header_overlay) ){ ?>
            <div class="overlay" style="background-color:<?php echo $header_overlay ?>; opacity:.<?php echo $overlay_opacity ?>;"></div>
        <?php //} ?>
        <img src="<?php echo $header_img ?>" alt="">
        <div class="the-content">

            <h2>No Results Found:</h2>

                <h3>Try another search</h3>

            <?= custom_search_form( null, 'Search posts', 'post'); ?>
        </div>
    </div>
<?php } ?>
<?php endif; ?>
</div>